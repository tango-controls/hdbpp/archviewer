# Archviewer

This is a webapp for viewing HDB++ archived Tango attribute data in a Timescale database.

The back-end is written in python >=3.11, using FastAPI. 

The frontend uses javascript + React. D3 for plotting. For React state management, "zustand".


## Installation

    $ python3 -m venv env
    $ env/bin/pip install -e .
    $ npm install
    $ make build


## Configuration

Put whatever settings you need in a `.env` file in the repo dir. See `config.py` for which settings are available. 

E.g. to access a TimescaleDB instance on your local machine, `.env` might contain:

    timescale_host=localhost
    timescale_port=5432
    hdb_user=hdb
    hdb_password=mysupersecurepwd

Alternatively, settings can be put in corresponding environment variables, e.g. `TIMESCALE_HOST`. That's probably
preferred for deployment.

If you set `fake_db=true`, the service should start without a timescale database, delivering some generated (very boring) data. This should be enough for trying the user interface.

Default logging settings are in `logging.ini`. This can be pointed to some other file with the setting `logging_config`.

The application uses local disk to cache data while a query is underway. It defaults to `/tmp` which is probably a good choice in most cases. It can be configured if needed, using the `cache_dir` setting.

Note: while this application does not need db write access for normal operation (and probably should not have it), it needs to create a few utility functions in the database. The easy way to do this is to use an account with write access on the first run. Starting the server is not enough, the functions are created as needed on the first DB access. After that, only read access is needed.

TODO: Provide the functions as a separate SQL file for separate insertion.


## Running

    $ env/bin/uvicorn archviewer.app:app --port 8123

Now the app should be accessible at http://localhost:8123


## API docs

The backend API is self-documented (thanks to FastAPI), check out http://localhost:8123/docs for interactive docs.

Note that the backend API is primarily intended for use by the frontend at this point, and is subject to change.

Under `scripts` you can find the `archfetcher.py` script which is a simple way to download data from the API.


## Development 

### Backend

The backend is really just a thin layer between the client and database, translating requests into SQL and sending results back. As much as possible of the work (e.g. aggregation) is performed by the database.

For auto-reload on changes in the backend code, add the `--reload` argument to `uvicorn`. 


### Frontend

To rebuild the frontend, we use `esbuild`.

    $ npm install

When developing, you can run esbuild in "watch" mode:

    $ make watch

To make changes to the frontend, just edit the JS files under `frontend/` and then reload the page. The important application state is kept in the URL.

You can install the browser add-on from https://react.dev/learn/react-developer-tools to be able to inspect the components etc.


## Tests

To run the tests:

    $ env/bin/pip install -e .[tests]
    $ env/bin/playwright install firefox  # may work on other browsers but only tested on FF for now
    $ env/bin/pytest --browser firefox 


## Deployment

There's a MAX IV specific Dockerfile, that can work as an example. Also, note that for deployment at MAX IV there is a Helm repo at https://gitlab.maxiv.lu.se/kits-maxiv/kubernetes/helm-maxiv-archviewer

