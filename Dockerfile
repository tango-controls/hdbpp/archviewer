# ---- Frontend ----
FROM node:18-alpine AS build

# Build the fronend
WORKDIR /app
COPY frontend /app/frontend
COPY package.json package-lock.json /app/
RUN npm install
RUN ./node_modules/.bin/esbuild frontend/index.js --bundle --outfile=static/out.js


# ---- Backend ----
FROM harbor.maxiv.lu.se/dockerhub/library/python:3.12-slim

ENV DEBIAN_FRONTEND noninteractive
RUN ln -sf /usr/share/zoneinfo/Europe/Stockholm /etc/localtime

# Install setuptools-scm dependency
RUN apt-get update
RUN apt-get install -y --no-install-recommends git

# Copy application files
COPY archviewer /app/archviewer
COPY static /app/static
# Copy over the frontend build from previous step
COPY --from=0 /app/static /app/static
COPY pyproject.toml /app/
COPY logging.ini /app/
WORKDIR /app

# Install backend
# Note: this mount stuff is needed for setuptools-scm to correctly
# deduce the version from git.
RUN --mount=source=./.git,destination=/app/.git,type=bind \
    pip install -e .

EXPOSE 8000
CMD uvicorn archviewer.app:app --host 0.0.0.0 --port 8000
