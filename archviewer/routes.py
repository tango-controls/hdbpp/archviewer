import asyncio
from datetime import datetime
from enum import Enum
import logging
import os
import time
import tempfile
from typing import List, Type, Optional
from uuid import uuid4

from fastapi import APIRouter, Query, Depends, Request, Response, HTTPException, status
from fastapi.responses import (
    StreamingResponse,
    FileResponse,
)
from pydantic import BaseModel, Field

from .config import get_config
from . import calculate_aggregation_size
from .timescale import HDBPPTimescaleConnection, FakeHDBPPTimescaleConnection


config = get_config()
logger = logging.getLogger(__name__)


router = APIRouter()


async def make_hdbpp(db: str) -> HDBPPTimescaleConnection:

    """Return a database connection"""

    logger.debug("Creating database connection for database %r", db)
    connection_class: Type
    if config.fake_db:
        # For testing purposes
        connection_class = FakeHDBPPTimescaleConnection
    else:
        connection_class = HDBPPTimescaleConnection
    hdbpp = connection_class(
        host=config.timescale_host,
        port=config.timescale_port,
        database=db,
        user=config.hdb_user,
        password=config.hdb_password,
    )
    await hdbpp.connect()
    return hdbpp


# Cache hdbpp instances, to make sure we don't create duplicates
hdbpp_futures = {}
hdbpp_cache = {}

async def get_hdbpp(db: str):
    if db in hdbpp_cache:
        return hdbpp_cache[db]
    if db in hdbpp_futures:
        fut = asyncio.Future()
        hdbpp_futures[db].append(fut)
        return await fut

    hdbpp_futures[db] = futs = []
    hdbpp = await make_hdbpp(db)
    for fut in futs:
        fut.set_result(hdbpp)
    hdbpp_cache[db] = hdbpp
    return hdbpp


@router.get("/")
async def get_index():
    """Deliver the frontend"""
    return FileResponse("static/index.html")


# TODO add an "/health" endpoint or something, for readiness/liveness checks?


class Database(BaseModel):
    db_name: str


@router.get("/api/databases", response_model=List[Database])
async def get_databases():
    """
    Return a list of HDB databases.
    """
    return [{"db_name": db} for db in config.timescale_databases.split(",")]


class Controlsystem(BaseModel):
    cs_name: str


@router.get("/api/controlsystems", response_model=List[Controlsystem])
async def get_controlsystems(
        db: str,
        hdbpp: HDBPPTimescaleConnection = Depends(get_hdbpp),
):
    """
    Return a list of control systems we have data for in the given db.
    """
    css = await hdbpp.get_controlsystems()
    return map(dict, css)


class Attribute(BaseModel):
    """Basic information about a single attribute"""
    db: str
    att_conf_id: int
    type: str
    format: str
    write: str
    att_name: str
    name: str
    cs_name: str
    table_name: Optional[str] = None


@router.get("/api/attribute/search", response_model=List[Attribute])
async def get_attribute_search(db: str, cs: str, phrase: str = "",
                               hdbpp: HDBPPTimescaleConnection = Depends(get_hdbpp)):
    """
    Attribute search queries, matching a search phrase against attribute names
    in a given control system.

    Normally only reports exact attribute name matches, but the phrase may
    contain "*" which is interpreted as a wildcard character, matching anything.
    """
    return await hdbpp.search_attributes(cs, phrase)


@router.get("/api/attribute/info", response_model=List[Attribute])
async def get_attribute_info(
    db: str,
    att_conf_id: List[int] = Query([]),
    hdbpp: HDBPPTimescaleConnection = Depends(get_hdbpp),
):
    """
    Return information on specific attribute(s), identified by att_conf_id.

    Returns a JSON array of attribute info objects:

    - att_conf_id: database id
    - att_name: the full Tango name of the attribute
    - cs_name: Control system name
    - name: Tango name, stripped of control system prefix
    - type: Data type of the attribute, e.g. "DEV_DOUBLE"
    - format: Data format of the attribute, e.g "SCALAR"
    - write: Read/write type of the attribute, e.g. "read"
    """
    return await hdbpp.get_attributes(att_conf_id)


# Globals keeping track of background queries
get_attribute_data_tasks: dict[str, asyncio.Task] = {}
get_attribute_data_results: dict[str, str] = {}


async def bg_get_attribute_data(
        hdbpp, task_id, att_conf_id, att_info, start_time, end_time,
        agg_size, agg_func, grace_time=60
):
    "Background data fetching task"
    task_logger = logger.getChild(task_id)
    try:
        # Fetch data into a temporary file. Once finished, the client
        # will be able to read the file.
        # The temp file will be automatically removed once we exit the
        # context manager.
        with tempfile.NamedTemporaryFile(
                prefix=f"attr_data_{att_conf_id}_{task_id}",
                dir=config.cache_dir) as f:
            t0 = time.time()
            task_logger.info(f"Get data for attribute {att_conf_id}: {start_time.isoformat()} - {end_time.isoformat()} agg_size={agg_size} agg_func={agg_func} task={task_id}")
            try:
                await hdbpp.get_attribute_data_tsv(
                    f.name,
                    att_conf_id,
                    att_info,
                    start_time,
                    end_time,
                    agg_size,
                    agg_func=agg_func,
                )
            except asyncio.CancelledError:
                # This typically happens because the client no longer wants
                # the data. It's perfectly normal.
                task_logger.debug(f"Cancelled")
                return
            filesize = os.path.getsize(f.name)
            task_logger.info(f"Got data for attribute {att_conf_id} in {time.time() - t0:.3f} s ({filesize} bytes)")
            get_attribute_data_results[task_id] = str(f.name)  # Mark as ready
            await asyncio.sleep(grace_time)  # Wait a bit so the client can download the data
    except Exception as e:
        task_logger.exception(f"Error: {e}")
    finally:
        task_logger.debug(f"Cleaning up")
        del get_attribute_data_tasks[task_id]
        del get_attribute_data_results[task_id]


@router.get("/api/attribute/data/{task_id}",
         status_code=status.HTTP_200_OK,
         response_class=FileResponse,
         responses={
             200: {
                 "content": {"text/tab-separated-values": {}},
                 "description": "Attribute data in tab-separated values text format.",
             },
             404: {
                 "description": "The query or result is not available.",
             }
         })
async def get_attribute_data_result(task_id: str,
                                    response: Response):
    """
    Get results from a query, previously started via /attribute/data/.
    This can be queried repeatedly but will not respond until
    the data is available. Once the data is available it will be
    kept for a short time.

    Data returned as plaintext, one row per point, in tab separated columns:
    - data_time (iso encoded date),
    - value_r
    - value_w (always empty for read-only attributes),
    - quality (encoded as int),
    - att_error_desc_id (int)
    """

    try:
        while True:
            # Keep the client waiting until there's data
            task = get_attribute_data_tasks[task_id]
            if task.cancelled():
                raise KeyError  # TODO give some other error in this case
            if task_id not in get_attribute_data_results:
                await asyncio.sleep(0.1)
            else:
                break

        task_logger = logger.getChild(task_id)
        filename = get_attribute_data_results.get(task_id)
        task_logger.info(f"Delivering data to client")
        return filename  # FileResponse will take over from here
    except KeyError:
        # Probably too late, the data has been cleaned up, or job was canceled
        raise HTTPException(status_code=404, detail="Data not found")


@router.post("/api/attribute/data/{task_id}/cancel")
async def cancel_attribute_data_task(task_id: str, response: Response):
    """A polite client can cancel a pending data fetch it doesn't need."""
    if task_id in get_attribute_data_tasks:
        logger.debug(f"Request to cancel {task_id}")
        task = get_attribute_data_tasks.pop(task_id)
        task.cancel()
    else:
        response.status_code = status.HTTP_404_NOT_FOUND


class AggregationFunction(str, Enum):
    "Allowed aggregation functions"
    avg = "avg"
    min = "min"
    max = "max"


@router.get("/api/attribute/data",
         status_code=status.HTTP_202_ACCEPTED,
         responses={
             202: {
                 "content": {"application/json": {}},
                 "description": "Info about the query and urls for handling it.",
             },
             404: {
                 "description": "The attribute was not found in the DB.",
             }
         })
async def get_attribute_data(
    request: Request,
    db: str,
    att_conf_id: int,
    start_time: datetime,
    end_time: datetime,
    agg_limit: int = 20000,
    agg_buckets: int = 1000,
    agg_func: AggregationFunction = AggregationFunction.avg,
    hdbpp: HDBPPTimescaleConnection = Depends(get_hdbpp),
):

    """
    Query for attribute data for one attribute and time period.

    Aggregation is controlled by agg_limit, which sets an (approximate) max rows
    for raw data. Any more and we will return aggregated data, using the
    agg_buckets and agg_func parameters.

    This route does not return actual data, but only some metadata plus
    URLs for getting the actual results, or cancelling the query.
    This is because we can't guarantee that the query will complete in a
    reasonable time, which may cause things to time out (browser, gateway...)
    The URL returned is specific to the query and can therefore be retried until
    it returns, unlike this one which would start a new query each time.

    TODO: also support getting data as JSON?
    """

    try:
        (att_info,) = await hdbpp.get_attributes([att_conf_id])
    except KeyError:
        raise HTTPException(status_code=404, detail="Attribute not found")

    # Find out if we should aggregate
    count = await hdbpp.get_attribute_count(
        att_conf_id,
        att_info["table_name"],
        start_time,
        end_time,
        agg_limit=agg_limit,
        agg_buckets=agg_buckets,
        estimate=True)
    agg_size = calculate_aggregation_size(agg_limit, agg_buckets, count, start_time, end_time)

    task_id = str(uuid4())
    logger.debug(f"Starting data fetching task {task_id} for attribute {att_conf_id}")
    get_attribute_data_tasks[task_id] = asyncio.create_task(
        bg_get_attribute_data(hdbpp, task_id, att_conf_id, att_info, start_time, end_time,
                              agg_size, agg_func.value)
    )
    # We want "relative" URLS
    result_url = request.app.url_path_for("get_attribute_data_result", task_id=task_id).lstrip("/")
    cancel_url = request.app.url_path_for("cancel_attribute_data_task", task_id=task_id).lstrip("/")
    return {
        "info": {
            "n_points": count,
            "agg_size": agg_size or None,
        },
        "urls": {
            "result": result_url,
            "cancel": cancel_url,
        }
    }


class AttributeHistoryEvent(BaseModel):
    """Represents a single archiving "event", e.g. start/stop/pause/add/remove etc"""
    att_history_event_id: int
    att_conf_id: int
    event_time: datetime
    event: str


@router.get("/api/attribute/history", response_model=List[AttributeHistoryEvent])
async def get_attribute_history(
    db: str,
    start_time: datetime,
    end_time: datetime,
    att_conf_id: List[int] = Query([]),
    hdbpp: HDBPPTimescaleConnection = Depends(get_hdbpp),
):

    """
    Attribute archiving history; add/remove/stop/start etc.
    """
    hist = await hdbpp.get_history(att_conf_id, start_time, end_time)
    return (dict(h) for h in hist)


def clean_parameters(params):
    """Parse parameters, mostly to remove placeholder strings"""
    label = params["label"]
    unit = params["unit"]
    standard_unit = params["standard_unit"]
    display_unit = params["display_unit"]
    format_ = params["format"]
    archive_rel_change = params["archive_rel_change"]
    archive_abs_change = params["archive_abs_change"]
    archive_period = params["archive_period"]
    description = params["description"]
    return {
        "label": label,
        "unit": unit if unit != "No unit" else None,
        "standard_unit": standard_unit if standard_unit != "No standard unit" else None,
        "display_unit": display_unit if display_unit != "No display unit" else None,
        "format": format_,
        "archive_rel_change": float(archive_rel_change) if archive_rel_change != "Not specified" else None,
        "archive_abs_change": float(archive_abs_change) if archive_abs_change != "Not specified" else None,
        "archive_period": int(archive_period) if archive_period != "Not specified" else None,
        "description": description if description != "No description" else None,
        # "details": params["details"],  # I think this is currently unused.
    }


ATT_PARAMETERS = [
    "label",
    "unit",
    "standard_unit",
    "display_unit",
    "format",
    "archive_rel_change",
    "archive_abs_change",
    "archive_period",
    "description",
]


def make_parameter_events(db, rows):
    """Build 'incremental' parameter events"""
    prev_row = None
    for row in rows:
        clean_row = clean_parameters(row)
        if not prev_row:
            # Send the first event complete
            yield {
                "db": db,
                "att_conf_id": row["att_conf_id"],
                "recv_time": row["recv_time"],
                **clean_row
            }
        else:
            # Only include what changed in subsequent events
            # Usually only one thing changes, no point in sending everything every time.
            changes = {
                param: clean_row[param]
                for param in ATT_PARAMETERS
                if clean_row[param] != prev_row[param]
            }
            if changes:
                yield {
                    "db": db,
                    "att_conf_id": row["att_conf_id"],
                    "recv_time": row["recv_time"],
                    **changes
                }
        prev_row = clean_row


class AttributeParameters(BaseModel):
    """Represents a change in attribute configuration"""
    db: str
    att_conf_id: int
    recv_time: datetime
    label: Optional[str] = Field(None)
    unit: Optional[str] = Field(None)
    standard_unit: Optional[str] = Field(None)
    display_unit: Optional[str] = Field(None)
    format: Optional[str] = Field(None)
    archive_rel_change: Optional[float] = Field(None)
    archive_abs_change: Optional[float] = Field(None)
    archive_period: Optional[int] = Field(None)
    description: Optional[str] = Field(None)


@router.get("/api/attribute/parameters",
         response_model=List[AttributeParameters],
         response_model_exclude_unset=True)
async def get_attribute_parameters(
        db: str,
        start_time: datetime,
        end_time: datetime,
        att_conf_id: int,
        hdbpp: HDBPPTimescaleConnection = Depends(get_hdbpp),
):
    """
    Parameters are attribute configuration such as label, unit, format,
    as well as archive event settings.

    Note that this endpoint only allows a single attribute at a time. This
    is because of querying reasons; we want to fetch the last parameter
    change *before* the start time in order to know what the settings were
    at the start time. This is tricky to do for multiple attributes at the
    same time. Guess we could do several queries here but not much point...
    """
    rows = await hdbpp.get_parameters(att_conf_id, start_time, end_time)
    return make_parameter_events(db, rows)
