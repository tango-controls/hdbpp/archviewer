from logging.config import fileConfig
from pathlib import Path

from pydantic_settings import BaseSettings


class Settings(BaseSettings):

    """Load configuration from environment vars"""

    app_name: str = "ArchViewer"

    timescale_databases: str = "hdb"
    timescale_host: str = "localhost"
    timescale_port: int = 5432
    hdb_user: str = "postgres"
    hdb_password: str = ""
    hdb_database: str = "hdb"  # TODO remove?
    cors_allow_origins: str = "*"  # comma separated list of origins
    cache_dir: Path = Path("/tmp")  # Directory where we will temporarily store data
    logging_config: str = "logging.ini"
    fake_db: bool = False

    class Config:
        env_file = ".env"


def get_config():
    settings = Settings()

    # It's important to setup logging at an early point
    if settings.logging_config:
        fileConfig(settings.logging_config)

    # Check the cache file setting
    try:
        settings.cache_dir.mkdir(parents=True, exist_ok=True)
        test_file = settings.cache_dir / "archviewer_testfile.txt"
        test_file.write_text("test")
        test_file.unlink()
    except IOError as e:
        raise RuntimeError(f"Bad config: cache_dir {settings.cache_dir}: {e}")

    return settings
