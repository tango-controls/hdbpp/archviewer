import asyncio
from datetime import timedelta, datetime
import fnmatch
import io
from itertools import product, count
import logging
from math import sin
from textwrap import dedent
import time
from typing import List, Dict, Sequence, Any, Optional
from random import random, randint, choice

import asyncpg


logger = logging.getLogger(__name__)


class HDBPPTimescaleConnection:

    """
    Connection to a timescale DB instance containing HDB++ format data

    To access several databases (or "schema") in the same instance, a separate
    connection should be created for each.
    """

    # Tweak the PG date format to be compliant with JS Date parsing
    # Firefox older than v ~92 or so requires this. Probably we can
    # drop this at some point.
    DATE_FORMAT = "YYYY-MM-DD HH24:MI:SS.USTZH:TZM"

    def __init__(self, host: str, port: int, database: str, user: str, password: str, timeout=5):

        self.host = host
        self.port = port
        self.database = database

        self.user = user
        self.password = password

        self.timeout = timeout  # This is used for smaller queries, not the big data queries

        self.pool: asyncpg.Pool = None

        self._attr_info_cache: Dict[int, Dict[str, Any]] = {}
        self.logger = logger.getChild(f"{self.host}:{self.port}/{self.database}")

    async def connect(self):
        if not self.pool:
            self.logger.info("Connecting to %s:%d/%s",
                             self.host, self.port, self.database)
            try:
                self.pool = await asyncpg.create_pool(
                    host=self.host,
                    database=self.database,
                    user=self.user,
                    password=self.password,
                    port=self.port,
                    # TODO these numbers are just made up, look into this
                    min_size=5,
                    max_size=10,
                )
                self.logger.debug("Database pool connection opened")
            except asyncpg.exceptions.PostgresError:  # TODO
                self.logger.exception("Error connecting to database")
                raise

            await self._create_functions()
        else:
            self.logger.info("Re-using connection to %s:%d/%s",
                             self.host, self.port, self.database)

    async def close(self):
        await self.pool.close()

    async def _create_functions(self):

        """
        Install some postgres functions for later use
        Note: this is the only thing this service needs write access for, and it
        should only be needed once.
        """

        # TODO prefix function names e.g. "archviewer_"

        estimate_count_func = dedent(
            """
            CREATE OR REPLACE FUNCTION count_estimate(query text) RETURNS integer AS $f$
            DECLARE
              rec   record;
              rows  integer;
            BEGIN
              FOR rec IN EXECUTE 'EXPLAIN ' || query LOOP
                rows := substring(rec."QUERY PLAN" FROM ' rows=([[:digit:]]+)');
                EXIT WHEN rows IS NOT NULL;
              END LOOP;
              RETURN rows;
            END;
            $f$ LANGUAGE plpgsql VOLATILE STRICT;
        """
        )

        async with self.pool.acquire() as conn:
            try:
                # TODO figure out a way to check if the function needs to be updated
                await conn.execute("SELECT pg_get_functiondef('count_estimate(text)'::regprocedure)")
            except asyncpg.exceptions.UndefinedFunctionError:
                self.logger.info("Creating missing count_estimate function; write permission required!")
                await conn.execute(estimate_count_func)

        # Experimental!
        array_agg_funcs = "\n".join(
            dedent(
                f"""
                CREATE OR REPLACE FUNCTION array_column_{func}(arr anyarray)
                RETURNS anyarray AS
                $f$
                    SELECT array_agg(
                        (SELECT {func}(arr[i][j])
                         FROM generate_subscripts(arr, 1) AS i)
                         ORDER BY j)
                    FROM generate_subscripts(arr, 2) AS j
                $f$
                LANGUAGE sql IMMUTABLE PARALLEL SAFE;
            """
            )
            for func in ["max", "min", "avg"]
        )

        async with self.pool.acquire() as conn:
            try:
                await conn.execute("SELECT pg_get_functiondef('array_column_max(anyarray)'::regprocedure)")
            except asyncpg.exceptions.UndefinedFunctionError:
                self.logger.info("Creating missing array_column functions; write permission required!")
                await conn.execute(array_agg_funcs)

        self.logger.debug("Postgres functions OK")

    async def get_controlsystems(self):
        self.logger.debug("Connection pool stats: size=%r, idle_size=%r, max_size=%r",
                          self.pool.get_size(), self.pool.get_idle_size(), self.pool.get_max_size())
        async with self.pool.acquire() as conn:
            query = "SELECT DISTINCT cs_name FROM att_conf ORDER BY cs_name"
            try:
                # This query should be quick, let's fail fast here
                rows = await conn.fetch(query, timeout=self.timeout)
            except asyncio.exceptions.TimeoutError:
                self.logger.debug("Database timeout getting controlsystems")
                await self.pool.expire_connections()
                raise RuntimeError("Database timed out")
        return rows

    ATT_COLUMNS = (
        "att_conf_id",  # e.g. 45
        "att_name",  # tango://my.fine.cs:1000/sys/tg_test/1/double_scalar
        "table_name",  # "att_scalar_devdouble"
        "type",  # "DEV_DOUBLE"
        "format",  # "SCALAR"
        "write",  # "READ"
        "cs_name",  # "my.fine.cs:10000"
    )

    async def search_attributes(self, cs: str, phrase: str):
        # TODO we could consider to just cache the whole list of attributes
        # in memory... it will not be very large and will rarely change.
        async with self.pool.acquire() as conn:
            psql_pattern = phrase.replace("*", "%")
            query = dedent(
                f"""
                SELECT {', '.join(self.ATT_COLUMNS)} FROM att_conf
                JOIN att_conf_type ON att_conf_type.att_conf_type_id = att_conf.att_conf_type_id
                JOIN att_conf_format ON att_conf_format.att_conf_format_id = att_conf.att_conf_format_id
                JOIN att_conf_write ON att_conf_write.att_conf_write_id = att_conf.att_conf_write_id
                WHERE LOWER(att_conf.att_name) LIKE LOWER($1)
                ORDER BY att_conf.att_name
            """
            )
            try:
                rows = await conn.fetch(query, f"tango://{cs}/{psql_pattern}",
                                        timeout=self.timeout)
            except asyncio.exceptions.TimeoutError:
                raise RuntimeError("Database timed out")
        results = []
        for row in rows:
            cs_name = row["cs_name"]
            # Remove the CS prefix from the attribute name, it's redundant
            # TODO: are we sure the attribute names are always stored in "full" form?
            short_name = row["att_name"][len(f"tango://{cs_name}/"):]
            results.append(
                {
                    **row,
                    "db": self.database,
                    "name": short_name,
                }
            )
        return results

    async def get_attributes(self, att_conf_ids: List[int]):

        """Get basic info about the given attributes, as a list"""

        # This info never changes; let's cache it as it will be accessed a lot.
        non_cached_attrs = set()
        for att_conf_id in att_conf_ids:
            if att_conf_id not in self._attr_info_cache:
                non_cached_attrs.add(att_conf_id)

        if non_cached_attrs:
            query = dedent(
                f"""
            SELECT {', '.join(self.ATT_COLUMNS)} FROM att_conf
            JOIN att_conf_type ON att_conf_type.att_conf_type_id = att_conf.att_conf_type_id
            JOIN att_conf_format ON att_conf_format.att_conf_format_id = att_conf.att_conf_format_id
            JOIN att_conf_write ON att_conf_write.att_conf_write_id = att_conf.att_conf_write_id
            WHERE att_conf.att_conf_id = ANY($1)
            """
            )
            async with self.pool.acquire() as conn:
                try:
                    rows = await conn.fetch(query, non_cached_attrs,
                                            timeout=self.timeout)
                except asyncio.exceptions.TimeoutError:
                    raise RuntimeError("Database timed out")
            for row in rows:
                # TODO do this in a nicer way
                _, short_name = row["att_name"][len("tango://"):].split("/", 1)
                self._attr_info_cache[row["att_conf_id"]] = {
                    **row,
                    "db": self.database,
                    "name": short_name,
                }

        # TODO: how to handle nonexistent attributes?
        results = [self._attr_info_cache[att_conf_id] for att_conf_id in att_conf_ids]
        return results

    async def get_history(self, att_conf_ids: List[int],
                          start_time: Optional[datetime] = None,
                          end_time: Optional[datetime] = None):

        "Get the attribute event history (add/remove/start/stop/pause...)"

        if start_time and end_time:
            query = dedent(
                """
            SELECT att_history.att_history_event_id,
                   att_conf.att_conf_id,
                   event_time,
                   event
            FROM att_history
            JOIN att_history_event
            ON att_history_event.att_history_event_id = att_history.att_history_event_id
            JOIN att_conf ON att_history.att_conf_id = att_conf.att_conf_id
            WHERE att_history.att_conf_id = ANY($1)
               AND event_time BETWEEN $2 AND $3
            ORDER BY event_time
            """
            )
            async with self.pool.acquire() as conn:
                try:
                    return await conn.fetch(query, att_conf_ids,
                                            start_time, end_time,
                                            timeout=self.timeout)
                except asyncio.exceptions.TimeoutError:
                    raise RuntimeError("Database timed out")
        else:
            query = dedent(
                """
            SELECT * FROM att_history
            WHERE att_conf_id = ANY($1)
            """
            )
            async with self.pool.acquire() as conn:
                return await conn.fetch(query, att_conf_ids)

    async def get_parameters(self, att_conf_id: int,
                             start_time: datetime, end_time: datetime):
        """Get attribute configuration changes (label, unit, ...)"""
        query = dedent(
            """
        (SELECT * FROM att_parameter
            WHERE att_conf_id = $1
            AND recv_time < $2
         ORDER BY recv_time desc LIMIT 1)
        UNION ALL
        SELECT * FROM att_parameter
            WHERE att_conf_id = $1
            AND recv_time BETWEEN $2 AND $3
        ORDER BY recv_time
        """
        )
        async with self.pool.acquire() as conn:
            try:
                return await conn.fetch(query, att_conf_id, start_time, end_time, timeout=self.timeout)
            except asyncio.exceptions.TimeoutError:
                raise RuntimeError("Database timed out")

    async def get_attribute_count(
        self,
        att_conf_id: int,
        table_name: str,
        start_time: datetime,
        end_time: datetime,
        agg_limit: int,
        agg_buckets: int = 2000,
        estimate: bool = True,
    ):

        """
        Return the number of rows of data available for an attribute
        within a time range.
        """

        t0 = time.perf_counter()
        async with self.pool.acquire() as conn:
            if estimate:
                # This is a much faster way (~10x), but gets an approximate count.
                # https://www.postgresql.org/message-id/20050810133157.GA46247@winnie.fuhr.org
                # I think this will be fine for this use case; the agg limits are not
                # intended to be exact anyway.
                # TODO: seems pretty good so far, but keep an eye on how accurate it is.
                # I think it's accuracy will depend on running regular ANALYZE queries
                # on the database.
                start = start_time.isoformat()
                end = end_time.isoformat()
                count_estimate_query = dedent(
                    f"""
                SELECT count_estimate('SELECT 1 FROM {table_name}
                    WHERE att_conf_id = {att_conf_id}
                    AND data_time BETWEEN ''{start}'' AND ''{end}''')
                """
                )
                (result,) = await conn.fetch(count_estimate_query)
                count = result["count_estimate"]
            else:
                # Appears to take ~10% of the real query's time. That seems
                # a bit much overhead, let's try the faster estimation method above.
                count_query = dedent(
                    f"""
                SELECT count(*) FROM {table_name}
                    WHERE att_conf_id = $1 AND data_time BETWEEN $2 AND $3
                """
                )
                (result,) = await conn.fetch(count_query, att_conf_id, start_time, end_time)
                count = result["count"]

        self.logger.debug(
            "Counted %d rows (estimated: %r) for att_conf_id %d in %.3f s",
            count,
            estimate,
            att_conf_id,
            time.perf_counter() - t0,
        )
        return count

    async def get_attribute_data(
        self,
        att_conf_id: int,
        att_info: Optional[Dict[str, Any]] = None,
        start_time: Optional[datetime] = None,
        end_time: Optional[datetime] = None,
        agg_size: int = 0,
        agg_func: str = "avg",
        page_size: int = 100000,
        expected_count: Optional[int] = None,
    ) -> Sequence[asyncpg.Record]:

        "Get attribute data, returning list of Records"

        if att_info is None:
            (att_info,) = await self.get_attributes([att_conf_id])

        if att_info["format"] == "SCALAR":
            if agg_size == 0:
                query = self._build_query_scalar_full(att_info["table_name"])
            else:
                query = self._build_query_scalar_agg(
                    att_info["table_name"],
                    agg_size,
                    agg_func,
                )
            return await self.pool.fetch(query, att_conf_id, start_time, end_time)
        raise NotImplementedError("Sorry, no support for non-scalar data yet")

    async def get_attribute_data_tsv(
        self,
        output,
        att_conf_id: int,
        att_info: Optional[Dict[str, Any]] = None,
        start_time: Optional[datetime] = None,
        end_time: Optional[datetime] = None,
        agg_size: int = 0,
        agg_func: str = "avg",
        page_size: int = 100000,
    ):
        """
        Gets all data for the given attribute between start_time and end_time.
        Aggregates data if agg_size is > 0.
        Returns data in plain TSV format, as an async generator of chunks (bytes).
        """
        logger.debug("get_attribute_data_tsv %r %s - %s agg_size=%r agg_func=%s",
                     att_conf_id,
                     start_time.isoformat(), end_time.isoformat(),
                     agg_size, agg_func)
        if att_info is None:
            (att_info,) = await self.get_attributes([att_conf_id])

        # default to the last 24 hours
        if not start_time:
            start_time = datetime.now() - timedelta(days=1)
        if not end_time:
            end_time = datetime.now()

        # build SQL query
        if att_info["format"] == "SCALAR":
            if agg_size == 0:
                query = (
                    self._build_query_scalar_full(
                        att_info["table_name"],
                    )
                )
            else:
                query = self._build_query_scalar_agg(
                    att_info["table_name"],
                    agg_size,
                    agg_func,
                )

        elif att_info["format"] == "SPECTRUM":
            if agg_size == 0:
                query = (
                    self._build_query_spectrum_full(
                        att_info["table_name"],
                    )
                )
            else:
                query = self._build_query_spectrum_agg(
                    att_info["table_name"],
                    agg_size,
                    agg_func,
                )

        # Perform query
        async with self.pool.acquire() as conn:
            # Let asyncpg directly copy data to the given output (file).
            # This should be low overhead, and we don't have to handle the data.
            # Drawback: we get whatever format pg gives us. Fortunately it's
            # pretty flexible!
            return await conn.copy_from_query(
                query,
                att_conf_id,
                start_time, end_time,
                output=output,
                null="",
                format="csv",
                delimiter="\t",
            )

    def _build_query_scalar_agg(self, table_name, agg_size, agg_func):
        "Build query for scalar attribute data, with aggregation"

        # We don't want postgres to give us actual booleans, let's cast them to
        # int (false = 0, true = 1) instead. TODO: less hacky way
        if "boolean" in table_name:
            cast = "::int"
        else:
            cast = ""

        # Aggregate data
        # TODO: I haven't come up with a good way to get the nearest points
        # outside the range for aggregated data. Not sure what it even would mean.
        # TODO: some data types probably don't make sense to aggregate, at least
        # not e.g. average. E.g. boolean, state, enum.
        return dedent(
            f"""
        SELECT
            to_char(time_bucket('{agg_size} seconds', data_time)
                + '{agg_size // 2} seconds',
                '{self.DATE_FORMAT}') as data_time_agg,
            {agg_func}(value_r{cast}) as value_r,
            {agg_func}(value_w{cast}) as value_w,
            count(*) as n_points,
            count(att_error_desc_id) as n_errors
        FROM {table_name}
            WHERE att_conf_id = $1 AND data_time BETWEEN $2 AND $3
        GROUP BY data_time_agg
        ORDER BY data_time_agg
        """
        )

    def _build_query_scalar_full(self, table_name):
        "Build query for scalar attribute data, no aggregation"

        # We don't want postgres to give us actual booleans, let's cast them to
        # int (false = 0, true = 1) instead. TODO: less hacky way
        if "boolean" in table_name:
            cast = "::int"
        else:
            cast = ""

        # TSV values (according to
        # https://www.loc.gov/preservation/digital/formats/fdd/fdd000533.shtml)
        # shouldn't contain some characters, like newlines and tabs.
        # This also goes for JSON, so better fix that here to prevent problems.
        if "string" in table_name:
            convert = r"replace(replace(replace({}, E'\n', '\n'), E'\t', '\t'), E'\r', '\r')"
        else:
            convert = "{}"

        columns = ", ".join([
            f"to_char(data_time, '{self.DATE_FORMAT}')",
            f"{convert.format('value_r')}{cast}",
            f"{convert.format('value_w')}{cast}",
            "quality",
            "att_error_desc_id"
        ])

        # Combine three queries, we get the last value before the time window,
        # all within the time window, and the first value after.
        #
        # TODO disabled the pre and post queries for now as it caused some
        # queries to be very slow.
        # Needs more investigation, maybe there is a better way.
        return dedent(f"""
        SELECT {columns} FROM (
           -- --Query for the last point *before* our time window
           -- (SELECT DISTINCT ON (att_conf_id) * from {table_name}
           --   WHERE att_conf_id = $1 AND data_time < $2
           --   ORDER BY att_conf_id, data_time DESC LIMIT 1)
           -- UNION ALL

           -- Query for data within the time window
           (SELECT * FROM {table_name}
               WHERE att_conf_id = $1 AND data_time BETWEEN $2 AND $3
               ORDER BY data_time)

           -- UNION ALL
           -- -- Query for the first point *after* the time window
           -- (SELECT DISTINCT ON (att_conf_id) * FROM {table_name}
           --   WHERE att_conf_id = $1 AND data_time > $3
           --   ORDER BY att_conf_id, data_time ASC LIMIT 1)
        ) AS foo
        """)

    def _build_query_spectrum_full(self, table_name):
        columns = "data_time, value_r, value_w, quality, att_error_desc_id"
        return f"""
        SELECT {columns} FROM {table_name}
            WHERE att_conf_id = $1
            AND data_time BETWEEN $2 AND $3
        ORDER BY data_time
        """

    def _build_query_spectrum_agg(self, table_name, agg_size, agg_func):
        return dedent(
            f"""
        SELECT
            to_char(time_bucket('{agg_size} seconds', data_time)
                + '{agg_size // 2} seconds',
                '{self.DATE_FORMAT}') as agg_time,
            array_to_json(array_column_{agg_func}(array_agg(value_r))) as agg_value_r,
            NULL,
            count(*) as n_points,
            count(att_error_desc_id) as n_errors
        FROM {table_name}
        WHERE att_conf_id = $1
            AND data_time BETWEEN $2 AND $3
            AND value_r IS NOT NULL
        GROUP BY agg_time
        ORDER BY agg_time
        """
        )


# TODO: support more types
FAKE_ATTR_FORMATS = ["SCALAR"]
FAKE_ATTR_TYPES = ["DEV_DOUBLE"]


class FakeHDBPPTimescaleConnection(HDBPPTimescaleConnection):

    """
    A fake connection that delivers fake random data, for local testing without a DB.
    Interface should be identical to the real thing. The data delivered is not
    intended to be "realistic", just correctly formatted.

    Note that this class is also used by some tests.
    """

    FAKE_CONTROLSYSTEMS = {
        "hdb": [
            "fake.control.system.1:10000",
            "fake.control.system.2:10000",
            "fake.control.system.3:10000",
        ]
    }

    FAKE_ATTRIBUTES = {
        db: {
            (10 * i + d): (
                cs,
                f"fake/device/{d}/attr_{attr_format.lower()}_{attr_type.lower()}",
                attr_format,
                attr_type,
            )
            for i, cs in enumerate(controlsystems)
            for d in range(10)
            for attr_format, attr_type in product(FAKE_ATTR_FORMATS, FAKE_ATTR_TYPES)
        }
        for db, controlsystems in FAKE_CONTROLSYSTEMS.items()
    }

    FAKE_HISTORY = {
        db: {
            att_conf_id: [
                {
                    "att_history_event_id": att_conf_id * 100 + i,
                    "att_conf_id": att_conf_id,
                    "event_time": (datetime.now() - timedelta(seconds=randint(0, 365 * 24 * 3600))).astimezone(),
                    "event": choice(["stop", "start", "pause", "add", "remove", "crash"]),
                }
                for i in range(randint(10, 90))
            ]
            for att_conf_id in attrs
        }
        for db, attrs in FAKE_ATTRIBUTES.items()
    }

    FAKE_PARAMETERS = {
        db: {
            att_conf_id: [
                {
                    "db": db,
                    "att_conf_id": att_conf_id,
                    "recv_time": (datetime.now() - timedelta(seconds=randint(0, 365 * 24 * 3600))).astimezone(),
                    "label": "some label",
                    "unit": "km",
                    "standard_unit": "No standard unit",
                    "display_unit": "No display unit",
                    "format": "%.3f",
                    "archive_rel_change": 1,
                    "archive_abs_change": "Not specified",
                    "archive_period": "Not specified",
                    "description": "Some description",
                    "details": None,
                }
                for i in range(randint(1, 20))
            ]
            for att_conf_id in attrs
        }
        for db, attrs in FAKE_ATTRIBUTES.items()
    }

    async def connect(self):
        self.logger.warning("Using fake database, only intended for testing!")

    async def get_controlsystems(self):
        return [{"cs_name": cs} for cs in self.FAKE_CONTROLSYSTEMS[self.database]]

    async def search_attributes(self, cs: str, phrase: str):
        return (
            {
                "db": self.database,
                "att_conf_id": att_conf_id,
                "type": attr_type,
                "format": attr_format,
                "write": "READ",
                "name": attr_name,
                "att_name": f"tango://{attr_cs}/{attr_name}",
                "cs_name": attr_cs,
            }
            for att_conf_id, (attr_cs, attr_name, attr_format, attr_type) in self.FAKE_ATTRIBUTES[
                self.database
            ].items()
            if (cs == attr_cs and fnmatch.fnmatch(attr_name, phrase))
        )

    async def get_attributes(self, att_conf_ids):
        return (
            {
                "db": self.database,
                "att_conf_id": att_conf_id,
                "table_name": f"att_{att_format.lower()}_{att_type.lower()}",
                "type": att_type,
                "format": att_format,
                "write": "READ",
                "cs_name": attr_cs,
                "name": att_name,
                "att_name": f"tango://{attr_cs}/{att_name}",
            }
            for att_conf_id, (attr_cs, att_name, att_format, att_type)
            in self.FAKE_ATTRIBUTES[self.database].items()
            if att_conf_id in att_conf_ids
        )

    async def get_history(self, att_conf_ids, start_time=None, end_time=None):
        return (
            event
            for att_conf_id in att_conf_ids
            for event in self.FAKE_HISTORY[self.database].get(att_conf_id, [])
            if start_time < event["event_time"] < end_time
        )

    async def get_parameters(self, att_conf_id: int, start_time=None, end_time=None):
        all_params = self.FAKE_PARAMETERS[self.database].get(att_conf_id, [])
        previous = [param for param in all_params if param["recv_time"] < start_time]
        return previous[-1:] + [
            params
            for params in all_params
            if params["att_conf_id"] == att_conf_id and start_time < params["recv_time"] < end_time
        ]

    async def get_attribute_count(
        self,
        att_conf_id: int,
        table_name: str,
        start_time: datetime,
        end_time: datetime,
        agg_limit: int,
        agg_buckets: int = 2000,
        estimate: bool = True,
    ):
        interval = end_time - start_time
        interval_points = int(interval.total_seconds() / 10)
        return interval_points

    async def get_attribute_data_tsv(
        self,
        filename,
        att_conf_id,
        table_name,
        start_time,
        end_time,
        agg_size,
        agg_func="avg",
    ):
        interval = end_time - start_time
        expected_count = int(interval.total_seconds() / 10)
        if agg_size:
            step_time = max(agg_size, (interval / expected_count).total_seconds())
        else:
            step_time = (interval / expected_count).total_seconds()
        _, att_name, *_ = self.FAKE_ATTRIBUTES[self.database][att_conf_id]
        # Some variation in the data, that is persistent for each attribute
        some_number = hash(att_name)
        period = (some_number % 79 + 1) * 60
        amplitude = some_number % 51 + 1
        offset = ((some_number % 13) / 13 - 0.5) * amplitude * 2
        start_t = start_time.timestamp()
        count = int(interval.total_seconds() / step_time)
        timestamps = (start_t + i * step_time for i in range(count))
        # TODO: come up with some funner way of generating data. Note that it should
        # be deterministic; same attribute and range should give the same result, or it
        # will just be confusing.
        values = (sin((start_t + i * step_time) / period) * amplitude + offset
                  for i in range(count))
        # columns = "data_time, value_r, value_w, quality, att_error_desc_id"
        rows = (
            (
                datetime.utcfromtimestamp(t).strftime("%Y-%m-%d %H:%M:%S.%f+00"),
                value,
                None,
                randint(1, 100),
                randint(1, 10) if random() < 0.1 else None,
            )
            for t, value in zip(timestamps, values)
        )
        with open(filename, "wb") as f:
            for t, rv, wv, quality, error in rows:
                f.write(f"{t}\t{rv}\t{wv}\t{quality}\t{error}\n".encode())
