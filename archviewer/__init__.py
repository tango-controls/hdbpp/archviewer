def calculate_aggregation_size(agg_limit, agg_buckets, count, start_time, end_time):
    """
    Return a "reasonable" aggregation bucket size in seconds.
    0 means no aggregation is needed.
    """
    if agg_limit and count >= agg_limit:
        # Data too big, use timescale bucket function to aggregate
        time_window = end_time - start_time
        # Calculate a suitable aggregation size in seconds
        bucket_size = time_window.total_seconds() / agg_buckets
        # Buckets could be small, let's work in milliseconds
        bucket_size = int(bucket_size * 1000)
        ndigits = len(str(bucket_size)) - 1
        # "round" to one significant digit, to make it more readable
        return round(bucket_size, -ndigits) / 1000
        # TODO: for larger agg_size, round to even minutes, hours, days..?
    else:
        return 0
