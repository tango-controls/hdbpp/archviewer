"""
Entry point
"""

import asyncio

from fastapi import FastAPI
from fastapi.responses import PlainTextResponse
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.gzip import GZipMiddleware
from fastapi.middleware.cors import CORSMiddleware

from .routes import router
from .config import get_config

app = FastAPI()
app.include_router(router)
app.mount("/static", StaticFiles(directory="static"), name="static")
app.add_middleware(GZipMiddleware, minimum_size=10000)
app.add_middleware(
    CORSMiddleware,
    allow_origins=get_config().cors_allow_origins.split(","),
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.exception_handler(RuntimeError)
async def runtime_error_handler(request, exc):
    return PlainTextResponse(str(exc), status_code=503)


@app.exception_handler(asyncio.exceptions.TimeoutError)
async def timeout_error_handler(request, exc):
    return PlainTextResponse(f"Timeout: {exc}", status_code=503)
