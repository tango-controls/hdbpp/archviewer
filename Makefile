# This makefile contains some helpful development commands

install:
	python3 -m venv env &&\
	env/bin/pip install -e .

install-test:
	env/bin/pip install -e .[tests] &&\
	env/bin/playwright install firefox

.PHONY: test
test:
	env/bin/pytest --browser firefox


# Helpers to switch between different envs
prod:
	ln -sf .env.prod .env

dev:
	ln -sf .env.dev .env

fake:
	ln -sf .env.fake .env

local:
	ln -sf .env.local .env


# Compile the frontend
build:
	./node_modules/.bin/esbuild frontend/index.js --bundle --outfile=static/out.js

watch:
	./node_modules/.bin/esbuild frontend/index.js --bundle --outfile=static/out.js --sourcemap --watch

# Run locally
run:
	env/bin/uvicorn archviewer.app:app --port 8009
