import * as d3 from "d3";
import { debounce } from "./util.js";
import { makeAttrKey } from "./store/store.js";

// TODO make weeks start on monday instead of sundays.
const formatMillisecond = d3.timeFormat(".%L"),
      formatSecond = d3.timeFormat(":%S"),
      formatMinute = d3.timeFormat("%H:%M"),
      formatHour = d3.timeFormat("%H:00"),
      formatDay = d3.timeFormat("%a %d"),
      formatWeek = d3.timeFormat("%a %d"),
      formatMonth = d3.timeFormat("%B"),
      formatYear = d3.timeFormat("%Y");


const ATTRIBUTE_QUALITY = [
    "VALID",
    "INVALID",
    "ALARM",
    "CHANGING",
    "WARNING",
]

function timeFormat(date) {
  return (d3.timeSecond(date) < date ? formatMillisecond
      : d3.timeMinute(date) < date ? formatSecond
      : d3.timeHour(date) < date ? formatMinute
      : d3.timeDay(date) < date ? formatHour
      : d3.timeMonth(date) < date ? (d3.timeWeek(date) < date ? formatDay : formatWeek)
      : d3.timeYear(date) < date ? formatMonth
      : formatYear)(date);
}

// Y axis ticks is a tricky thing, especially log.
// "e" means always exponent notation, and the tilde means drop all trailing zeros.
// I think it's the best one I've come upon so far, but more experimentation is probably good.
const Y_TICK_FORMAT = "~e";
const Y_TICK_FORMAT_LOG = "~e";
// These numbers arrived at by experimentation. May need further tweaking.
const TICK_SIZE = 10;
const LOG_TICKS = 3;

// Keep track of a single attribute
class PlotAttribute {

    WRITABLE = new Set(["WRITE", "READ_WRITE", "READ_WITH_WRITE"]);

    constructor (info, canvas, padding) {
        this.info = info;
        this.limits = [];
        this.data = [];
        this.aggSize = null;
        this.canvas = canvas;
        this.context = this.canvas.getContext("2d");

        // Make sure we have some padding at the top and bottom
        // Also, half a pixel offset makes thin lines somewhat sharper
        // since we also round coordinates to closest pixel
        this.context.translate(-0.5, padding - 0.5)

        this.style = "line";
        this.padding = padding
    }

    update (data, aggSize) {
        this.data = data;
        this.aggSize = aggSize;
    }

    draw (scale, alpha = 1.0, showGaps = false) {
        const t0 = Date.now();
        this.context.clearRect(0, -this.padding, this.canvas.width, this.canvas.height + 2 * this.padding);
        this.canvas.style.opacity = alpha;
        if (this.info.lineStyle === "p") {
            // Points
            this.#drawPoints(this.info.valueKey || "value_r", scale, alpha);
        } else {
            this.#drawLine(this.info.valueKey || "value_r", scale, alpha, showGaps);
        }
        // console.log(`Drawing attr ${this.info.att_conf_id} (${this.data.length} points) took ${Date.now() - t0} ms`);
    }

    #drawLine (valueKey, [xScale, yScale], alpha = 1.0, showGaps = false) {
        // TODO: setting opacity could be done without redraw
        if (this.data && this.data.length > 0) {

            this.context.strokeStyle = this.info.color;

            // 1 pixel line width is a bit thin, let's add some width
            const lineWidth = this.info.lineWidth <= 1? 1.2 : this.info.lineWidth;
            this.context.lineWidth = lineWidth;
            if (this.info.lineWidth > 1) {
                // For thicker lines, it looks a lot better with round endings
                this.context.lineJoin = "round";
                this.context.lineCap = "round";
            } else {
                // For thin lines it makes no difference visually, and this may be faster
                this.context.lineJoin = "butt";
                this.context.lineCap = "butt";
            }

            switch (this.info.lineStyle) {
            case "d":  // Dashes
                this.context.setLineDash([lineWidth * 7, lineWidth * 3]);
                break;
            case "s":  // Short dashes
                this.context.setLineDash([lineWidth * 3, lineWidth * 3]);
                break;
            default:
                this.context.setLineDash([]);
            }

            /*
              "Gaps" are defined as points where either:
              - there is no value for the attribute, usually because
                there was an error reported.
              - the value can't be displayed because it's out of
                the axis' domain (e.g <=0 for a log axis)
            */

            if (this.aggSize) {
                const startIndex = 0,
                      endIndex = this.data.length;
                this.context.beginPath();
                this.context.moveTo(
                    Math.round(xScale(this.data[startIndex].data_time)),
                    Math.round(yScale(this.data[startIndex][valueKey]))
                );
                let gap = false;
                for (var i = startIndex + 1; i < endIndex; i++) {
                    const d = this.data[i];
                    const value = d[valueKey]
                    const x = xScale(d.data_time);
                    const y = yScale(value);
                    if (showGaps && isNaN(y)) {
                        gap = true;
                    } else {
                        if (gap) {
                            this.context.moveTo(Math.round(x), Math.round(y));
                            gap = false;
                        } else {
                            this.context.lineTo(Math.round(x), Math.round(y));
                        }
                    }
                }
            } else {

                const startIndex = 0,
                      endIndex = this.data.length;
                this.context.beginPath();
                this.context.moveTo(
                    Math.round(xScale(this.data[startIndex].data_time)),
                    Math.round(yScale(this.data[startIndex][valueKey]))
                );
                let gap = false;
                let ys = [];
                let lastY = null
                let lastX = 0;
                for (var i = startIndex + 1; i < endIndex; i++) {
                    var d = this.data[i];
                    const value = d[valueKey];
                    const y = yScale(value);
                    const x = xScale(d.data_time)
                    const roundX = Math.round(x);
                    if (roundX === Math.round(lastX)) {
                        // Accumulate points that end up in the same pixel column
                        // without drawing them yet.
                        if (ys.length == 0 && !isNaN(lastY)) {
                            ys.push(lastY);
                        }
                        if (!isNaN(y)) {
                            ys.push(y);
                        }
                        lastY = y;
                        if (i < endIndex - 1) {
                            // Unless this is the very last point, just keep going
                            // Otherwise we need to draw.
                            continue;
                        }
                    }
                    // Moved to a new pixel column
                    if (ys.length > 0) {
                        const tmpX = Math.round(lastX);
                        if (ys.length === 1) {
                            // This can happen if there are error points
                            this.context.lineTo(tmpX, Math.round(ys[0]));
                        } else {
                            // Represent any accumulated previous points as a vertical line
                            // covering all the y values.
                            const yMin = Math.min.apply(Math, ys);
                            const yMax = Math.max.apply(Math, ys);
                            this.context.moveTo(tmpX, Math.round(yMax));
                            this.context.lineTo(tmpX, Math.round(yMin));
                        }
                        if (isNaN(lastY) && showGaps) {
                            gap = true;
                        } else {
                            this.context.moveTo(Math.round(lastX), Math.round(lastY));
                        }
                        ys = [];
                    }
                    // Draw the new point
                    if (isNaN(y) && showGaps) {
                        gap = true;
                    } else {
                        if (gap) {
                            this.context.moveTo(Math.round(x), Math.round(y));
                            gap = false;
                        } else {
                            this.context.lineTo(Math.round(x), Math.round(y));
                        }
                    }
                    lastX = x;
                    lastY = y;
                }
            }
            this.context.stroke();  // Actually draw the line
        }
    }

    #drawPoints (valueKey, [xScale, yScale], alpha = 1.0) {
        // this.canvas.setAttribute("transform", null);
        if (this.data && this.data.length > 0) {
            this.context.fillStyle = this.info.color;
            this.context.lineWidth = 0;
            const pointDiameter = this.info.lineWidth || 1;
            const offsetX = -pointDiameter / 2,
                  offsetY = -pointDiameter / 2 + this.padding;
            if (this.aggSize) {
                // #draw it
                for (var i = 1; i < this.data.length; i++) {
                    var d = this.data[i];
                    const x = xScale(d.data_time) + offsetX;
                    const y = yScale(d[valueKey]) + offsetY;
                    this.context.fillRect(x, y, pointDiameter, pointDiameter);
                }
            } else {
                const startIndex = 0;
                const endIndex = this.data.length;
                for (var i = startIndex; i < endIndex; i++) {
                    const d = this.data[i];
                    const value = d[valueKey];
                    if (d && !isNaN(value)) {
                        const x = xScale(d.data_time) + offsetX;
                        const y = yScale(value) + offsetY
                        this.context.fillRect(x, y, pointDiameter, pointDiameter);
                    }
                }
            }
        }
    }

    getNearestPoint (x, xScale) {
        const [start, end] = xScale.domain();
        if (this.data.length <= 2) {
            // Need to consider that the first and/or last point may lie outside
            // of the visible range. If no points are visible, there is nothing to return;
            if (this.data.filter(d => d.data_time >= start
                                 && d.data_time <= end).length === 0) {
                return null;
            }
        }
        const bisectDate = d3.bisector(d => d.data_time).left;
        const date = xScale.invert(x);
        const index = bisectDate(this.data, date);
        const next = this.data[index];
        if (index === 0) {
            return next;
        } else {
            const prev = this.data[index - 1];
            if (next === undefined || next.data_time > end) {
                return prev;
            }
            const t = date.getTime();
            const dRight = next.data_time.getTime() - t;
            const dLeft = t - prev.data_time.getTime();
            if (dRight < dLeft) {
                return next;
            } else {
                return prev;
            }
        }
    }

}

// Plot multiple attribute over a time interval, across several Y axes
export class Plot {

    constructor (container, timeRange, axes, rangeCallback, axisCallback,
                 initialData) {

        this.showHoverInfoDebounced = debounce(this.#showHoverInfo.bind(this), 100);

        this.container = container;  // Element to put the plot in
        this.axisCallback = axisCallback;  // called on changes to an Y axis

        this.axes = axes
        this.attributes = {}

        this.selected = new Set();
        this.showGaps = false;

        this.margin = {
            top: 30,
            right: 30,
            bottom: 30,
            left: 60
        };
        this.width = container.clientWidth - this.margin.left - this.margin.right;
        this.height = container.clientHeight - this.margin.top - this.margin.bottom;

        // Container for the individual canvas elements where we will #draw the plots
        // (using PlotAttribute)
        this.canvasContainer = d3.select(container)
            .append("div")
            .classed("canvas-container", true)
            .style("position", "relative")
            .style("width", "0")
            .style("top", this.margin.top + "px")
            .style("left", this.margin.left + "px");

        const totalWidth = this.width + this.margin.left + this.margin.right,
              totalHeight = this.height + this.margin.top + this.margin.bottom;

        // This SVG element will contain the rest of the plot elements, such as
        // axes and so on. It will be overlaid on top of the canvases.
        this.SVG = d3.select(container)
            .append("svg")
            .attr("xmlns", "http://www.w3.org/2000/svg")
            .attr("version", "1.1")
            .attr("viewBox", `0 0 ${totalWidth} ${totalHeight}`)
            .attr("width", totalWidth + "px")
            .attr("height", totalHeight + "px")
            .style("position", "absolute")
            .style("top", 0)
            .style("left", 0)

        this.mainGroup = this.SVG
            .append("g")
            .attr("transform",
                  "translate(" + this.margin.left + "," + this.margin.top + ")");

        this.legend = this.mainGroup.append("g")
            .classed("legend", true)

        // Container for markers such as the dots marking the points nearest to the cursor
        this.markers = this.mainGroup.append("g")
        this.markers.append("line")
            .attr("stroke", "black")
            .classed("vertical", true)
        this.markers.append("text")
            .classed("vertical", true)
            // .attr("font-size", "small")
            .text("")
        this.markers.append("line")
            .attr("stroke", "black")
            .classed("horizontal", true)
        this.markers.append("text")
            .classed("horizontal", true)
            .classed("left", true)
            // .attr("font-size", "small")
            .text("")
        this.markers.append("text")
            .classed("horizontal", true)
            .classed("right", true)
            // .attr("font-size", "small")
            .text("")

        // "history" event markers (vertical lines with a icon at the top
        this.historyEl = this.mainGroup.append("g")
            .classed("history", true)

        // Container for the info "popup" about nearest point
        this.infoContainer = d3.select(container)
            .append("div")
            .classed("info-container", true)
            .style("pointer-events", "none")

        // Y axis objects, stored by axis id
        this.yAxes = {};
        this.#createYAxes(axes);

        // X axis
        const xZoomed = (event) => {
            // Note: we don't actually change the x scale here, we
            // just rescale the axis based on the original scale. This
            // is because the event transform is cumulative.
            const x = event.transform.rescaleX(this.x);
            this.xAxis = d3.axisBottom(x).ticks(5).tickFormat(timeFormat);
            this.xAxisEl
                .call(this.xAxis)
                .call(g => g.select(".domain").remove())
            this.xGridEl.call(makeXGrid(x));
            if (event.transform !== d3.zoomIdentity) {
                // We don't call callback when resetting the view
                const [start, end] = x.domain();
                rangeCallback([start, end]);
                this.#updateHistoryEventPositions();
            }
            this.#clearMarkers();
            this.#redraw();
        }

        // X zoom/pan behavior
        this.xZoom = d3.zoom()
            .extent([[0, 0], [this.width, 0]])
            .on("zoom", xZoomed);

        const {start, end} = timeRange;
        this.x = d3.scaleTime()
            .domain([start, end])
            .range([0, this.#getXAxisWidth()]);
        this.xAxis = d3.axisBottom(this.x)
            .ticks(5).tickFormat(timeFormat);

        this.xAxisEl = this.mainGroup.append("g")
            .classed("x-axis", true)
            .attr("transform", "translate(0," + this.height + ")")
            .call(this.xAxis)
            .call(g => g.select(".domain").remove())  // remove axis line

        const makeXGrid = (x) => {
            return d3.axisBottom(x)
                .ticks(5)
                .tickSize(this.height)
                .tickFormat("")
        }

        this.xGridEl = this.mainGroup.append("g")
            .attr("class", "grid bottom")
            .call(makeXGrid(this.x));

        // Overlay a rect on the axis to use for zoom events
        this.xRect = this.mainGroup
            .append("rect")
            .attr("y", this.height)
            .attr("width", this.#getXAxisWidth())
            .attr("height", 30)
            .style("pointer-events", "all")
            .style("fill-opacity", 0)
            .style("cursor", "ew-resize")
            .call(this.xZoom);

        // The "overlay" is an invisible element covering the whole plot.
        // We will use it for attaching mouse pointer events.
        this.overlay = this.mainGroup.append("rect")
            .attr("id", "overlay")
            .attr("width", this.#getXAxisWidth())
            .attr("height", this.height)

            .style("fill", "none")
            .style("pointer-events", "all")

            .call(this.xZoom)

        // Show some info about the points closest to the pointer
            .on("mousemove", this.#onHover.bind(this))
            .on("mouseenter", () => {
                this.markers.style("display", null);
                this.infoContainer.style("display", null)
            })
            .on("mouseleave", () => {
                this.markers.style("display", "none");
                this.infoContainer.style("display", "none");
            });

        if (initialData) {
            const {attributes, info, history, parameters, selected, data} = initialData;
            if (attributes && info) {
                this.setAttributes(attributes, info);
            }
            if (history) {
                this.setHistory(history);
            }
            // if (parameters) {
            //     this.setParameters(parameters);
            // }
            if (selected) {
                this.setSelected(selected);
            }
            if (data && attributes) {
                for (let attr of attributes) {
                    const key = makeAttrKey(attr);
                    this.updateAttribute(key,
                                         data[key].data,
                                         data[key].aggSize);
                }
            }
            this.showGaps = initialData.showErrorGaps;
        }

    }

    // ==== Private methods ====

    // The "legend" displays all attributes and their respective colors
    // to make the plot readable in itself.
    #buildLegend () {

        this.axes.forEach(axis => {

            const axisAttributes = Object.values(this.attributes)
                  .filter(a => a.info.axis === axis.id);

            const className = `axis-${axis.id}`;

            this.legend.selectAll(`circle.${className}`)
                .data(axisAttributes, a => a.info.db + a.info.att_conf_id)
                .join(
                    enter => enter.append("circle")
                        .attr("cx", () => (axis.side === "left" ? 20 : this.width - 60))
                        .attr("cy", (a, i) => 20 + i * 25)
                        .attr("r", 7)
                        .attr("stroke", "white")
                        .attr("fill", a => a.info.color)
                        .classed(className, true),
                    update => update.attr("cy", (a, i) => 20 + i * 25),
                    exit => exit.remove(),
                );

            this.legend.selectAll(`text.${className}`)
                .data(axisAttributes, a => a.info.db + a.info.att_conf_id)
                .join(
                    enter => {
                        const t = enter.append("text")
                              .attr("text-anchor", () => axis.side === "left"? "start" : "end")
                              .attr("y", (a, i) => 20 + i * 25)
                              .attr("dominant-baseline", "middle")
                              .classed(className, true);
                        // Add two tspans into the text element, the second drawn on top
                        // of the other, to create an "outline" effect. This is for better
                        // readability if we overlap something
                        t.append("tspan")
                            .attr("x", () => (axis.side === "left" ? 30 : this.width - 70))
                            .text(a => a.info.name || "...")
                            .attr("stroke-width", "3px")
                            .attr("stroke", "white")
                        t.append("tspan")
                            .attr("x", () => (axis.side === "left" ? 30 : this.width - 70))
                            .text(a => a.info.name || "...")
                        return t;
                    },
                    update => {
                        update.attr("y", (a, i) => 20 + i * 25);
                        update.selectAll("tspan").text(a => a.info.name);
                        return update;
                    },
                    exit => exit.remove(),
                );

        });

    }

    #getYAxisPosition (axisId) {
        for (let i = 0; i < this.axes.length; i++) {
            const axis = this.axes[i];
            if (axis.id === axisId) {
                if (axis.side === "left") {
                    return -60 + 30 * i;
                } else {
                    return this.#getXAxisWidth() + 30 * (i - 1);
                }
            }
        }
    }

    #getXAxisWidth () {
        const nYAxes = this.axes.length;
        return this.width - 30 * (nYAxes - 1);
    }

    // Y axes from scratch.
    // TODO: this may seem a bit inefficient, perhaps there is a good way
    // to update them instead. But it's not so easy and this seems to work well.
    #createYAxes (axes) {

        for (let yAxis of Object.values(this.yAxes)) {
            yAxis.element.remove();
            yAxis.label.remove();
            yAxis.grid && yAxis.grid.remove();
            yAxis.rect.remove();
        }

        this.#clearMarkers();

        this.yAxes = {}
        for (const axis of axes) {
            var yAxis = { ...axis };
            this.yAxes[axis.id] = yAxis;

            const yZoomed = (yAxis, event) => {
                const yScale = event.transform.rescaleY(yAxis.scale);
                if (yAxis.side === "left") {
                    if (axis.scale === "log") {
                        yAxis.axis = d3.axisLeft(yScale)
                            .ticks(LOG_TICKS)
                            .tickSize(-TICK_SIZE)
                            .tickFormat(d3.format(Y_TICK_FORMAT_LOG));
                        yAxis.grid.call(makeYGrid(yScale));
                    } else {
                        yAxis.axis = d3.axisLeft(yScale)
                            .ticks(5)
                            .tickSize(-TICK_SIZE)
                            .tickFormat(d3.format(Y_TICK_FORMAT));
                        yAxis.grid.call(makeYGrid(yScale));
                    }
                } else {
                    yAxis.axis = d3.axisRight(yScale)
                        .ticks(5)
                        .tickSize(-TICK_SIZE)
                        .tickFormat(d3.format(Y_TICK_FORMAT));
                }
                yAxis.element.call(yAxis.axis);

                const xScale = this.xAxis.scale(),
                      scale = [xScale, yScale];
                const attrs = this.#getAxisAttributes(axis.id);
                attrs.forEach(attr => attr.draw(scale, 1, this.showGaps));
                axis.limits = yScale.domain();
                this.axisCallback(axis.id, yAxis.axis.scale().domain());
            }

            yAxis.zoom = d3.zoom()
                .extent([[0, 0], [30, this.height]])
                .on("zoom", yZoomed.bind(this, yAxis));

            yAxis.scale = (axis.scale === "log"? d3.scaleLog() : d3.scaleLinear())
                .domain(axis.limits || [-1, 1])
                .range([this.height, 0]);

            const x = this.#getYAxisPosition(axis.id);

            const makeYGrid = (y) => {
                if (axis.scale === "log") {
                    return d3.axisLeft(y)
                        .ticks(LOG_TICKS)
                        .tickSize(-this.width + 30)
                        .tickFormat("")
                } else {
                    return d3.axisLeft(y)
                        .ticks(5)
                        .tickSize(-this.width + 30)
                        .tickFormat("")
                }
            }

            if (axis.side === "left") {
                // D3 axis that manages the range/domain of the axis
                if (axis.scale === "log") {
                    yAxis.axis = d3.axisLeft(yAxis.scale)
                        .ticks(LOG_TICKS)
                        .tickSize(-TICK_SIZE)
                        // .tickFormat(d3.format(Y_TICK_FORMAT))
                        .tickFormat(d3.format(Y_TICK_FORMAT_LOG));
                    // .tickFormat(d3.format(Y_TICK_FORMAT));
                } else {
                    yAxis.axis = d3.axisLeft(yAxis.scale)
                        .ticks(5)
                        .tickSize(-TICK_SIZE)
                        .tickFormat(d3.format(Y_TICK_FORMAT));
                }

                // yAxis.axis = d3.axisLeft(yAxis.scale)
                //     .ticks(5)
                //     .tickSize(-TICK_SIZE)
                //     .tickFormat(d3.format(Y_TICK_FORMAT));
                // Container for drawing the D3 axis; ticks and so on
                yAxis.element = this.mainGroup.append("g")
                    .classed(`y-axis-${axis.id}`, true)
                    .call(yAxis.axis)
                // Element to contain "labels" for each attribute on the axis
                yAxis.label = this.mainGroup
                    .append("text")
                    .attr("dy", -15)
                    .attr("dx", -this.margin.left + 10)
                    // .attr("font-size", "small")
                    .attr("text-anchor", "start")
                // Axis grid (only on left axis for now)
                yAxis.grid = this.mainGroup.append("g")
                    .attr("class", "grid left")
                    .call(makeYGrid(yAxis.scale))
                    // .selectAll(".tick line")
                    // .attr("stroke", "#0003")
            }
            else {
                yAxis.axis = d3.axisRight(yAxis.scale)
                    .ticks(5)
                    .tickSize(-TICK_SIZE)
                    .tickFormat(d3.format(Y_TICK_FORMAT));
                yAxis.element = this.mainGroup.append("g")
                    .attr("transform", "translate(" + x + " 0)")
                    .call(yAxis.axis)
                yAxis.label = this.mainGroup.append("text")
                    // .attr("font-size", "small")
                    .attr("transform", `translate(${x + this.margin.right + 20} -15)`)
                    .attr("text-anchor", "end")
            }

            // Display unit (if any)
            yAxis.label
                .append("tspan")
                .classed(`y-axis-${axis.id}`, true)
                .classed("unit", true)
                .text(d => (
                    (axis.label? axis.label : "") +
                        (axis.unit? ` [${axis.unit}]` : "")
                ))
                .attr("fill", "black");

            // Element to capture pan/zoom events on the axis
            yAxis.rect = this.mainGroup
                .append("rect")
                .attr("x", x)
                .attr("width", 60)
                .attr("height", this.height)
                .style("fill-opacity", 0)
                .style("pointer-events", "all")
                .style("cursor", "ns-resize")
                .call(yAxis.zoom);
        }
    }

    #clearHoverInfo() {
        this.infoContainer.selectAll("div").remove();
    }

    #showHoverInfo(x, y, visiblePoints) {
        // Display "popup" only for closest point to the mouse pointer.
        // Perhaps show info for all points later, but that requires figuring out
        // a way to prevent them from overlapping.

        let minDistance, closestPoint = [];
        for (const [a, p] of visiblePoints) {
            const py = this.yAxes[a.info.axis].axis.scale()(p.value_r),
                  px = this.xAxis.scale()(p.data_time);
            const d = Math.sqrt(Math.pow(x - px, 2) + Math.pow(y - py, 2));
            if (minDistance === undefined || d < minDistance) {
                minDistance = d;
                closestPoint = [[a, p]];
            }
        }

        const xFormat = d3.timeFormat("%Y-%m-%d %H:%M:%S.%L");
        const yFormat = d3.format("g");
        function makeInfo([attr, point]) {
            // TODO this is a pretty large info popup, maybe there's a better way
            // TODO: show unit, etc
            if (attr.aggSize) {
                // Aggregated point
                let info = (`<span style="color: ${attr.info.color};">●</span>`
                            + `${attr.info.name}`
                            + `<br>Read value: ${yFormat(point.value_r)}`
                            + (!isNaN(point.value_w)? `<br>Write value: ${yFormat(point.value_w)}` : "")
                            + `<br>Time: ${xFormat(point.data_time)}`
                            + `<br>Agg. points: ${point.n_points}`);
                if (point.n_errors) {
                    info += `<br>Errors: ${point.n_errors}`;
                }
                return info;
            }
            // Single point
            return (`<span style="color: ${attr.info.color};">●</span>`
                    + `${attr.info.name}`
                    + `<br>Read value: ${yFormat(point.value_r)}`
                    + (!isNaN(point.value_w)? `<br>Write value: ${yFormat(point.value_w)}` : "")
                    + (point.quality > 0? `<br>Quality: ${ATTRIBUTE_QUALITY[point.quality]}` : "")
                    + `<br>Time: ${xFormat(point.data_time)}`);
        }

        const self = this;

        this.infoContainer
            .selectAll("div")
            .data(closestPoint)
            .join("div")
            .html(makeInfo)
            .attr("class", "hover-info")
            .style("position", "absolute")
            .style("border", ([a, p]) => `1px solid ${a.info.color}`)
            .style("white-space", "nowrap")
            .each(function ([attr, point]) {
                // Figure out a placement that is least likely to poke outside
                // of the plot area. We simply "anchor" the popup in the corner
                // closest to the edge, so that it "hangs" in the opposite direction.
                const x = self.xAxis.scale()(point.data_time),
                      y = self.yAxes[attr.info.axis].axis.scale()(point.value_r),
                      el = d3.select(this);
                // Horizontal position
                if (x / self.width > 0.5) {
                    el.style("right", (self.width + self.margin.right - x + 5) + "px");
                } else {
                    el.style("left", (self.margin.left + x + 5) + "px");
                }
                // Vertical position
                if (y / self.height > 0.5) {
                    el.style("top", null);
                    el.style("bottom", (self.margin.bottom + self.height - y + 5) + "px")
                } else {
                    el.style("top", (self.margin.top + y + 5) + "px")
                    el.style("bottom", null);
                }

            })
    }
    
    // Display mouse hover markers at the closest points
    lastVisiblePoints = [];
    #onHover (event) {

        const x = event.offsetX - this.margin.left,
              y = event.offsetY - this.margin.top,
              attributes = Object.values(this.attributes),
              yAxes = this.yAxes,
              xScale = this.xAxis.scale();
        const points = attributes
              .map(a => [a, a.getNearestPoint(x, this.xAxis.scale())])
              .filter(a => a !== null)
              .filter(d => d[1] && d[1].data_time !== null)  // TODO do this properly

        // mark closest point to cursor for each attribute
        const visiblePoints = points.filter(([attr, point]) => {
            const yAxis = this.yAxes[attr.info.axis],
                  [min, max] = yAxis.axis.scale().domain();
            const height = max - min;
            // TODO adding 1% margin here is a bit of a hack. Perhaps we should
            // instead put some padding on the axis itself.
            return (point.value_r >= (min - height * 0.01)
                    && point.value_r <= (max + height)) ;
        })
        // If the list of nearest points is different from last time, we update
        // the point markers.
        if (visiblePoints.length !== this.lastVisiblePoints.length
            || visiblePoints.some(
                (pt, index) => pt[1].data_time !== this.lastVisiblePoints[index][1].data_time)
           ) {
            this.markers
                .attr("display", null)
                .selectAll("circle")
                .data(visiblePoints)
                .join("circle")
                .attr("cx", ([a, p]) => xScale(p.data_time) - 0.5)
                .attr("cy", ([a, p]) => yAxes[a.info.axis].axis.scale()(p.value_r) - 0.5)
                .attr("r", 4)
                .attr("fill", ([a, p]) => a.info.color)
                .attr("stroke", "white")
                .attr("stroke-width", 2)
                .style("pointer-events", "none")

            // the info is now inaccurate, so we hide it
            this.#clearHoverInfo();
            this.lastVisiblePoints = visiblePoints;
        }
        // Debounced means we don't update until the mouse pointer is still for a short time
        // No point in showing the info for a brief moment, and it prevents "flicker"
        this.showHoverInfoDebounced(x, y, visiblePoints);

        // // Vertical line under the mouse pointer

        // this.markers.select("line.vertical")
        //     .attr("x1", x)
        //     .attr("y1", 0)
        //     .attr("x2", x)
        //     .attr("y2", this.height);
        // this.markers.select("text.vertical")
        //     .attr("x", x + (x < this.width/2 ? 3 : -3))
        //     .attr("y", this.height)
        //     .text(this.xAxis.scale().invert(x).toISOString())
        //     .attr("text-anchor", x < this.width/2 ? "start" : "end")
        // this.markers.select("line.horizontal")
        //     // .attr("display", null)
        //     .attr("x1", 0)
        //     .attr("y1", y)
        //     .attr("x2", this.width - this.margin.right)
        //     .attr("y2", y)
        // for (const axis of this.axes) {
        //     console.log("axis", axis.side, this.yAxes[axis.id].axis.scale().invert(y));
        //     this.markers.select(`text.horizontal.${axis.side}`)
        //         .attr("x", axis.side === "left" ? 3 : this.width - this.margin.right - 3)
        //         .attr("y", y - 3)
        //         .attr("text-anchor", axis.side === "left" ? "start" : "end")
        //         .text(d3.format("g")(yAxes[axis.id].axis.scale().invert(y)))
        // }

    }

    #clearMarkers () {
        this.markers
            .selectAll("circle")
            .remove();
        this.#clearHoverInfo();
    }

    #updateYAxis (axisId, [yMin, yMax], logarithmic) {
        let yAxis = this.yAxes[axisId];
        if (logarithmic) {
            yAxis.scale = d3.scaleLog()
                .domain([yMin, yMax])
                .range([this.height, 0])
        } else {
            yAxis.scale = d3.scaleLinear()
                .domain([yMin, yMax])
                .range([this.height, 0])
        }
        if (yAxis.side == "left") {
            if (logarithmic) {
                yAxis.axis = d3.axisLeft(yAxis.scale)
                    .ticks(LOG_TICKS)
                    .tickSize(-TICK_SIZE)
                    // .tickFormat(d3.format(Y_TICK_FORMAT))
                    .tickFormat(d3.format(Y_TICK_FORMAT_LOG));
                    // .tickFormat(d3.format(Y_TICK_FORMAT));
            } else {
                yAxis.axis = d3.axisLeft(yAxis.scale)
                    .ticks(5)
                    .tickSize(-TICK_SIZE)
                    .tickFormat(d3.format(Y_TICK_FORMAT));
            }
        } else {
            yAxis.axis = d3.axisRight(yAxis.scale)
                // .ticks(5)
                .tickSize(-TICK_SIZE)
                .tickFormat(d3.format(Y_TICK_FORMAT));
        }
        yAxis.element.call(yAxis.axis);
        yAxis.rect.call(yAxis.zoom.transform, d3.zoomIdentity);
        this.#clearMarkers()
    }

    #getAxisAttributes(axisId) {
        return Object.values(this.attributes).filter(attr => attr.info.axis === axisId);
    }

    // Replot all attributes
    #redraw() {
        for (var key of Object.keys(this.attributes)) {
            this.#redrawAttribute(key);
        }
    }

    // Replot a single attribute
    #redrawAttribute(key) {
        const attr = this.attributes[key];
        if (attr) {
            const yScale = this.yAxes[attr.info.axis].axis.scale(),
                  scale = [this.xAxis.scale(), yScale],
                  selected = this.selected.size === 0 || this.selected.has(key);
            attr.draw(scale, selected? 1.0 : 0.25, this.showGaps);
        } else {
            console.error("can't redraw attr", att_conf_id);
        }
    }

    // Set screen positions of any History event markers (e.g. start/stop/...)
    #updateHistoryEventPositions () {
        const scale = this.xAxis.scale();
        this.historyEl.selectAll("g.history-event")
            .attr("transform", d => `translate(${scale(d.event_time)} 0)`);
    };

    // ==== Public methods ====

    destroy() {
        this.canvasContainer.remove();
        this.SVG.remove();
        this.infoContainer.remove();
    }

    // Create a new x scale and reset the transform. This prevents
    // problems with the cumulative nature of the zoom events.
    setTimeRange (start, end) {
        this.x = d3.scaleTime()
            .domain([start, end])
            .range([0, this.#getXAxisWidth()]);
        this.overlay.call(this.xZoom.transform, d3.zoomIdentity);
        this.xRect.call(this.xZoom.transform, d3.zoomIdentity);
        for (let yAxis of Object.values(this.yAxes)) {
            const axis = this.axes[yAxis.id];
            this.#updateYAxis(yAxis.id, yAxis.axis.scale().domain(), axis.scale === "log");
        }
    }

    setAttributes (attributes, info) {
        const attributeInfos = attributes.map(
            attr => ({...attr, ...info[makeAttrKey(attr)]}))
        let newAttributes = {};
        let deletedAttributes = {...this.attributes};
        let attributesToRedraw = [];
        for (var info of attributeInfos) {
            var attr;
            const key = makeAttrKey(info);
            if (this.attributes[key]) {
                attr = this.attributes[key];
                if (attr.info !== info) {
                    // Updated attribute
                    attr.info = info;
                    attributesToRedraw.push(key);
                }
            } else {
                // New attribute
                // Note that we surround the canvas with a padding, to ensure that we
                // don't cut any pixels at the top/bottom edges.
                const padding = 3;
                const canvas = this.canvasContainer
                      .append("canvas")
                      .attr("height", this.height + 2 * padding)
                      .attr("width", this.#getXAxisWidth())
                      .style("position", "absolute")  // positioned relative to the container
                      .style("top", -padding + "px")  // TODO the -1 is needed for things to line up... why?
                attr = new PlotAttribute(info, canvas.node(), padding);
            }
            newAttributes[key] = attr;
            delete deletedAttributes[key];
        }
        for (let deletedAttr of Object.values(deletedAttributes)) {
            deletedAttr.canvas.remove();
        }
        this.attributes = newAttributes;
        this.#createYAxes(this.axes);
        attributesToRedraw.forEach(key => this.#redrawAttribute(key));
        this.#clearMarkers();
        this.#buildLegend();
    }

    HISTORY_ICONS = {
        start: "⏵︎",
        stop: "⏹",
        pause: "⏸︎︎",
        add: "+",
        remove: "-",
        crash: "!",
    };

    // Update current set of History event markers (start, stop, add, ...)
    setHistory(history) {
        // TODO if we failed to get history, this can break the plot
        this.historyEl
            .selectAll(".history-event")
            .data(history, event => `${event.event_time.getTime()}-${event.att_conf_id}-${event.event}`)
            .attr("transform", null)
            .join(
                enter => {
                    // Create new event markers
                    let g = enter.append("g")
                        .attr("class", "history-event")

                    // Vertical line
                    g.append("line")
                        .attr("vector-effect", "non-scaling-stroke")
                        .attr("stroke", d => (this.attributes[makeAttrKey(d)]?
                                              this.attributes[makeAttrKey(d)].info.color:
                                              null))
                        .attr("stroke-dasharray", "5, 5")
                        .attr("y1", 0)
                        .attr("y2", this.height)

                    // Colored box
                    g.append("rect")
                        .attr("vector-effect", "non-scaling-size")
                        .attr("width", 16)
                        .attr("height", 16)
                        .attr("fill", d => (this.attributes[makeAttrKey(d)]?
                                            this.attributes[makeAttrKey(d)].info.color:
                                            null))
                        .attr("stroke", "white")
                        .attr("stroke-width", 2)
                        .attr("x", -8)
                        .attr("y", -18)
                        .attr("rx", 4)
                    // "Tooltip" that appears when hovering the mouse pointer on the box
                        .append("title")
                        .text(d => (`Event: ${d.event}\n`
                                    + `Attribute: ${this.attributes[makeAttrKey(d)]? this.attributes[makeAttrKey(d)].info.name : ""}\n`
                                    + `Time: ${d.event_time.toISOString()}`))
                    // Text element to provide an icon
                    g.append("text")
                        .attr("fill", "white")
                        .style("pointer-events", "none")
                        .attr("text-anchor", "middle")
                        .attr("y", -5)
                        .text(d => this.HISTORY_ICONS[d.event] || "?")

                    return g;
                },
                update => {
                    // Update existing markers
                    update.select("line")
                        .attr("stroke", d => this.attributes[makeAttrKey(d)]? this.attributes[makeAttrKey(d)].info.color: null)
                    update.select("rect")
                        .attr("fill", d => this.attributes[makeAttrKey(d)]? this.attributes[makeAttrKey(d)].info.color: null)
                    return update;
                },
                exit => {
                    // Remove markers that are no longer present
                    return exit.remove();
                }
            )
        // Update the element transforms, to position them
        this.#updateHistoryEventPositions();
    }

    setYAxis(axis) {
        this.axes[axis.id] = axis;
        this.#updateYAxis(axis.id, axis.limits, axis.scale === "log");
        let yAxis = this.yAxes[axis.id];
        yAxis.label.select("tspan.unit")
            .text(
                (axis.label? axis.label : "") +
                    (axis.unit? ` [${axis.unit}]` : "")
            );
    }

    setSelected(keys) {
        this.selected = new Set(keys);
        // for (let attribute of Object.values(this.attributes)) {
        //     const wasSelected = attribute.selected;
        //     const isSelected = att_conf_ids.has(attribute.info.att_conf_id);
        // }
        this.#redraw();
    }

    updateAttribute (key, data, aggSize) {
        let attr = this.attributes[key];
        attr.update(data, aggSize);
        this.#redrawAttribute(key);
    }

    setShowGaps (value) {
        if (value != this.showGaps) {
            this.showGaps = value;
            this.#redraw();
        }
    }

    setShowHistory (value) {
        if (value) {
            this.historyEl.style("display", null);
        } else {
            this.historyEl.style("display", "none");
        }
    }

}
