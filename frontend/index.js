// Entrypoint for the frontend.

// import "preact/debug";  // Note: needs to be first. TODO remove for production?
import { createElement as h, useRef, useEffect } from "react";
import { createRoot } from 'react-dom/client';

import { debounce } from "./util.js";
import { useStore } from "./store/store.js";
import { setStatusMessage, loadFromURL, fetchInitialAttributes } from "./store/actions.js";

import { DatabaseChooser } from "./components/database.js";
import { Controlsystem } from "./components/controlsystem.js";
import { AttributeSearch } from "./components/attributesearch.js";
import { Attributes } from "./components/attributes.js";
import { AttributeConfig } from "./components/attributeconfig.js";
import { AxisConfig } from "./components/axisconfig.js";
import { GlobalConfig } from "./components/globalconfig.js";
import { PlotWrapper } from "./components/plotwrapper.js";
import { LitepickerWrapper } from "./components/litepickerwrapper.js";
import { LiveTimeRange } from "./components/livetimerange.js";


// Main app
function App() {

    useEffect(() => {
        // Restore state from URL hash, if any
        if (document.location.hash.length > 1) {
            loadFromURL();
        }
        // Debounce so that e.g. a sequence of quick clicks on "back" doesn't cause
        // unnecessary loads
        const slowFetch = debounce(fetchInitialAttributes);
        window.addEventListener(
            // Event listener that catches back/forward browser navigation
            "popstate",
            event => {
                if (event.state) {
                    useStore.setState(event.state);
                    const {attributes} = useStore.getState();
                    slowFetch(attributes);
                }
            });
        setStatusMessage("Application started");
    }, [])

    return [
        h("nav", {key: 0, className: "column left"},
            h("header", {},
                h("a", {
                    id: "reset",
                    href: "",
                    title: "Reload application from scratch, resetting all configuration."
                }, "Reset"),
                h(DatabaseChooser, {}),
                h(Controlsystem, {}),
                h(AttributeSearch, {}),
            ),
            h(Attributes, {}),
            h(SettingsPanel, {}),
        ),
        h("main", {key: 1, className: "column center"},
            h("header", {},
              h(GlobalConfig, {}),
                h("div", {style: {"float": "right"}},
                    h(LitepickerWrapper, {}),
                    h(LiveTimeRange, {})
                ),
            ),
            h(PlotWrapper, {}),
            h(Footer, {}),
        ),
    ];
}

// Settings for current attribute or axis
function SettingsPanel() {
    const current = useStore(state => state.current);
    const [ currentType, currentName ] = current || [null, null];

    switch (currentType) {
    case "attribute":
        return h(AttributeConfig);
    case"axis":
        return h(AxisConfig);
    }
}

// Footer that displays messages to the user
function Footer() {
    const loading = useStore(store => store.loading);
    const livePeriod = useStore(store => store.livePeriod);
    const statusError = useStore(store => store.statusError);
    const statusMessage = useStore(store => store.statusMessage);
    return h("footer", {
        key: 3,
        className: "status" + (
            loading.size > 0? " loading"
                : (statusError? " error" :
                   (livePeriod !== 0? " live" : ""))
        )
    },
        (loading.size > 0?
         h(
             "img", {
                 src: "static/images/loading.gif",
                 style: {width: "1em", height: "1em"},
                 // className: "loader",
             }
         )
         : null),
        h("span", {className: "status-message"}, (
            loading.size > 0 ?
                `Loading ${loading.size} attributes...`
                : statusMessage
        )),
        h("a", {id: "help", href: "static/documentation.html"}, "Help"),
    )
}

const root = createRoot(document.getElementById("root"));
root.render(
    h(App),
);
