import { createElement as h, useRef, useState, useEffect } from "react";

import { attributeSearch } from "../data.js";
import { useStore, makeAttrKey } from "../store/store.js";
import { addAttributes} from "../store/actions.js";

// Attribute search widget
export function AttributeSearch () {
    const database = useStore(store => store.database);
    const controlsystem = useStore(store => store.controlsystem);
    const attributes = useStore(store => store.attributes);
    const [value, setValue] = useState("");
    const [matches, setMatches] = useState([]);
    const [selected, setSelected] = useState([]);

    useEffect(() => {
        // Erase search results when the db/cs changes
        setMatches([]);
        setSelected([]);
    }, [database, controlsystem])

    async function onKeyPress(event) {
        var code = (event.keyCode ? event.keyCode : event.which);
        if (code === 13) {
            try {
                const attributes = await attributeSearch(database, controlsystem, event.target.value);
                setMatches(attributes)
                setSelected([]);
            }
            catch (ex) {
                console.error("Could not get search results", ex.response);
            }

        }
    }

    function onChange (event) {
        setValue(event.target.value);
    }

    function onSelected (event) {
        const att_conf_ids = [...event.target.options]
              .filter(o => o.selected)
              .map(o => parseInt(o.value));
        setSelected(att_conf_ids);
    }

    function onAdd (axis, event) {
        addAttributes(matches.filter(attr => selected.includes(attr.att_conf_id)), axis);
        setSelected([]);
    }

    function onClear() {
        setValue("");
        setMatches([]);
        setSelected([]);
    }

    const options = matches.map(
        (attr, i) => h("option", {
            key: attr.att_conf_id,
            value: attr.att_conf_id,
            disabled: attributes.includes(makeAttrKey(attr)),
        }, attr.name));
    return h("div", {id: "search", title: "Search for attributes by name. '*' is treated as a wildcard, matching anything."},
        h("input", {
            type: "text",
            value,
            placeholder: "Search for attributes",
            onChange, onKeyPress
        }),
        h("div", {style: {display: matches.length > 0? null : "none"}},
          h("select", {
              id: "attribute-search-results",
              multiple: true,
              value: selected,
              onChange: onSelected},
            options),
          h("div", {className: "add-buttons"},
                h("button", {className: "add left",
                             disabled: selected.length === 0 ,
                             onClick: onAdd.bind(this, 0)}, "Add left"),
                h("button", {className: "add right",
                             disabled: selected.length === 0,
                             onClick: onAdd.bind(this, 1)}, "Add right"),
                h("button", {className: "clear", onClick: onClear}, "Clear"),

            )
        )
    );
}
