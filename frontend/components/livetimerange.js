// Widget to choose settings for "live" plotting

import { createElement as h, useRef, useState, useEffect } from "react";
import { useStore } from "../store/store.js";
import { setLiveHistoryPeriod } from "../store/actions.js";

// A set of reasonable window sizes and corresponding update periods.
// We could separate these settings, but that would invite users
// to always use the fastest update period possible, which could
// cause a lot of needless database load.
// The point is, if you really need fast updated values, an archive
// viewer is not the tool you should use.
const options = {
    // "label": [history, period]
    "[Live reload]": [null, null],
    "Off": [0, 0],
    "2 minutes [5s]": [60, 5],
    "5 minutes [10s]": [5 * 60, 10],
    "15 minutes [15s]": [15 * 60, 15],
    "30 minutes [20s]": [30 * 60, 20],
    "1 hour [30s]": [3600, 30],
    "3 hours [30s]": [3 * 3600, 30],
    "6 hours [1m]": [6 * 3600, 60],
    "12 hours [1m]": [12 * 3600, 60],
    "1 day [1m]": [24 * 3600, 60],
};

const optionsByHistory = {}
Object.entries(options).forEach(([title, [history, period]]) => optionsByHistory[history] = period);


export function LiveTimeRange() {
    const liveHistory = useStore(state => state.liveHistory);
    return (
        h("span", {},
          h("label", {title: "Size of the 'live' time window and [update period]."},
            h("select", {
                value: liveHistory,
                onChange: e => {
                    const history = e.target.value;
                    const period = optionsByHistory[history];
                    setLiveHistoryPeriod(history, period);
                }
            },
              Object.entries(options).map(
                  ([title, [history]], i) => h("option", {
                      key: title,
                      value: history,
                      disabled: options[title][0] === null,
                  }, title)
              )
             )
           ),
         )
    );
}
