import { createElement as h, useRef, useState, useEffect, useCallback } from "react";
import * as d3 from "d3";
import { useShallow } from 'zustand/react/shallow'

import { useStore, makeAttrKey } from "../store/store.js";
import { setTimeRange, setAxisLimits, setAggBuckets, setShowErrorGaps, setShowHistory, setAxisAuto, setSelected, setCurrent } from "../store/actions.js";
import { download, debounce } from "../util.js";

import { Plot } from "../plot.js";

// Component that wraps a d3 plot.
export function PlotWrapper () {

    // TODO move into the useShallow call below
    const showErrorGaps = useStore(state => state.showErrorGaps);
    const showHistory = useStore(state => state.showHistory);

    // TODO do we really use all these?
    const {
        timeRange,
        info,
        axes,
        data,
        attributes,
        parameters,
        history,
        selected,
    } = useStore(
        useShallow(state => ({
            timeRange: state.timeRange,
            info: state.info,
            axes: state.axes,
            data: state.data,
            attributes: state.attributes,
            parameters: state.parameters,
            history: state.history,
            selected: state.selected,
        }))
    );

    const plotEl = useRef();
    const plot = useRef();

    useEffect( () => {
        createPlot();
        window.addEventListener("resize", debounce(createPlot, 100));
    }, [])

    const createPlot = () => {
        if (plot.current) {
            // Re-creating the plot, first get rid of the old one
            plot.current.destroy();
            plot.current = null;
        }

        // Hack to get around the "closure" problem where this function
        // refers to stale versions of the state. There may be a better way?
        const state = useStore.getState()

        // Update range only if limits have actually changed
        const maybeUpdateRange = ([start, end]) => {
            if (start.valueOf() !== state.timeRange.start.valueOf() ||
                end.valueOf() !== state.timeRange.end.valueOf()) {
                setTimeRange([start, end]);
            }
        }
        // update axis only if the limits have actually changed
        const maybeUpdateAxis = (axisId, [lower, upper]) => {
            const axis = axes.find(axis => axis.id == axisId);
            const range = axis.limits[1] - axis.limits[0]
            const delta = range * 10e-10;
            if (Math.abs(lower - axis.limits[0]) > delta || Math.abs(upper - axis.limits[1]) > delta) {
                setAxisLimits(axisId, [lower, upper]);
            }
        }
        // Create the actual D3 plot
        plot.current = new Plot(
            plotEl.current,
            state.timeRange,
            state.axes,
            debounce(maybeUpdateRange),
            debounce(maybeUpdateAxis),
        );
        // Report the current plot width back; this will be used as a
        // guideline for how many buckets to use when aggregating.
        setAggBuckets(plot.current.width);
        if (state.attributes && state.info) {
            plot.current.setAttributes(state.attributes, state.info);
            for (let attr of state.attributes) {
                const key = makeAttrKey(attr);
                // if (this.props.data[key] !== oldProps.data[key]) {
                const attrData = state.data[key];
                if (attrData) {
                    plot.current.updateAttribute(key,
                                                 attrData.data,
                                                 attrData.aggSize);
                }
            }
        }
        state.history && plot.current.setHistory(state.history);
        plot.current.setShowGaps(state.showErrorGaps);
        plot.current.setShowHistory(state.showHistory);
    };

    // These useEffects update the D3 plot whenever some parts of state changed

    useEffect( () => {
        // Any changes to attribute "metadata" results in an update
        // Maybe this could be done per attribute, but it gets complicated
        plot.current?.setAttributes(attributes, info);

        // TODO: actually only needed if an attribute changed color
    }, [attributes, info]);

    useEffect( () => {
        plot.current?.setHistory(history);
    }, [history])

    useEffect(() => {
        plot.current?.setTimeRange(timeRange.start,
                                  timeRange.end);
    }, [timeRange])

    useEffect(() => {
        // Y axes
        for (let i = 0; i < axes.length; i++) {
            const axis = axes[i];
            plot.current?.setYAxis(axis);
        }
    }, [axes]);

    useEffect(() => {
        plot.current?.setHistory(history);
    }, [history]);

    useEffect(() => {
        plot.current?.setSelected(selected);
    }, [selected]);

    useEffect(() => {
        // Attribute data changed -> redraw
        for (let attr of attributes) {
            const key = makeAttrKey(attr);
            // if (this.props.data[key] !== oldProps.data[key]) {
            const attrData = data[key];
            if (attrData) {
                plot.current?.updateAttribute(key,
                                              attrData.data,
                                              attrData.aggSize);
            }
        }
    }, [data]);

    useEffect(() => {
        plot.current?.setShowGaps(showErrorGaps);
    }, [showErrorGaps]);

    useEffect(() => {
        plot.current?.setShowHistory(showHistory);
    }, [showHistory]);

    // Convert the plot into a PNG image and download it to the client.
    const saveImage = useCallback(() => {
        if (!plot.current) {
            return
        }
        const svg = plotEl.current.querySelector("svg").cloneNode(true);

        // A bit of a hack, to embed some styles from CSS
        // To do this properly I guess we need to set the styles inline in the plot
        // itself. But this works.
        d3.select(svg)
            .selectAll("text")
            .attr("font-family", "sans-serif");
        d3.select(svg)
            .selectAll(".tick line")
            .attr("stroke", "#0003");
        d3.select(svg)
            .selectAll(".grid .domain")
            .style("display", "none");

        const bbox = plotEl.current.querySelector("svg").getBoundingClientRect();
        const canvases = plotEl.current.querySelector("div").children;
        const mainGroup = svg.querySelector("g");
        let canvasImages = [];
        let img;
        for (let i = 0; i < canvases.length; i++) {
            img = document.createElementNS(null, "image");
            img.setAttribute("width", canvases[i].width)
            img.setAttribute("height", canvases[i].height)
            const dataUrl = canvases[i].toDataURL("image/png");
            img.setAttribute("href", dataUrl);
            mainGroup.insertBefore(img, mainGroup.children[0]);
        }

        // Create an offscreen canvas same size as the plot SVG
        var canvas = document.createElement("canvas");
        canvas.width = bbox.width;
        canvas.height = bbox.height;

        var context = canvas.getContext("2d");
        context.fillStyle = "white";
        context.fillRect(0, 0, canvas.width, canvas.height);

        d3.select(svg).select(".legend").attr("display", "block")

        let image = new Image; // create <img> element
        image.onload = () => {
            // "Trick" to render the SVG onto the canvas
            context.fillStyle = context.createPattern(image, 'repeat');
            context.fillRect(0, 0, canvas.width, canvas.height);

            // TODO: Come up with a nicer filename; the idea is to keep it "unique"
            // and also at least a bit readable...
            const att_conf_ids = attributes.map(attr => attr.att_conf_id);
            const fileName = (
                "archive_"
                    + att_conf_ids.join("_")
                    + timeRange.start.toISOString().slice(0, -5).replaceAll(":", "")
                    + "_"
                    + timeRange.end.toISOString().slice(0, -5).replaceAll(":", "")
                    + ".png");
            download(canvas.toDataURL("image/png"), fileName);
        }
        image.onerror = () => {
            console.error("Error rendering SVG!")
        }
        // Convert the SVG to a data url, to be loaded into canvas
        image.src = 'data:image/svg+xml;base64,' + btoa(unescape(encodeURIComponent(svg.outerHTML)));
    });

    return h("div",
             {id: "plot-wrapper"},
             h("div", {id: "plot", ref: plotEl}),
             h("button", {
                 className: "download-png",
                 onClick: saveImage
             }, "PNG"),
            );
}
