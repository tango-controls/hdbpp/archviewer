import { createElement as h, useRef, useState, useEffect, useCallback } from "react";
import { useStore } from "../store/store.js";
import { setControlsystem, setStatusMessage } from "../store/actions.js";

// Component that renders a controlsystem chooser dropdown.
export function Controlsystem () {

    const [controlsystems, setControlsystems] = useState([]);
    const database = useStore(store => store.database);
    const controlsystem = useStore(store => store.controlsystem);

    // ==== React API metods ====

    const fetchData = useCallback(async () => {
        setControlsystems([])
        var response;
        try {
            response = await fetch(`api/controlsystems?db=${database}`);
        } catch (error) {
            // Usually means a network timeout, should not happen
            console.error("Exception when loading controlsystems", error);
            setStatusMessage(`Unable to list controlsystems for ${database}! Please check console (F12) for error.`, true);
            return;
        }
        if (response.ok) {
            const data = await response.json();
            setControlsystems(data);
            if (data.length === 1) {
                // There's only one control system in the DB; automatically choose it
                setControlsystem(data[0].cs_name);
            }
        } else {
            const data = await response.text()
            console.error("Could not load controlsystems:", data);
            setStatusMessage(`Unable to list controlsystems for ${database}! Please check console (F12) for details.`, true);
        }
    });

    useEffect(() => {
        if (database) {
            fetchData();
        }
    }, [database]);

    // ==== Callbacks ====

    function onCurrentChanged (event) {
        setControlsystem(event.target.value);
    }

    const placeholderOption = h(
        "option",
        {
            key: -1,
            value: "default",
            disabled: true
        },
        "Choose controlsystem..."
    );
    const options = controlsystems.map(
        (cs, i) => h(
            "option",
            {key: i, value: cs.cs_name},
            cs.cs_name)
    );
    return h(
        "div", {id: "controlsystem", title: "Current controlsystem to search for attributes"},
        h("select",
          {
              value: controlsystem || "default",
              disabled: !database || controlsystems.length === 0,
              onChange: onCurrentChanged,
          },
          [placeholderOption, ...options])
    );
}
