import { createElement as h, useRef, useState, useEffect } from "react";
import { useStore } from "../store/store.js";
import { setTimeRange } from "../store/actions.js";

// Custom plugin to add "go to timestamp" feature.
// Allows specifying a timestamp, as well as a "context" size.
// The time window will be set to the timestamp +/- the context.
Litepicker.add("timestamp", {
    init: function(picker) {
        var lastTimestamp = new Date();
        var lastContext = 5;
        var lastContextUnit = 60;
        picker.on("render", ui => {
            const block = document.createElement('form');
            block.className = "container__timestamp";
            block.innerHTML = "Timestamp"
            block.title = "Specify a timestamp and a context size. The time window will be adjusted to center on the timestamp, showing the context around it."

            // Date+time input
            const timestampInput = document.createElement("input");
            timestampInput.setAttribute("type", "datetime-local");
            timestampInput.setAttribute("step", "1");
            ts = new Date(lastTimestamp.getTime());
            ts.setMinutes(ts.getMinutes() - ts.getTimezoneOffset());
            block.appendChild(timestampInput);
            timestampInput.value = ts.toISOString().slice(0,16)

            block.appendChild(document.createTextNode("+/-"))

            // Context size input
            const contextInput = document.createElement("input");
            contextInput.setAttribute("type", "number");
            contextInput.setAttribute("size", 8)
            contextInput.value = lastContext;
            block.appendChild(contextInput)

            // Context unit select
            const contextUnitSelect = document.createElement("select");
            [["seconds", 1], ["minutes", 60], ["hours", 60 * 60], ["days", 24 * 60 * 60]]
                .forEach(([unit, seconds]) => {
                    const option = document.createElement("option");
                    option.setAttribute("value", seconds)
                    option.setAttribute("label", unit)
                    contextUnitSelect.appendChild(option);
                })
            block.appendChild(contextUnitSelect)
            contextUnitSelect.value = lastContextUnit;

            // Go-to button
            const button = document.createElement("button")
            button.innerHTML = "Go to"
            block.appendChild(button);

            function submit(e) {
                // Pressing return on any field, or pressing the button
                // will go to the current timestamp +/ context.
                e.preventDefault();
                const context = contextInput.value;
                const contextUnit = parseInt(contextUnitSelect.value);
                const contextMillis = context * contextUnit * 1000;
                const newTimestamp = new Date(timestampInput.value);
                const startDate = new Date(newTimestamp.getTime() - contextMillis),
                      endDate = new Date(newTimestamp.getTime() + contextMillis);
                picker.setDateRange(picker.DateTime(Number(startDate)),
                                    picker.DateTime(Number(endDate)),
                                    true);
                picker.emit('timestamp.apply', startDate, endDate);
                lastTimestamp = newTimestamp;
                lastContext = context;
                lastContextUnit = contextUnit;
                picker.hide();
            }

            block.addEventListener("submit", submit);
            ui.querySelector('.container__main').after(block);
        })
    }
})


function getWholeDaysRange(start, end) {
    // Litepicker is a *date* picker, so it returns the
    // beginning of each date, e.g. midnight. Since we want an
    // *inclusive* range, we add *almost* one day here, to
    // include the last date in the range. This is more
    // intuitive. Maybe there is a better way though...
    var endInclusive = end.dateInstance;
    endInclusive.setDate(end.getDate() + 1);
    endInclusive.setSeconds(-1);
    return [start.dateInstance, endInclusive];
}

/*
  Litepicker is a pure javascript library for displaying a date picker.
  Here we just wrap it up in a component for convenience.
*/
export function LitepickerWrapper () {

    const inputRef = useRef();
    const litepicker = useRef(null);

    const timeRange = useStore(state => state.timeRange);

    useEffect(() => {
        recreate();
    }, []);

    function recreate() {
        if (litepicker.current) {
            litepicker.current.destroy();
            litepicker.current = null;
        }
        const container = inputRef.current;
        litepicker.current = new Litepicker({
            element: container,
            singleMode: false,
            // allowRepick: true,
            numberOfColumns: 1,
            numberOfMonths: 2,
            splitView: true,
            autoApply: false,
            dropdowns: {years: true, months: true},
            maxDate: new Date(),
            showWeekNumbers: true,
            // autoUpdate: true,
            plugins: ['ranges', 'timestamp'],
            ranges: {
                autoApply: false,  // Won't work since it does not cause the "apply" event
                customRanges: getCustomRanges(),
                position: "right",
            }
        });
        litepicker.current.on("button:apply", (start, end) => {
            // This happens when the user clicks "Apply".
            const range = getWholeDaysRange(start, end);
            setTimeRange(range);
        });
        litepicker.current.on("timestamp.apply", (start, end) => {
            // This happens when the user clicks "Go!".
            setTimeRange([start, end]);
        });
        litepicker.current.on("ranges.preselect", (start, end) => {
            // This happens when the user clicks one of the "preset" buttons.
            // This is a workaround to get "auto" apply.
            // TODO Not sure we actually want this though... let's see what users say.
            const range = getWholeDaysRange(start, end)
            setTimeRange(range);
            litepicker.current.hide();
        });

        litepicker.current.on("before:show", () => {
            // Update the ranges to be relative to now.
            let options = litepicker.current.options.ranges;
            options.customRanges = getCustomRanges();
            const {start, end} = timeRange;
            litepicker.current.setDateRange(start, end);
        })
    }

    function getCustomRanges() {
        var todayEnd = new Date();
        todayEnd.setDate(todayEnd.getDate() + 1);
        todayEnd.setHours(0);
        todayEnd.setMinutes(0);
        todayEnd.setSeconds(-1);
        var todayStart = new Date();
        todayStart.setHours(0);
        var yesterdayEnd = new Date();
        yesterdayEnd.setDate(todayEnd.getDate() - 1);
        var yesterdayStart = new Date();
        yesterdayStart.setDate(todayStart.getDate() - 1);
        var aWeekAgoStart = new Date();
        aWeekAgoStart.setDate(todayStart.getDate() - 6);
        var aMonthAgoStart = new Date();
        aMonthAgoStart.setDate(todayStart.getDate() - 30);
        return {
            "Today": [todayStart, todayEnd],
            "Yesterday": [yesterdayStart, yesterdayEnd],
            "Last 7 days": [aWeekAgoStart, todayEnd],
            "Last 30 days": [aMonthAgoStart, todayEnd],
        }
    }

    useEffect(() => {
        const {start, end} = timeRange;
        setRange(start, end);
    }, [timeRange]);

    function setRange (start, end) {
        // Set the picker's selected range
        litepicker.current.setDateRange(start, end);
        // Set the months displayed by the picker
        // TODO: more useful to not display same month in both columns?
        litepicker.current.gotoDate(start, 0);
        litepicker.current.gotoDate(end, 1);
    }

    return h("input", {
        type: "text",
        id: "litepicker",
        ref: inputRef,
        title: "The date range of the plot. Click to select from a calendar."
    });

}
