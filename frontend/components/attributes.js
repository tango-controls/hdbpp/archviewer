import { createElement as h, useRef, useState, useEffect } from "react";
import { useStore, makeAttrKey } from "../store/store.js";
import { removeAttributes } from "../store/actions.js";
import { Axis } from "./axis.js";

// Current attributes list (per axis)
export function Attributes () {

    const attributes = useStore(state => state.attributes);
    const selected = useStore(state => state.selected);
    const axes = useStore(state => state.axes);

    function onRemoveSelected(event) {
        const keys = attributes
              .map(makeAttrKey)
              .filter(key => selected.has(key));
        removeAttributes(keys);
    }

    function onRemoveAll(event) {
        removeAttributes(attributes.map(makeAttrKey));
    }

    const sides = ["left", "right"];
    return h("article", {},
             h("div", {id: "attributes"},
               sides.map(
                   side => h("div", {key: side, className: "axis-side"},
                             h("div", {key: 1, className: "axis-side-info left"}, side),
                             h("ul", {key: 2, className: "axes"},
                               axes
                               .filter(axis => axis.side === side)
                               .map(
                                   (axis, i) => h(Axis, {
                                       key: i,
                                       axis: axis,
                                       selected: Array.from(selected),
                                   }))),
                            ),
               ),
               h("footer", {},
                 h("button", {
                     title: "Remove selected attributes",
                     className: "remove",
                     disabled: selected.size === 0,
                     onClick: onRemoveSelected
                 }, "Remove"),
                 h("button", {
                     title: "Remove all attributes",
                     className: "remove",
                     disabled: attributes.length === 0,
                     onClick: onRemoveAll
                 }, "Remove all")
                )
              )
            );
}
