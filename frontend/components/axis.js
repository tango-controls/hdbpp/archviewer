import { createElement as h, useRef, useState, useEffect } from "react";
import { useShallow } from "zustand/react/shallow";

import { useStore, makeAttrKey } from "../store/store.js";
import { setAxisAuto, setAxisScale, setSelected, setCurrent, getAttribute} from "../store/actions.js";

import { AxisAttribute } from "./attribute.js";


// Displays a single axis, and the attributes associated with it.
export function Axis ({axis}) {

    const {
        info,
        data,
        parameters,
        loading,
        selected,
        aggFunc,
    } = useStore(
        useShallow(state => ({
            info: state.info,
            data: state.data,
            parameters: state.parameters,
            loading: state.loading,
            selected: state.selected,
            aggFunc: state.aggFunc,
        }))
    );
    const attributes = useStore(state => state.attributes)
          .filter(attr => attr.axis === axis.id)
          .map(attr => {
              const key = makeAttrKey(attr);
              return {
                  ...getAttribute(key),
                  loading: loading.has(key),
                  selected: selected.has(key),
              };
          });

    return (
        h("li", {className: "axis"},
            h("div", {key: 0, className: "axis-info"},
                h("button", {
                    key: 0,
                    title: "Show settings for this axis",
                    onClick: () => setCurrent("axis", axis.id)
                }, `Axis ${axis.id}`),
                h("label", {
                    title: "Automatically fit the Y axis limits to the data."
                },
                  "Auto",
                  h("input", {
                      key: 1,
                      type: "checkbox",
                      checked: axis.auto,
                      onChange: event => setAxisAuto(axis.id, event.target.checked),
                  }),
                 ),

              h("label", {
                  title: "Use a logarithmic scale for this Y axis."
              },
                "Log",
                h("input", {
                    key: 1,
                    type: "checkbox",
                    checked: axis.scale === "log",
                    onChange: event => setAxisScale(axis.id, event.target.checked? "log" : "lin"),
                }),
               ),
             ),
          h("ul", {key: 1, className: "axis-attributes"},
            attributes.map(
                (attribute, i) => h(
                    AxisAttribute,
                    {
                        key: makeAttrKey(attribute),
                        ...attribute,
                    }
                )))
         )
    );
}
