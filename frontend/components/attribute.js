import { createElement as h, useRef, useState, useEffect } from "react";
import { useStore, makeAttrKey } from "../store/store.js";
import { setSelected, setCurrent } from "../store/actions.js";

// Single attribute in an axis
export function AxisAttribute ({db, att_conf_id, info, data, color, loading}) {

    const defaultAggFunc = useStore(state => state.aggFunc);
    const selected = useStore(state => state.selected);

    let tooltipText;
    const aggFunc = data?.aggFunc || defaultAggFunc;
    if (info) {
        const {name, cs_name, format, type} = info;
        tooltipText = `${name}\n\nCS: ${cs_name}\nType: ${type}\nFormat: ${format}`;
        if (data && data.aggSize > 0) {
            tooltipText += `\nAgg func: ${aggFunc}\nAgg size: ${data.aggSize} s`;
        }
    } else {
        tooltipText = "Loading...";
    }

    const key = makeAttrKey({db, att_conf_id});
    const isSelected = selected.has(key);

    return h("li",
             {},
             h("div", {
                 className: `axis-attribute${isSelected? " selected" : ""}${loading? " loading" : ""}`,
                 title: tooltipText,
             },
               [
                   h("button", {
                       key: 0,
                       title: "Show settings for this attribute",
                       className: "color-patch",
                       onClick: () => setCurrent("attribute", key),
                       style: {background: color}
                   }),
                   // h("input", {key: 0, type: "color"}),
                   h("span", {
                       key: 1,
                       className: "axis-attribute-name",
                       onClick: () => setSelected(key, !isSelected)
                   },
                     info? info.name : "..."),
                   h("label", {
                       key: 2,
                       className: "aggregated",
                       style: {
                           display: !loading && data && data.aggSize > 0 ? "block" : "none"
                       }
                   },
                     aggFunc,
                    ),
                   h("img", {
                       key: 3,
                       src: "static/images/loading.gif",
                       className: "loader",
                       style: {display: loading ? "block" : "none"}
                   }),
               ])
            )
}
