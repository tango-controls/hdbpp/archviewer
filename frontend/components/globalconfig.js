import { createElement as h, useRef, useState, useEffect } from "react";
import { useStore, AGG_FUNCS } from "../store/store.js";
import { setAggLimit, setShowErrorGaps, setShowHistory, setAggFunc } from "../store/actions.js";

// "Global" configuration settings
export function GlobalConfig (props) {

    const aggFunc = useStore(state => state.aggFunc);
    const aggLimit = useStore(store => store.aggLimit);
    const showErrorGaps = useStore(store => store.showErrorGaps);
    const showHistory = useStore(store => store.showHistory);
    const [limit, setLimit] = useState(0);

    useEffect(() => {
        setLimit(aggLimit);  // Keep component state in sync
    }, [aggLimit])

    function onAggFunc(event) {
        setAggFunc(event.target.value);
    }

    function onAggLimit(event) {
        setAggLimit(event.target.value || 0);
    }

    function onShowErrorGaps(event) {
        setShowErrorGaps(event.target.checked);
    }

    function onShowHistory(event) {
        setShowHistory(event.target.checked);
    }

    const aggFuncOptions = [
        ...AGG_FUNCS.map((f, i) => h("option", {key: i + 1, value: f}, f || ""))
    ];

    return h("span", {},
             h("label",
               {
                   title: "Default aggregation function. Attributes may override this with individual settings."
               },
               "Agg. func.:",
               h("select", {
                   value: aggFunc,
                   onChange: onAggFunc,
                   className: "agg-func",
               }, aggFuncOptions)
              ),
             h("label",
               {
                   htmlFor: "agg-limit-global",
                   title: ("Aggregation limit. If the estimated number of points within the time range is above this,"
                           + " the data will be aggregated into approximately one value per x-axis pixel, using"
                           + " an aggregation function. 0 means never aggregate, 1 means always aggregate."
                           + "\nPlease note that loading a large number of raw points can be very slow, even hang your browser!")
               },
               "Agg. limit:"),
             h("input", {
                 id: "agg-limit-global",
                 type: "number",
                 size: 8,
                 min: 0,
                 step: 10000,
                 value: limit,
                 onChange: event => setLimit(event.target.value),
                 onKeyPress: event => {
                     if (event.key === "Enter") {
                         onAggLimit(event);
                     }
                 },
             }),
             " Show",
             h("label",
               {
                   htmlFor: "show-error-gaps",
                   title: "Whether to show errors as 'gaps' in the lines, or to ignore them."
               },
               "gaps:"),
             h("input", {
                 id: "show-error-gaps",
                 type: "checkbox",
                 value: showErrorGaps,
                 onChange: onShowErrorGaps,
             }),
             h("label",
               {
                   htmlFor: "show-history-events",
                   title: "Whether to display archiving history events (start, stop, ...)."
               },
               "events:"),
             h("input", {
                 id: "show-history-events",
                 type: "checkbox",
                 value: showHistory,
                 onChange: onShowHistory,
             }),
            );
}
