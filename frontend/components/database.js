import { createElement as h, useRef, useState, useEffect } from "react";
import { useStore } from "../store/store.js";
import { setDatabase } from "../store/actions.js";

// Component that renders a database chooser dropdown.
export function DatabaseChooser () {

    const [ databases, setDatabases ] = useState([])
    const database = useStore(state => state.database);

    useEffect(() => {
        async function fetchData() {
            const response = await fetch("api/databases");
            if (response.ok) {
                const data = await response.json();
                setDatabases(data);
                if (data.length === 1) {
                    setDatabase(data[0].db_name)
                }
            } else {
                console.error("Could not load databases", response);
            }
        }
        fetchData();
    }, []);

    // ==== Callbacks ====

    function onCurrentChanged (event) {
        setDatabase(event.target.value)
    }

    const placeholderOption = h(
        "option",
        {
            key: -1,
            value: "default",
            disabled: true
        },
        "Choose database...");
    const options = databases.map(
        (db, i) => h("option", {key: i, value: db.db_name}, db.db_name));
    return h("div", {id: "database", title: "Current database to search for attributes"},
             h("select",
               {

                   value: database || "default",
                   onChange: onCurrentChanged
               },
               [placeholderOption, ...options])
            );
}
