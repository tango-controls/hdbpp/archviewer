import { createElement as h, useRef, useState, useEffect } from "react";
import { useStore } from "../store/store.js";
import { setAttributeAggFunc, setAxisLimits, setAxisScale, setAxisAuto, setCurrent, getAxisAttributes} from "../store/actions.js";

// Display info and settings for a single attribute
export function AxisConfig() {

    const axes = useStore(state => state.axes);
    const [ currentType, currentId ] = useStore(state => state.current);
    const axis = axes[currentId]

    function onAggFunc(event) {
        const aggFunc = event.target.value !== "default"? event.target.value : null;
        const attrs = getAxisAttributes(axis.id);
        setAttributeAggFunc(aggFunc, attributes);
    }

    function onMaxChange(event) {
        const maxValue = parseFloat(event.target.value);
        setAxisLimits(axis.id, [axis.limits[0], maxValue]);
    }

    function onMinChange(event) {
        const minValue = parseFloat(event.target.value);
        setAxisLimits(axis.id, [minValue, axis.limits[1]]);
    }

    const [minValue, maxValue] = axis.limits;
    const range = maxValue - minValue;

    return h("footer", {className: "config"},
        h("header", {key: -1},
            h("span", {key: 1, className: "heading"}, `Axis ${axis.id}`),
            h("button", {key: 2, className: "close", onClick: () => setCurrent(null)}, "×"),
        ),
        h("div", {key: 0}, "Side: " + axis.side),
        h("label", {key: 1},
            "Logarithmic:",
            h("input", {
                type: "checkbox",
                checked: axis.scale === "log",
                onChange: e => setAxisScale(axis.id, e.target.checked? "log" : "lin")
            }),
        ),
        h("label", {key: 2},
            "Auto scale:",
            h("input", {
                type: "checkbox",
                checked: axis.auto,
                onChange: e => setAxisAuto(axis.id, e.target.checked)
            })
        ),
        h("div", {key: 3},
            "Max:",
            h("input", {
                type: "number",
                value: axis.limits[1],
                onChange: onMaxChange,
            }),
        ),
        h("div", {key: 4},
            "Min:",
            h("input", {
                type: "number",
                value: axis.limits[0],
                onChange: onMinChange,
            }),
        ),
    )
}
