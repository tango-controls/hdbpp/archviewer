import { createElement as h, useRef, useState, useEffect } from "react";

import { useStore, makeAttrKey, AGG_FUNCS } from "../store/store.js";
import { setCurrent, getAttribute, setAttributeLook, setAttributeAggFunc } from "../store/actions.js";
import { download } from "../util.js";

function formatLimit(value) {
    //return value.toLocaleString('en-US', {maximumSignificantDigits: 5});
    // TODO use attribute format?
    if (value === null) {
        return null;
    }
    return value.toExponential(5)
}

const LINE_STYLES = [
    ["", "Default"],
    ["d", "Long dash"],
    ["s", "Short dash"],
    ["p", "Points"],
];

// Display info and settings for a single attribute
export function AttributeConfig() {

    const current = useStore(state => state.current),
          // Note: we don't use 'attributes' here, but we need to render when it
          // changes. Exercise: refactor to get rid of this!
          attributes = useStore(state => state.attributes),
          data2 = useStore(state => state.data),  // Same here
          attribute = getAttribute(current[1]),
          { att_conf_id, color, lineStyle, lineWidth, info, data, parameters } = attribute,
          key = makeAttrKey(attribute);

    const aggFuncs = [
        "",  // Default: use global setting
        ...AGG_FUNCS,
    ];
    const aggFuncOptions = [
        ...aggFuncs.map((f, i) => h("option", {key: f, value: f}, f || "Default"))
    ];

    function downloadJSON() {
        const jsonData = JSON.stringify(data.data);
        const fileName = info.name.replaceAll("/", "__") + ".json";
        download("data:application/json;charset=utf-8," + encodeURIComponent(jsonData), fileName)
    }

    function downloadTSV() {
        const tsvData = data.tsvData;
        const fileName = info.name.replaceAll("/", "__") + ".tsv";
        download("data:text/tab-separated-values;charset=utf-8," + encodeURIComponent(tsvData), fileName)
    }

    // If some of the info is still loading, wait with rendering
    // TODO show some nice loading thing...
    if (!info || !data) {
        return h("footer", {className: "config"},
            h("header", {},
                h("span", {className: "heading"}, "Attribute"),
                h("button", {className: "close", onClick: () => setCurrent(null)}, "×"),
            ),
            "...loading..."
        );
    }

    // TODO do some kind of layout here, it's a mess
    return h("footer", {className: "config"},
        h("header", {},
            h("span", {className: "heading"}, "Attribute settings"),
            h("button", {className: "close", onClick: () => setCurrent(null)}, "×"),
        ),
        h("div", {className: "attribute-name"}, info.name || "..."),
        h("details", {open: true},
            h("summary", {}, "Info"),
            h("div", {className: "table"},
                h("label", {}, "Label:"),
                parameters ? (parameters.label || "-") : "...",
                h("label", {}, "Unit:"),
                parameters ? (parameters.unit || "-") : "...",
                h("label", {}, "att_conf_id:"),
                `${att_conf_id}`,
                ...(data.aggSize? [
                    h("div", {
                        key: 0,
                        "title": "Number of data points in the range before aggregation. This is an estimate, not an exact number."
                    }, "N. points:"),
                    `${data.nPoints}`,
                    h("div", {
                        key: 1,
                        "title": "Aggregation size. Width of each aggregation 'bucket' in time. Determined from the time range and the number of pixels on screen.",
                    }, "Agg. size:"),
                    h("span", {key: 2}, `${data.aggSize} s`),
                ] : [
                    h("div", {
                        key: 0,
                        "title": "Number of data points in the range."
                    }, "N. points:"),
                    h("span", {key: 1}, `${data.nPoints}`),
                ]),
                "Min:", h("span", {}, `${formatLimit(data.limits[0])}`),
                "Max:", h("span", {}, `${formatLimit(data.limits[1])}`),
            )
        ),

        h("details", {open: true},
            h("summary", {}, "Settings"),
            h("div", {className: "table"},
                "Color:",
                h("input", {type: "color", value: color, onChange: e => setAttributeLook(key, "color", e.target.value)}),

                "Line width:",
                h("input", {type: "number", value: lineWidth || 1, onChange: e => setAttributeLook(key, "lineWidth", e.target.value),
                            min: 1, max: 5, size: 2}),
                "Line style:",
                h("select", {value: lineStyle || "", onChange: e => setAttributeLook(key, "lineStyle", e.target.value)},
                  LINE_STYLES.map(([style, title]) => h("option", {key: title, value: style}, title))
                 ),
                h("label", {
                    title: "Aggregation function for this attribute. 'Default' means use the global aggregation function."
                }, "Agg. func:"),
                h("select", {
                    value: attribute.aggFunc,
                    onChange: e => setAttributeAggFunc(e.target.value || null, [key]),
                    className: "agg-func",
                }, aggFuncOptions),
            ),
        ),
        
        h("details", {open: false},
            h("summary", {}, "Download"),
            h("div", {},
                h("button", {onClick: downloadJSON, title: "Download current data for attribute as JSON file."}, "JSON"),
                h("button", {onClick: downloadTSV, title: "Download current data for attribute as 'tab separated values' file."}, "TSV"),
             )
        )
    )
}
