/*
  Functions to store and retrieve configuration from the URL.
*/

function parseTimeRange({start, end}) {
    return {start: new Date(start), end: new Date(end)};
}

export function encodeURL(data) {
    let params = new URLSearchParams();
    params.append("db", data.database);
    params.append("cs", data.controlsystem);
    data.attributes.forEach(
        attr => {
            let look;
            if (attr.lineStyle || attr.lineWidth) {
                look = `${attr.color}:${attr.lineStyle || ""}:${attr.lineWidth || ""}`;
            } else {
                look = attr.color;
            }
            params.append(
                // "attr", `${attr.att_conf_id};${attr.axis};${attr.color};${attr.aggFunc || ""}`));
                "attr", [
                    attr.db,
                    attr.att_conf_id,
                    attr.axis,
                    look,
                    attr.aggFunc || ""
                ].join(";")
            )
        });
    data.axes.forEach(
        axis => {
            const lowerLimit = axis.auto? "" : axis.limits[0];
            const upperLimit = axis.auto? "" : axis.limits[1];
            params.append(
                "axis",
                [axis.id, axis.side[0], axis.scale, lowerLimit, upperLimit].join(",")
            );
        })
    if (data.livePeriod !== 0) {
        params.append("history", data.liveHistory);
        params.append("period", data.livePeriod);
    } else {
        params.append("start", data.timeRange.start.toISOString());
        params.append("end", data.timeRange.end.toISOString());
    }
    params.append("agg_func", data.aggFunc);
    params.append("agg_limit", data.aggLimit);
    return params;
}

// Store the state in the URL
var lastState = null;
export function updateURL (state) {
    if (state !== lastState) {
        const url = new URL(window.location);
        // Decoding because (I hope) all the characters that will appear
        // are OK for URLs. Much more readable without encoding.
        url.hash = "#" + decodeURIComponent(encodeURL(state).toString());

        // By this method of setting the URL we don't generate any events
        // which prevents circular behavior. Also, we store state in
        // in the history data, which we can use for browser navigation!
        window.history.pushState({
            controlsystem: state.controlsystem,
            attributes: state.attributes,
            axes: state.axes,
            timeRange: state.timeRange,
        }, '', url);
    }
};

// Make an attribute config from an URL attr argument
function parseURLAttribute(attr) {
    const [db, att_desc, axis_str, look, aggFunc] = attr.split(";");
    const [color, lineStyle, lineWidth] = look.split(":");

    let att_conf_id = parseInt(att_desc);
    let att_name = null;

    // handle the case where instead of attribute conf ID we got attribute's full name
    if (isNaN(att_conf_id)) {
        att_conf_id = null;
        att_name = att_desc;
    }

    return {
        db,
        att_conf_id: att_conf_id,
        att_name: att_name,
        axis: parseInt(axis_str),
        color,
        lineStyle,
        lineWidth: (lineWidth? parseFloat(lineWidth) : null),
        aggFunc,
    };
}

// Make an axis config from an URL axis argument
function parseURLAxis(axis) {
    const [axis_id, side, scale, min, max] = axis.split(",");
    return {
        id: parseInt(axis_id),
        side: side == 'l'? "left" : "right",
        scale,
        auto: isNaN(parseFloat(min)),
        limits: [parseFloat(min) || -1, parseFloat(max) || 1]
    };
}

// Parse an URL into config
export function parseURL(hash, defaults) {
    const params = new URLSearchParams(hash);
    const urlAxes = params.getAll("axis").map(parseURLAxis);
    const history = !!params.get("history");
    return {
        database: params.get("db"),
        controlsystem: params.get("cs"),
        attributes: params.getAll("attr").map(parseURLAttribute),
        axes: defaults.axes.map((axis, i) => urlAxes.find(a => a.id === i) || axis),
        liveHistory: parseInt(params.get("history")) || 0,
        livePeriod: parseInt(params.get("period")) || 0,
        timeRange: {
            start: (
                history?
                    new Date((new Date()) - parseInt(params.get("history")) * 1000) :
                    new Date(params.get("start") || defaults.timeRange.start)
            ),
            end: (
                history?
                    new Date() :
                    new Date(params.get("end") || defaults.timeRange.end)
            ),
        },
        aggFunc: params.get("agg_func") || defaults.aggFunc,
        aggLimit: params.get("agg_limit") || defaults.aggLimit,
    };
}
