import { create } from 'zustand'

function getDefaultTimeRange() {
    return {
        start: new Date(Date.now() - 24 * 60 * 60 * 1000),
        end: new Date(),
    };
};

export const AGG_FUNCS = ["avg", "min", "max"];  // TODO get this from backend?

export const DEFAULTS = {
    aggFunc: "avg",
    aggLimit: 20000,
    axes: [
        {"id": 0, "side": "left", "limits": [-1, 1], auto: true, scale: "lin"},
        {"id": 1, "side": "right", "limits": [-1, 1], auto: true, scale: "lin"},
    ],
    timeRange: getDefaultTimeRange()
};

// This is the store for global application state
// Uses Zustand (see https://zustand.docs.pmnd.rs)
// Components "subscribe" to changes by selecting some parts of the store.
// The store should only be modified via "actions" (see below.)
export const useStore = create(() => ({
    database: null,  // Current database (e.g. "hdb")
    controlsystem: "",  // Current selected control system (e.g. "tango_host:10000")

    // Note: data below is keyed on "att_key" which is "<database>:<att_conf_id>"
    // This uniquely identifies one attribute. "att_conf_id" is only unique per database.
    attributes: [],  // [{att_key, axis, color}, ...] settings, as stored in URL
    info: {},  // {att_key: {cs, name, type...}, ...} from DB
    data: {},  // {att_key: {data: value_r, value_w, ...}, aggSize, ...} fom DB
    history: [],  // attribute history events; start/stop/etc from DB in one common list
    parameters: {},  // attribute settings settings like label, unit... from DB, stored by att_key

    axes: DEFAULTS.axes,  // Y axes (always two for now)
    timeRange: DEFAULTS.timeRange,  // time (X axis) range {start: Date, end: Date}
    aggBuckets: 2000,  // number of points to aggregate data into. Will be set according to plot size.
    loading: new Set(),  // set of ids of attributes currently loading data
    selected: new Set(),  // set of ids of attrs clicked in the list
    current: null,  // attribute/axis: ["attribute", att_conf_id] or ["axis", axisId] which we are showing
    // the settings dialog for. TODO rename me

    // Global settings
    aggLimit: 20000,  // Current aggregation limit (max number of points to get before aggregating)
    aggFunc: "avg",  // Current default aggregation function
    showErrorGaps: false,  // Show errors as gaps in plot
    showHistory: false,  // Show "history" events in plot (start/stop archiving etc)

    // Live updates
    livePeriod: 0,  // How often to update, in seconds (0 means no live update)
    liveHistory: 0, // Size of the live time window, in seconds

    // Status
    statusMessage: "...",  // Informational message about recent operation
    statusError: false,  // Indicate whether the status message is an error
}));

// Return a string that identifies the attribute uniquely. This
// key is used to store various information about attributes.
export function makeAttrKey(attr) {
    return `${attr.db}:${attr.att_conf_id}`;
}
