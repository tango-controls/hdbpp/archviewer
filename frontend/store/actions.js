// Functions for modifying and interacting with the store.
// The store must not be directly modified, that will break reactivity.
// Instead, use these "action" functions, or add new ones if needed.

import { useStore, makeAttrKey, DEFAULTS } from "./store.js";
import { debounce, getColor, getAxisLimits } from "../util.js";
import { parseURL, updateURL } from "../url.js";
import { getAttributeData, getAttributeByName } from "../data.js";


// Synchronize URL with current state
// Debounced to prevent frequent updates
export const setURL = debounce(() => {
    updateURL(useStore.getState())
}, 500);


// ===== Functions for fetching data from the backend =====

// When initially loading the config from URL, we allow specifying attributes
// by name, instead of the usual "key". This is partly a backwards compatibility
// thing, but also sometimes convenient e.g. when generating an URL.
export async function fetchInitialAttributes (attributes) {
    //
    // if any attributes are specified by name,
    // look-up the attribute config IDs
    //
    let attrsUpdated = false;
    let newAttrs = await Promise.all(attributes.map(async (attr) => {
        if (attr.att_conf_id !== null) {
            return attr;
        }

        try {
            const att_conf_id = await getAttributeByName(attr.db, attr.att_name);
            attrsUpdated = true;
            return { ...attr, att_conf_id: att_conf_id };
        }
        catch (ex) {
            const errMsg = `Failed to lookup attribute ${attr.att_name}. ${ex}`;
            console.error(errMsg);
            setStatusMessage(errMsg);

            // 'mark' this attribute as invalid
            return null;
        }
    }));

    // drop all invalid attributes, i.e. cases were we failed to look-up the attribute
    newAttrs = newAttrs.filter((attr) => attr !== null);

    //
    // commit any looked-up attribute config IDs to the state
    //
    useStore.setState({attributes: newAttrs});

    if (attrsUpdated) {
        // overwrite URL with attributes specified with IDs
        setURL();
    }
    // If there were attributes in the URL, we load them now.
    // First get info for them
    const state = useStore.getState()
    await fetchInfo(state.attributes)

    if (state.livePeriod !== 0) {
        // Kick of the live reload loop, which will start by
        // loading the data.
        setLiveHistoryPeriod(state.liveHistory, state.livePeriod);
    } else {
        // Just load the data, once
        fetchData(state.attributes)
    }
}

// Get info information about some attributes. This includes metadata like
// attribute data type, etc.
async function fetchInfo(attributes) {
    if (attributes.length === 0) {
        return;
    }
    let attrPerDb = {};
    // We need to make one call per db, that's how the API works
    // But we will do them in parallel.
    attributes.forEach(attr => {
        if (attrPerDb[attr.db]) {
            attrPerDb[attr.db].push(attr.att_conf_id);
        } else {
            attrPerDb[attr.db] = [attr.att_conf_id];
        }
    });
    let promises = Object.entries(attrPerDb)
        .map(([db, att_conf_ids]) => {
            const params = new URLSearchParams();
            params.append("db", db);
            att_conf_ids.forEach(att_conf_id => params.append("att_conf_id", att_conf_id));
            // Fetch info for the attributes
            return new Promise(
                (resolve, reject) => {
                    fetch("api/attribute/info?" + params.toString())
                        .catch(e => {
                            console.error("Could not get attribute info", e);
                            setStatusMessage(`Error getting info for att_conf_id ${att_conf_ids}, see console for more detail.`, true);
                            reject();
                        })
                        .then(response => response.json())
                        .then(infos => {
                            let newInfo = {}
                            infos.forEach(attr => newInfo[makeAttrKey(attr)] = attr);
                            useStore.setState(state => ({info: {...state.info, ...newInfo}}));
                            resolve(newInfo);
                        })
                });
        });
    await Promise.allSettled(promises);
}

// Get actual archived attribute data for some attributes.
async function fetchData (attributes) {
    if (attributes.length === 0) {
        return;
    }
    const { loading, info, timeRange, aggLimit, aggFunc, aggBuckets, axes } = useStore.getState();
    const newLoading = new Set(loading);
    for (let attr of attributes) {
        newLoading.add(makeAttrKey(attr));
    }
    useStore.setState({ loading: newLoading });
    const t0 = new Date();
    const data = await Promise.all(attributes.map(
        attr => getAttributeData(
            attr,
            info[makeAttrKey(attr)],
            timeRange,
            aggLimit,
            attr.aggFunc || aggFunc,
            axes.find(axis => attr.axis === axis.id),
            aggBuckets,
        ).then(attr => {

            // Data for one attribute has arrived

            const t1 = new Date();
            console.info(`Fetch data for att_conf_id ${attr.att_conf_id} took ${t1 - t0} ms`);
            // useState.setState({data: {...data, [makeAttrKey(attr)]: attr.data}});

            // Update the plot
            let { axes } = useStore.getState();
            const axis = axes.find(axis => axis.id === attr.axis);
            if (axis.auto) {
                // Adjust axis to show the data, but never shrink the view
                // We will do that after all the attributes have arrived
                // TODO: could be done per axis, actually.
                // TODO: maybe this stuff should be done in the plot instead?
                let minValue, maxValue;
                if (axis.scale === "log" && attr.logLimits[0] !== null && attr.logLimits[1] !== null
                    || axis.scale === "lin" && attr.limits[0] !== null && attr.limits[1] !== null) {
                    if (axis.scale === "log") {
                        minValue = Math.max(0, Math.min(axis.limits[0], attr.logLimits[0])),
                        maxValue = Math.max(axis.limits[1], attr.logLimits[1]);
                    } else {
                        minValue = Math.min(axis.limits[0], attr.limits[0]),
                        maxValue = Math.max(axis.limits[1], attr.limits[1]);
                    }
                    const axisIndex = axes.indexOf(axis);
                    axes[axisIndex] = {
                        ...axis,
                        limits: [minValue, maxValue]
                    };
                }
            }

            // Loading spinner
            const { data, loading } = useStore.getState();
            let newLoading = new Set(loading);
            const key = makeAttrKey(attr);
            newLoading.delete(key);
            useStore.setState({ data: {...data, [key]: attr}, loading: newLoading, axes });
            return attr;
        }).catch(e => {

            if (e == "Aborted") {
                // Aborted. This is not an error, there is a new query going on.
                console.info("Aborted old fetch of", attr.att_conf_id)
                return
            }

            // TODO: show something to the user
            setStatusMessage(`Error getting data for att_conf_id ${attr.att_conf_id}, see console for more detail.`, true);
            console.error(`Error getting data for att_conf_id ${attr.att_conf_id}:`, e)
            // Loading spinner
            const {loading} = useStore.getState();
            let newLoading = new Set(loading);
            newLoading.delete(makeAttrKey(attr));
            useStore.setState({loading: newLoading})
        })

    )).then(maybeAttrs => {
        const attrs = maybeAttrs.filter(attr => attr !== undefined),
              badAttrs = maybeAttrs.filter(attr => attr === undefined);
        if (attrs.length === 0) {
            // Seems we got no actual data
            return;
        }
        const { data, axes } = useStore.getState();
        // attrs.forEach(attr => data[makeAttrKey(attr)] = attr);

        // this.setState(state => ({data: {...state.data, ...data}}));

        // Now we got all the data, we can finally correct auto scaled axis limits.
        const newAxes = axes.map(axis => {
            let newAxis = {...axis};
            if (axis.auto) {
                const axisAttrs = getAxisAttributes(axis.id),
                      axisAttrData = axisAttrs.map(attr => data[makeAttrKey(attr)]),
                      axisLimits = getAxisLimits(axisAttrData, axis.scale === "log");
                newAxis.limits = axisLimits;
            }
            return newAxis;
        });
        useStore.setState({axes: newAxes});  // TODO fixme
        if (badAttrs.length === 0) {
            const t1 = new Date();
            setStatusMessage(`Loaded data for ${attrs.length} attributes in ${(t1 - t0) / 1000} s.`);
        }
        // const {loading} = get();
        // let newLoading = new Set(loading);
        // for (let attr of maybeAttrs) {
        //     newLoading.delete(makeAttrKey(attr));
        // }
        // set({loading: newLoading})

        // TODO load all?
        fetchParameters(attributes);
        fetchHistory(attributes);
    })  //

};

// Fetch attribute configuration parameters (unit, label...)
// These also come as "events"; an array of changes.
async function fetchParameters (attributes) {
    if (attributes.length === 0) {
        return;
    }
    const { timeRange, parameters } = useStore.getState();
    Promise.all(
        attributes.map(
            (attribute) => {
                const params = new URLSearchParams([
                    ["db", attribute.db],
                    ["att_conf_id", attribute.att_conf_id],
                    ["start_time", timeRange.start.toISOString()],
                    ["end_time",  timeRange.end.toISOString()],
                ]);
                return fetch("api/attribute/parameters?" + params)
                    .then(response => {
                        if (response.ok)
                            return response.json();
                        throw response
                    })
                    .then(results => {
                        if (results.length === 0) {
                            return;
                        }
                        const newAxes = [],
                              attrParameters = results[0],  // TODO take last?
                              paramsByAttr = {};

                        // const parameters = {
                        //     ...parameters,
                        //     [makeAttrKey(attrParameters)]: attrParameters,
                        // }
                        return attrParameters;
                    })
                    .catch(error => console.error(`Error fetching parameters for ${attribute.att_conf_id}:`, error))
            })
    )
        .then(attrParams => {
            let newParameters = {};
            attrParams.forEach(
                // TODO workaround? Can this really happen?
                params => {
                    if (params) {
                        newParameters[makeAttrKey(params)] = params
                    }
                });
            useStore.setState({parameters: {...parameters, ...newParameters}});
            updateAxisInfo();
        })
}

// Fetch attribute "history" (HDB++ related events, like add/start/stop archiving)
async function fetchHistory (attributes) {
    if (attributes.length === 0) {
        return;
    }

    let attrPerDb = {};
    // We need to make one call per db, that's how the API works
    attributes.forEach(attr => {
        if (attrPerDb[attr.db]) {
            attrPerDb[attr.db].push(attr.att_conf_id);
        } else {
            attrPerDb[attr.db] = [attr.att_conf_id];
        }
    });
    const { timeRange } = useStore.getState();
    const startTime = timeRange.start.toISOString(),
          endTime = timeRange.end.toISOString();
    Promise.all(Object.entries(attrPerDb).map(([db, att_conf_ids]) => {
        const params = new URLSearchParams([
            ["db", db],
            ...att_conf_ids.map(att_conf_id => ["att_conf_id", att_conf_id]),
            ["start_time", startTime],
            ["end_time", endTime],
        ]);
        return fetch("api/attribute/history?" + params)
            .then(response => {
                if (response.ok)
                    return response.json();
                throw response;
            })
            .then(history => {
                const parsedHistory = history.map(d => ({...d, db, event_time: new Date(d.event_time)}));
                // TODO: separate by attribute?
                return parsedHistory;
            })
            .catch(error => console.error(`Error fetching history for ${attributes.map(a => a.att_conf_id)}:`, error))
    }))
        .then(historyPerDb => {
            useStore.setState({history: Array.prototype.concat(...historyPerDb)});
        });
};


// ==== Store actions ====

// Load state from the current URL
// We use the hash part (everything after "#") because it's client only, never sent to the server
// Otherwise it should always parse as a proper URL query string.
export function loadFromURL() {
    const urlState = parseURL(document.location.hash.slice(1), DEFAULTS);
    useStore.setState(urlState);
    const { attributes } = useStore.getState();
    fetchInitialAttributes(attributes)
}

export function setDatabase(database) {
    useStore.setState({database});
    setURL();
}

export function setControlsystem (cs) {
    useStore.setState({controlsystem: cs});
    setURL();
}

export async function addAttributes (attributesToAdd, axis) {
    const { attributes, database } = useStore.getState();
    // avoid adding attributes twice
    const newAttributes = attributesToAdd
          .filter(attr => !attributes.find(a => makeAttrKey(a) === makeAttrKey(attr)))
    var attributesToFetch = [];
    for (const attr of newAttributes) {
        const newAttr = {
            db: database,
            att_conf_id: attr.att_conf_id,
            axis,
            color: getColor([...attributes, ...attributesToFetch].map(a => a.color)),
        };
        attributesToFetch.push(newAttr);
    }
    useStore.setState({attributes: [...attributes, ...attributesToFetch]});
    setURL();
    const infos = await fetchInfo(attributesToFetch);
    await fetchData(attributesToFetch);
}

export function removeAttributes(keys) {
    let newAttributes = [],
        newInfo = {},
        newData = {},
        axesAffected = new Set();
    const {attributes, info, data, axes, history, parameters, current} = useStore.getState();

    for (let attr of attributes) {
        const key = makeAttrKey(attr);
        if (!keys.includes(key)) {
            newAttributes.push(attr);
            newInfo[key] = info[key];
            newData[key] = data[key];
        } else {
            axesAffected.add(attr.axis);
        }
    }

    // Recalculate auto limits for affected axes
    let newAxes = [];
    for (const axis of axes) {
        if (axesAffected.has(axis.id)) {
            // Attributes were removed from this axis, see if something should be done
            const axisAttrs = newAttributes.filter(attr => attr.axis === axis.id);
            if (axisAttrs.length === 0) {
                // Axis is empty; reset settings to default
                newAxes.push({...axis, limits: [-1, 1], auto: true, scale: "lin"});
                continue;
            }
            if (axis.auto) {
                // Axis is auto-scaling; recalculate limits
                const axisAttrDatas = axisAttrs.map(attr => newData[makeAttrKey(attr)]),
                      limits = getAxisLimits(axisAttrDatas,
                                             axis.scale === "log");
                newAxes.push({...axis, limits: limits});
            } else {
                newAxes.push(axis);
            }
        } else {
            newAxes.push(axis);
        }
    }

    const newHistory = history
          .filter(event => !keys.includes(makeAttrKey(event)));

    var newParameters = {...parameters};
    keys.forEach(key => delete newParameters[key]);

    // Check if any of the removed attributes was "current"
    let currentCurrent = current;
    if (current !== null) {
        const [currentType, currentKey] = current;
        if (currentType === "attribute" && keys.includes(currentKey)) {
            currentCurrent = null;
        }
    }
    useStore.setState({
        attributes: newAttributes,
        info: newInfo,
        data: newData,
        parameters: newParameters,
        history: newHistory,
        axes: newAxes,
        selected: new Set(),  // We've probably just removed the selected attributes
        current: currentCurrent,
    })
    updateAxisInfo();
    setURL();
};


export function setTimeRange (range) {
    const { livePeriod,} = useStore.getState();
    if (livePeriod) {
        setLiveHistoryPeriod(0, 0);
    }
    justSetTimeRange(range)
};

export function justSetTimeRange ([start, end]) {
    useStore.setState({timeRange: {start, end}})
    // Whenever the time range changes, fetch new data
    const { attributes, livePeriod } = useStore.getState();
    if (!livePeriod) {
        setURL();
    }
    fetchData(attributes);

};

export function updateAxisInfo () {
    const { parameters, axes } = useStore.getState();

    // Check each axis; if all attributes have the same unit/label
    // we consider that the unit/label of the axis.
    let axisUnits = {}, axisLabels = {};
    Object.entries(parameters)
        .forEach(([key, {unit, label}]) => {
            const attr = getAttribute(key),
                  axis = getAttributeAxis(attr);
            if (!axisUnits[axis.id])
                axisUnits[axis.id] = unit;
            else if (unit !== axisUnits[axis.id]) {
                axisUnits[axis.id] = "mixed units"
            }
            if (!axisLabels[axis.id])
                axisLabels[axis.id] = label;
            else if (label !== axisLabels[axis.id]) {
                delete axisLabels[axis.id];
            }
        });
    let newAxes = [];
    axes.forEach(axis => {
        const newAxis = {
            ...axis,
            unit: axisUnits[axis.id],
            label: axisLabels[axis.id]
        };
        newAxes.push(newAxis);
    });
    useStore.setState({axes: newAxes});
};

export function setStatusMessage (message, isError) {
    const t = new Date();
    const z = t.getTimezoneOffset() * 60 * 1000;
    const tLocal = t - z;
    const timeString = (new Date(tLocal)).toISOString().slice(0, 19).replace("T", " ")
    useStore.setState({
        statusMessage: `${timeString}: ${message}`,
        statusError: isError,
    });
};

export function setAxisAuto (axisId, value) {
    let newAxes;
    const { axes, data } = useStore.getState();
    if (value) {
        // Turning auto on; calculate new limits
        const axisAttrs = getAxisAttributes(axisId),
              axisAttrData = axisAttrs.map(attr => data[makeAttrKey(attr)]),
              axis = axes.find(ax => ax.id === axisId),
              limits = getAxisLimits(axisAttrData, axis.scale === "log");
        newAxes = axes.map(axis => (
            axis.id === axisId? {...axis, limits, auto: true} : axis));

    } else {
        // Turning auto off; don't touch limits
        newAxes = axes.map(axis => (
            axis.id === axisId? {...axis, auto: false} : axis));
    }
    useStore.setState({axes: newAxes});
    setURL();
};

export function setAxisScale (axisId, scale) {
    const { data, axes } = useStore.getState();
    const axis = axes.find(ax => ax.id === axisId);
    let newAxes;

    if (axis.auto) {
        // Axis has auto limits on; recalculate them
        const axisAttrs = getAxisAttributes(axisId),
              axisAttrData = axisAttrs.map(attr => data[makeAttrKey(attr)]),
              limits = getAxisLimits(axisAttrData, scale === "log");
        newAxes = axes.map(axis => (
            axis.id === axisId? {...axis, limits, scale: scale} : axis));
    } else {
        newAxes = axes.map(axis => (
            axis.id === axisId? {...axis, scale: scale} : axis));
    }
    useStore.setState({axes: newAxes});
    setURL();
}

export function setAxisLimits (axisId, [minValue, maxValue]) {
    const axes = useStore.getState().axes.map(axis => {
        if (axis.id === axisId) {
            return {
                ...axis,
                auto: false,  // disable auto on manual Y axis changes
                limits: [
                    minValue,
                    maxValue
                ]
            }
        } else {
            return axis;
        }
    })
    useStore.setState({axes});
    setURL();
};

export function setSelected (key, value) {
    let selected = new Set(useStore.getState().selected);
    if (!value) {
        selected.delete(key);
    } else {
        selected.add(key);
    }
    useStore.setState({selected});
}

export function setCurrent (what, key) {
    useStore.setState({current: [what, key]});
}

export function setAggBuckets (aggBuckets) {
    // TODO here we might want to re-fetch data as the aggregation has changed.
    // However this may cause some issues at startup. Investigate.
    useStore.setState({aggBuckets})
}

export function setAggLimit (aggLimit) {
    useStore.setState({aggLimit});
    const { attributes } = useStore.getState();
    fetchData(attributes);
    setURL();
};

export function setAggFunc (aggFunc) {
    useStore.setState({aggFunc});
    setURL();
    // Figure out if we need to refetch data for any attributes
    const { attributes } = useStore.getState();
    const attributesToFetch = attributes.filter(attr => {
        if (attr.aggFunc) {
            return false;  // Attribute has configured aggfunc; not affected by global config
        }
        const { data } = getAttribute(makeAttrKey(attr));
        if (data.aggSize != null) {
            return true;  // Attribute is aggregated, needs refetch
        };
    });
    fetchData(attributesToFetch);
};

export function setAttributeAggFunc (aggFunc, keys) {
    const attributes = useStore.getState().attributes.map(attr => {
        if (keys.includes(makeAttrKey(attr))) {
            return {...attr, aggFunc};
        } else {
            return attr;
        }
    });
    useStore.setState({attributes});
    setURL();
    fetchData(attributes.filter(a => keys.includes(makeAttrKey(a))));
};

export function setShowErrorGaps (value) {
    useStore.setState({showErrorGaps: value});
};

export function setShowHistory (value) {
    useStore.setState({showHistory: value})
};

export function setAttributeLook (key, name, value) {
    const attributes = useStore.getState().attributes.map(attr => {
        if (makeAttrKey(attr) === key) {
            return {...attr, [name]: value};
        } else {
            return attr;
        }
    });
    useStore.setState({attributes});
    setURL();
};

export function setLiveHistoryPeriod (history, period) {
    useStore.setState({liveHistory: history, livePeriod: period})
    setupLive();
    setURL();
};

// ===== Functions related to live updating =====
// Live updates means we set a fixed time window size, e.g. 1 hour, and then
// repeated at a given period, e.g. one minute, we load the last hour of data
// and display it.

let liveTimeout = null;  // JS timeout, triggering the next reload

// Perform a live update, and schedule the next one
function liveUpdate() {
    // This is an attempt to avoid running updates while the page is not
    // actually visible (e.g. in a tab somewhere). The point is that otherwise
    // it would be easy to forget about live pages e.g. in background tabs,
    // eating lots of resources for no use. It relies on
    // https://www.w3.org/TR/page-visibility/
    if (document.hidden) {
        console.debug("Hidden, pausing live updates.")
        document.addEventListener("visibilitychange", liveUpdate);
        return;
    } else {
        console.debug("Visible, resuming live updates.")
        document.removeEventListener("visibilitychange", liveUpdate);
    }
    if (liveTimeout) {
        // Make sure there's no existing timeout
        clearTimeout(liveTimeout);
        liveTimeout = null;
    }
    const { loading, liveHistory } = useStore.getState();
    if (!liveHistory) {
        // Stop updating if live updates have been turned off
        return;
    }

    const period = getLivePeriod();
    if (loading.size === 0) {
        const start = new Date((new Date()) - liveHistory * 1000);
        const end = new Date();
        justSetTimeRange([start, end]);
    } else {
        console.warn("Loading data is taking too long; skipping live update");
    }
    liveTimeout = setTimeout(liveUpdate, period * 1000);
}


// Start live reloading
function setupLive(skipInitialLoad) {
    const { liveHistory, livePeriod } = useStore.getState();
    clearTimeout(liveTimeout);
    if (livePeriod) {
        if (!skipInitialLoad) {
            liveUpdate();
        }
        const period = getLivePeriod();
        if (period > livePeriod) {
            console.warn("Live period limited to", period, "seconds.")
        }
        liveTimeout = setTimeout(liveUpdate, period * 1000);
    }
}


// ===== Store getter functions =====

export function getLivePeriod () {
    // Set a reasonable minimum limit for the live update period.
    // Maybe decrease this later, when we know it won't be a problem
    // This is a heuristic, seems to give reasonable results...
    const { liveHistory, livePeriod } = useStore.getState();
    const minPeriod = Math.pow(liveHistory, 0.35);
    const period = Math.max(3, minPeriod, livePeriod);
    if (period > livePeriod) {
        console.warn("Live period limited to", period, "seconds.")
    }
    return period
};

// Convenience function to collect everything we have for a given attribute
export function getAttribute (key) {
    const { attributes, info, data, parameters } = useStore.getState();
    const attr = attributes.find(attr => makeAttrKey(attr) === key);
    if (attr) {
        return {
            ...attr,
            info: info[key],
            data: data[key],
            parameters: parameters[key],
        }
    }
};

export function getAxisAttributes (axisId) {
    return useStore.getState().attributes.filter(attr => attr.axis === axisId);
};

export function getAttributeAxis (attr) {
    return useStore.getState().axes.find(axis => attr.axis === axis.id);
};
