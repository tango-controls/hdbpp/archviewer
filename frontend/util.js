import {scaleOrdinal, schemeCategory10} from "d3";


// Create a list of nicely different colors to rotate over
// TODO these are supposed to be good but I haven't really gone far
// into this. Maybe it can be improved.
var myColor = scaleOrdinal().domain([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    .range(schemeCategory10);

var COLORS = [];
for (var i = 0; i < 10; i++) {
    COLORS.push(myColor(i));
}

// Produce a color, given the current list of used colors
export function getColor(usedColors) {
    var colorUsages = {};
    for (color of usedColors) {
        // ignore customized colors, we can't handle those
        if (COLORS.includes(color)) {
            if (color in colorUsages) {
                colorUsages[color] += 1;
            } else {
                colorUsages[color] = 1;
            }
        }
    }
    // Now figure out which standard color is least used. If there are several,
    // let's use the one earliest in the COLORS list.
    var nextColor = -1;
    for (const [i, c] of COLORS.entries()) {
        const usages = colorUsages[c] || 0;
        var next = true;
        for (c2 of COLORS) {
            if ((colorUsages[c2] || 0) < usages) {
                next = false;
                break;  // Done, we know this one is not going to be used
            }
        }
        if (next) {
            // this color is among the least popular and since we're
            // going in COLORS order already, we know this is the one to use.
            nextColor = i;
            break;
        }
    }
    if (nextColor > -1) {
        return COLORS[nextColor];
    }
    return COLORS[0];
}


// Function wrapper that ensures that the function is not called immediately
// but only after it has not been called again for X ms. This is useful for expensive
// operations that we don't want to do too often (such as fetching data).
export function debounce(func, timeout = 500){
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => { func.apply(this, args); }, timeout);
    };
}

// Calculate "optimal" lower and upper limits for the axis, given current data
export function getAxisLimits(axisAttrDatas, logarithmic) {
    let minValue = null,
        maxValue = null,
        lower, upper;

    if (axisAttrDatas.length === 0) {
        return [-1, 1];  // Default when empty
    }

    axisAttrDatas.forEach(attr => {
        if (attr === undefined) {
            // No data for this attribute, presumably there was an error.
            return;
        }
        if (logarithmic) {
            [lower, upper] = attr.logLimits;
        } else {
            [lower, upper] = attr.limits;
        }
        if (lower === null || upper === null) {
            // This means there were no points; should not influence limits.
            // TODO: better handling of this case; it should not get this far.
            return;
        }
        if (minValue === null) {
            minValue = lower;
        } else {
            minValue = Math.min(minValue, lower);
        }
        if (maxValue === null) {
            maxValue = upper;
        } else {
            maxValue = Math.max(maxValue, upper);
        }
    });

    // Calculate "padding"; extra space to add to the top and bottom
    let lowerLimit, upperLimit;
    if (logarithmic) {
        const logMin = Math.log(minValue);
        const logMax = Math.log(Math.max(maxValue, Number.MIN_VALUE));
        const logDifference = logMax - logMin;
        if (logDifference === 0) {
            const padding = minValue * 0.1;
            lowerLimit = Math.max(0, Math.exp(logMin) - padding);
            upperLimit = Math.exp(logMax) + padding;
        } else {
            lowerLimit = Math.exp(logMin);
            upperLimit = Math.exp(logMax);
        }
        // TODO: there's something wrong when there are zeros in the data
    } else {
        const difference = maxValue - minValue;
        if (difference == 0) {
            const padding = difference * 0.1;
            lowerLimit = minValue - padding;
            upperLimit = maxValue + padding;
        } else {
            lowerLimit = minValue;
            upperLimit = maxValue;
        }
    }

    return [lowerLimit, upperLimit];
}


// Helper to let client download data as a file.
export function download(href, name) {
    var a = document.createElement('a');

    a.download = name;
    a.href = href;

    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}
