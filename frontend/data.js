// Functionality for fetching data from the backend

import {tsvParseRows} from "d3";

let abortControllers = {};

const VALUE_PARSERS = {
    "DEV_DOUBLE": parseFloat,
    "DEV_FLOAT": parseFloat,
    "DEV_INT": parseInt,
    "DEV_SHORT": parseInt,
    "DEV_LONG": parseInt,
    "DEV_USHORT": parseInt,
    "DEV_ULONG": parseInt,
    "DEV_UCHAR": parseInt,
    "DEV_ULONG64": parseInt,
    "DEV_LONG64": parseInt,
    "DEV_BOOLEAN": parseInt,
    "DEV_STATE": parseInt,
}

class AttributeSearchError extends Error {
   constructor(response) {
     super();
     this.response = response;
   }
}

// Search attributes by search phrase.
//
// Throws AttributeSearchError on error response from the backend.
export async function attributeSearch(database, controlsystem, phrase) {
    const params = new URLSearchParams({
        db: database,
        cs: controlsystem,
        phrase: phrase,
    });

    const response = await fetch("api/attribute/search?" + params.toString());
    if (response.ok) {
        return await response.json();
    } else {
        throw new AttributeSearchError(resp);
    }
}

// Look-up attribute configure ID from it's full name.
export async function getAttributeByName(database, attrName) {
    const slash = attrName.indexOf("/");
    if (slash === -1) {
       throw new Error("no '/' found in the attribute name");
    }

    const controlsystem = attrName.slice(0, slash);
    const phrase = attrName.slice(slash + 1);

    const res = await attributeSearch(database, controlsystem, phrase);
    if (res.length != 1) {
       throw new Error(`expected to find exactly one attribute, got ${res.length}`);
    }

    return res[0].att_conf_id;
}

export function getAttributeData(attr, info, timeRange, aggLimit, aggFunc, axisScaleType, aggBuckets) {

    // Abort any previous, still running query for this attribute
    let abortController;
    if (abortControllers[attr.att_conf_id]) {
        console.info("Aborting already running fetch for att_conf_id", attr.att_conf_id);
        abortController = abortControllers[attr.att_conf_id];
        try {
            abortController.abort();
        } catch (error) {
            console.warning("Error aborting", attr.att_conf_id, error);
        }
    }
    abortController = new AbortController();
    abortControllers[attr.att_conf_id] = abortController;

    const parseValue = VALUE_PARSERS[info.type] || function (value) { return value };

    function handleRow (row) {
        // data_time, value_r, value_w, quality, att_error_desc_id
        const data_time = new Date(row[0]),
              value_r = parseValue(row[1]),
              value_w = parseValue(row[2]),
              quality = parseInt(row[3]);
        // Calculate the max and min value as we go, to use for autoscaling.
        // We ignore points laying outsize the range as that is confusing.
        if (data_time >= timeRange.start && data_time <= timeRange.end) {
            if (!isNaN(value_r)) {
                if (valueMax === null) {
                    valueMax = value_r;
                } else {
                    valueMax = Math.max(valueMax, value_r);
                }
                if (valueMin === null) {
                    valueMin = value_r;
                } else {
                    valueMin = Math.min(valueMin, value_r);
                }

                // Logarithmic limits (only works for positive numbers)
                // Since the user may switch axis type we will calculate this too
                if (value_r > 0) {
                    if (valueMax === null) {
                        logValueMax = value_r;
                    } else {
                        logValueMax = Math.max(logValueMax, value_r);
                    }
                    if (logValueMin === null) {
                        logValueMin = value_r;
                    } else {
                        logValueMin = Math.min(logValueMin, value_r);
                    }
                }
            }
            // TODO since we don't display write values yet, it makes no sense
            // to use if to calculate min/max...
            // if (!isNaN(value_w)) {
            //     if (valueMax === null) {
            //         valueMax = value_w;
            //     } else {
            //         valueMax = Math.max(valueMax, value_w);
            //     }
            //     if (valueMin === null) {
            //         valueMin = value_w;
            //     } else {
            //         valueMin = Math.min(valueMin, value_w);
            //     }

            //     // Logarithmic limits (only works for positive numbers)
            //     // Since the user may switch axis type we will calculate this too
            //     if (value_r > 0) {
            //         if (valueMax === null) {
            //             logValueMax = value_w;
            //         } else {
            //             logValueMax = Math.max(logValueMax, value_w);
            //         }
            //         if (logValueMin === null) {
            //             logValueMin = value_w;
            //         } else {
            //             logValueMin = Math.min(logValueMin, value_w);
            //         }
            //     }
            // }
        }

        actualNPoints++;

        return {
            data_time,
            value_r,
            value_w,
            quality,
            att_error_desc_id: row[2]
        };
    }

    function handleAggRow (row) {
        // agg_time, agg_value_r, agg_value_w, n_points, n_errors
        const agg_value_r = parseValue(row[1]),
              agg_value_w = parseValue(row[2]);
        if (!isNaN(agg_value_r)) {
            if (valueMax === null) {
                valueMax = agg_value_r;
            } else {
                valueMax = Math.max(valueMax, agg_value_r);
            }
            if (valueMin === null) {
                valueMin = agg_value_r;
            } else {
                valueMin = Math.min(valueMin, agg_value_r);
            }

            // Logarithmic limits
            if (agg_value_r > 0) {
                if (valueMax === null) {
                    logValueMax = agg_value_r;
                } else {
                    logValueMax = Math.max(logValueMax, agg_value_r);
                }
                if (logValueMin === null) {
                    logValueMin = agg_value_r;
                } else {
                    logValueMin = Math.min(logValueMin, agg_value_r);
                }
            }
        }
        return {
            data_time: new Date(row[0]),  // We get UTC
            value_r: agg_value_r,
            value_w: agg_value_w,
            n_points: parseInt(row[3]),
            n_errors: parseInt(row[4]),
        };
    }

    const params = new URLSearchParams({
        db: attr.db,
        att_conf_id: attr.att_conf_id,
        start_time: timeRange.start.toISOString(),
        end_time: timeRange.end.toISOString(),
        agg_limit: aggLimit,
        agg_func: aggFunc,
        agg_buckets: aggBuckets || 1000,
    });

    let valueMax = null,
        valueMin = null;

    let logValueMin = null,
        logValueMax = null;

    let aggSize = null,
        reportedNPoints = null,
        actualNPoints = 0;

    // "Long polling" means requesting an URL, and if it times out we just try
    // again until it returns. Here, it is meant to handle long running
    // backend queries, that could timeout because of gateway or browser limits.
    async function longPoll(urls, abortController) {
        const response = await fetch(urls.result, {signal: abortController.signal})
              .catch(e => e);  // Not sure why this is needed...
        if (abortController.signal.aborted) {
            // A later query for the same attribute issued an abort
            await fetch(urls.cancel, {method: "POST"})
            return null;
        }
        else if (response instanceof TypeError || response.status === 502) {
            // Timeout, try again
            // The TypeError is something firefox throws... not sure why
            // but it is some kind of timeout.
            return await longPoll(urls, abortController);
        } else {
            // Done. This could be the result, or it could be a "real" error
            return response;
        }
    }

    // TODO: use async/await syntax?
    return fetch(`api/attribute/data?` + params)
    // First we initiate the query
        .then(async response => {
            if (response.ok) {
                return response.json();
            }
        })
    /// ...which returns some info including URL to the result
        .then(({info, urls}) => {
            reportedNPoints = info.n_points;
            aggSize = info.agg_size;

            // Then we get the actual result, which may take a long time
            return longPoll(urls, abortController);
        })
    // Done, check if there was an error, or we got aborted
        .then(
            response => {
                if (response === null) {
                    throw "Aborted";
                }
                if (response.ok) {
                    return response.text();
                }
                throw response;
            }
        )
    // Finally, we have the data
        .then((text) => {
            const handler = aggSize? handleAggRow : handleRow;
            const data = tsvParseRows(text, handler);
            if (data.length === 0) {
                reportedNPoints = 0;  // Clearly there wasn't any data
            }
            const limits = [valueMin, valueMax];
            const logLimits = [logValueMin, logValueMax];
            console.debug("logLimits", logLimits);
            if (isNaN(valueMin) || isNaN(valueMax)) {
                return Promise.reject("Bad limits!");
            } else {
                const nPoints = aggSize? reportedNPoints : actualNPoints;
                return Promise.resolve({...attr, nPoints, aggSize, tsvData: text, data, limits, logLimits});
            }
        })
    // Cleanup
        .finally(() => {
            delete abortControllers[attr.att_conf_id];
        });
}
