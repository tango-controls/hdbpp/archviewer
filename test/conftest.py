from datetime import datetime
from multiprocessing import Process
import os
import socket
import time
from urllib.request import urlopen
from urllib.error import URLError

import pytest
import uvicorn

from archviewer.timescale import FakeHDBPPTimescaleConnection

# Need to set these before importing app, or they don't take effect... pretty ugly
DATABASES = ["hdb_test1", "hdb_test2", "hdb_test3"]
os.environ["TIMESCALE_DATABASES"] = ",".join(DATABASES)
os.environ["FAKE_DB"] = "true"

from archviewer.app import app
from archviewer.routes import get_hdbpp  # noqa


class TestHDBPPTimescaleConnection(FakeHDBPPTimescaleConnection):

    """
    Override with static, test friendly data
    """

    FAKE_CONTROLSYSTEMS = {
        "hdb_test1": [
            "test.control.system.1:10000",
            "test.control.system.2:10000",
        ],
        "hdb_test2": [
            "test.control.system.3:10000",
            "test.control.system.4:10000",
        ],
        "hdb_test3": [
            "test.control.system.5:10000",
        ],
    }

    FAKE_ATTRIBUTES = {
        "hdb_test1": {
            0: ("test.control.system.1:10000", "test/hello/0/attr_a", "SCALAR", "DEV_DOUBLE"),
            1: ("test.control.system.1:10000", "test/hello/1/attr_b", "SCALAR", "DEV_DOUBLE"),
            2: ("test.control.system.2:10000", "test/hello/2/attr_a", "SCALAR", "DEV_DOUBLE"),
            3: ("test.control.system.2:10000", "test/hello/3/attr_c", "SCALAR", "DEV_DOUBLE"),
            4: ("test.control.system.2:10000", "test/hello/4/attr_a", "SCALAR", "DEV_DOUBLE"),
            5: ("test.control.system.2:10000", "test/hello/5/attr_c", "SCALAR", "DEV_DOUBLE"),
        },
        "hdb_test2": {
            0: ("test.control.system.3:10000", "test/hello/4/attr_a", "SCALAR", "DEV_DOUBLE"),
            1: ("test.control.system.3:10000", "test/hello/5/attr_b", "SCALAR", "DEV_DOUBLE"),
            2: ("test.control.system.4:10000", "test/hello/6/attr_a", "SCALAR", "DEV_DOUBLE"),
            3: ("test.control.system.4:10000", "test/hello/7/attr_c", "SCALAR", "DEV_DOUBLE"),
        },
        "hdb_test3": {
            0: ("test.control.system.5:10000", "test/hello/1/attr_a", "SCALAR", "DEV_DOUBLE"),
            1: ("test.control.system.5:10000", "test/hello/2/attr_a", "SCALAR", "DEV_DOUBLE"),
            2: ("test.control.system.5:10000", "test/hello/3/attr_a", "SCALAR", "DEV_DOUBLE"),
            3: ("test.control.system.5:10000", "test/hello/4/attr_a", "SCALAR", "DEV_DOUBLE"),
            4: ("test.control.system.5:10000", "test/hello/5/attr_a", "SCALAR", "DEV_DOUBLE"),
            5: ("test.control.system.5:10000", "test/hello/6/attr_a", "SCALAR", "DEV_DOUBLE"),
            6: ("test.control.system.5:10000", "test/hello/7/attr_a", "SCALAR", "DEV_DOUBLE"),
            7: ("test.control.system.5:10000", "test/hello/8/attr_a", "SCALAR", "DEV_DOUBLE"),
            8: ("test.control.system.5:10000", "test/hello/9/attr_a", "SCALAR", "DEV_DOUBLE"),
            9: ("test.control.system.5:10000", "test/hello/10/attr_a", "SCALAR", "DEV_DOUBLE"),
            10: ("test.control.system.5:10000", "test/hello/11/attr_a", "SCALAR", "DEV_DOUBLE"),
            11: ("test.control.system.5:10000", "test/hello/12/attr_a", "SCALAR", "DEV_DOUBLE"),
        }
    }

    FAKE_HISTORY = {
        "hdb_test1": {
            0: [
                {
                    "att_history_event_id": 100,
                    "att_conf_id": 0,
                    "event_time": datetime(2022, 1, 1, 1, 12, 32, 123456).astimezone(),
                    "event": "start",
                },
                {
                    "att_history_event_id": 102,
                    "att_conf_id": 0,
                    "event_time": datetime(2022, 1, 2, 4, 12, 32, 981234).astimezone(),
                    "event": "stop",
                },
            ],
            1: [
                {
                    "att_history_event_id": 101,
                    "att_conf_id": 1,
                    "event_time": datetime(2022, 1, 1, 11, 2, 56, 0).astimezone(),
                    "event": "start",
                },
            ]
        }
    }

    FAKE_PARAMETERS = {
        "hdb_test1": {
            1: [
                {
                    "att_conf_id": 1,
                    "recv_time": datetime(2022, 1, 1, 5, 46, 29, 2872).astimezone(),
                    "label": "some label",
                    "unit": "km",
                    "standard_unit": "No standard unit",
                    "display_unit": "No display unit",
                    "format": "%.3f",
                    "archive_rel_change": 1,
                    "archive_abs_change": "Not specified",
                    "archive_period": "Not specified",
                    "description": "Some description",
                    "details": None,
                },
                {
                    "att_conf_id": 1,
                    "recv_time": datetime(2022, 1, 1, 19, 6, 29, 2872).astimezone(),
                    "label": "some label",
                    "unit": "ly",  # Changed!
                    "standard_unit": "No standard unit",
                    "display_unit": "No display unit",
                    "format": "%.3f",
                    "archive_rel_change": 1,
                    "archive_abs_change": "Not specified",
                    "archive_period": "Not specified",
                    "description": "Some description",
                    "details": None,
                }
            ]
        }
    }


def get_test_hdbpp(db):
    return TestHDBPPTimescaleConnection(
        host="testhost",
        port=12345,
        database=db,
        user="testuser",
        password="testpassword",
    )


app.dependency_overrides[get_hdbpp] = get_test_hdbpp


def get_open_port():
    """Utility to get an unused local port"""
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("", 0))
    s.listen(1)
    port = s.getsockname()[1]
    s.close()
    return port


@pytest.fixture(scope="module")
def archviewer():

    """Runs the app in a process, on a random port"""

    port = get_open_port()
    proc = Process(
        target=uvicorn.run,
        kwargs={
            "app": app,
            "host": "127.0.0.1",
            "port": port,
            "log_level": "info"
        })
    proc.start()
    url = f"http://localhost:{port}"
    for _ in range(30):
        try:
            urlopen(url)
            break
        except URLError:
            pass
        time.sleep(0.1)
    else:
        raise RuntimeError("Backend didn't start up properly!")

    yield url

    proc.kill()


@pytest.fixture
def anyio_backend():
    # Don't care about other backends, and tests are broken under "trio"
    # See https://github.com/tiangolo/fastapi/issues/4473
    return 'asyncio'
