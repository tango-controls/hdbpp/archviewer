"""
Tests that directly access the HTTP API.
"""

import csv
from datetime import datetime

from httpx import AsyncClient, ASGITransport
import pytest

from archviewer.app import app
from archviewer.routes import ATT_PARAMETERS


@pytest.mark.anyio
async def test_controlsystems():
    async with AsyncClient(transport=ASGITransport(app=app), base_url="http://test") as client:
        response = await client.get("/api/controlsystems", params={"db": "hdb_test1"})
    assert response.status_code == 200
    assert response.json() == [
        {'cs_name': 'test.control.system.1:10000'},
        {'cs_name': 'test.control.system.2:10000'},
    ]


@pytest.mark.anyio
async def test_attribute_search():
    async with AsyncClient(transport=ASGITransport(app=app), base_url="http://test") as client:
        response = await client.get("/api/attribute/search",
                                    params={
                                        "db": "hdb_test1",
                                        "cs": "test.control.system.2:10000",
                                        "phrase": "*/hello/2/*"
                                    })
    assert response.status_code == 200
    attributes = response.json()
    assert len(attributes) == 1
    assert attributes[0]["att_conf_id"] == 2


@pytest.mark.anyio
async def test_attribute_info():
    async with AsyncClient(transport=ASGITransport(app=app), base_url="http://test") as client:
        response = await client.get("/api/attribute/info",
                                    params={
                                        "db": "hdb_test1",
                                        "cs": "test.control.system.2:10000",
                                        "att_conf_id": [2, 3],
                                    })

    assert response.status_code == 200
    attributes = response.json()
    assert len(attributes) == 2
    assert attributes[0]["att_conf_id"] == 2
    assert attributes[1]["att_conf_id"] == 3


@pytest.mark.anyio
async def test_attribute_data():
    async with AsyncClient(transport=ASGITransport(app=app), base_url="http://test") as client:
        # Request data
        response = await client.get("/api/attribute/data",
                                    params={
                                        "db": "hdb_test1",
                                        "att_conf_id": 2,
                                        "start_time": "2022-01-01T00:00:00Z",
                                        "end_time": "2022-01-02T00:00:00Z"
                                    })
        assert response.status_code == 202
        result = response.json()
        info = result["info"]
        assert isinstance(info["n_points"], int)
        assert info["agg_size"] is None
        urls = result["urls"]
        result_url = urls["result"]

        # Get the actual data
        response = await client.get(result_url)
        data = iter(response.text.splitlines())
        reader = csv.reader(data, delimiter="\t")
        for line in reader:
            timestamp, read_value, write_value, quality, error = line
            assert datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S.%f+00")
            float(read_value)


@pytest.mark.anyio
async def test_attribute_history():
    async with AsyncClient(transport=ASGITransport(app=app), base_url="http://test") as client:
        response = await client.get("/api/attribute/history",
                                    params={
                                        "db": "hdb_test1",
                                        "att_conf_id": 0,
                                        "start_time": "2022-01-01T00:00:00Z",
                                        "end_time": "2022-01-02T00:00:00Z",
                                    })
    assert response.status_code == 200
    history = response.json()
    assert len(history) == 1
    assert history[0]["att_history_event_id"] == 100


@pytest.mark.anyio
async def test_attribute_parameters():
    async with AsyncClient(transport=ASGITransport(app=app), base_url="http://test") as client:
        response = await client.get("/api/attribute/parameters",
                                    params={
                                        "db": "hdb_test1",
                                        "att_conf_id": 1,
                                        "start_time": "2022-01-01T00:00:00Z",
                                        "end_time": "2022-01-02T00:00:00Z",
                                    })
    assert response.status_code == 200
    parameters = response.json()
    assert len(parameters) == 2
    # We only expect to get changed parameters.
    for param in ATT_PARAMETERS:
        if param != "unit":
            assert param not in parameters[1]
        else:
            assert param in parameters[1]
