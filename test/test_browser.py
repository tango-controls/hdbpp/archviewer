"""
Tests that run a real web browser to interact with the frontend.
Uses "playwright" to handle this, with its pytest plugin.

Note that the browser normally runs "headless" i.e. not opening a window.
If you want to see the window to figure out what's happening, you can
add "--headed" to the pytest arguments.
"""

import re
from urllib.parse import parse_qs
from playwright.sync_api import Page, expect


def test_load_app(archviewer, page):
    page.goto(archviewer)
    page.wait_for_selector("#database select")


def test_add_one_attribute(archviewer, page):
    page.goto(archviewer)
    page.select_option("#database select", "hdb_test1")
    page.select_option("#controlsystem select", "test.control.system.1:10000")
    page.fill("#search input", "*/hello/1/*")
    page.press("#search input", "Enter")
    expect(page.locator('#attribute-search-results option')).to_have_count(1)
    page.select_option("#attribute-search-results",
                       label=["test/hello/1/attr_b"])
    page.click("button.add.left")

    # Wait for data load
    page.wait_for_selector(".canvas-container canvas")

    # Check that the attribute is in the list
    expect(page.locator(".axis-attribute-name")).to_have_text("test/hello/1/attr_b")

    # Check that we load label and unit
    page.wait_for_selector(".y-axis-0.unit")
    unit = page.query_selector(".y-axis-0.unit")
    assert unit.inner_html() == "some label [ly]"


def test_add_several_attributes(archviewer, page):
    page.goto(archviewer)
    page.select_option("#database select", "hdb_test1")
    page.select_option("#controlsystem select", "test.control.system.2:10000")
    page.fill("#search input", "*/hello/*")
    page.press("#search input", "Enter")
    expect(page.locator('#attribute-search-results option')).to_have_count(4)
    # Select 3 attributes
    (page.query_selector('#attribute-search-results option[value="3"]')
     .click(modifiers=['Shift']))
    (page.query_selector('#attribute-search-results option[value="5"]')
     .click(modifiers=['Shift']))
    page.click("button.add.right")

    page.wait_for_selector(".canvas-container canvas")

    # Check that the attributes are in the list
    attrs = page.query_selector_all(".axis-attribute")
    assert len(attrs) == 3
    first, second, third = page.locator(".axis-attribute-name").all()
    expect(first).to_have_text("test/hello/3/attr_c")
    expect(second).to_have_text("test/hello/4/attr_a")
    expect(third).to_have_text("test/hello/5/attr_c")


def test_remove_attribute(archviewer, page):
    page.goto(archviewer)
    page.select_option("#database select", "hdb_test1")
    page.select_option("#controlsystem select", "test.control.system.2:10000")
    page.fill("#search input", "*/hello/*")

    # Add some attributes
    page.press("#search input", "Enter")
    expect(page.locator('#attribute-search-results option')).to_have_count(4)
    (page.query_selector('#attribute-search-results option[value="3"]')
     .click(modifiers=['Shift']))
    (page.query_selector('#attribute-search-results option[value="5"]')
     .click(modifiers=['Shift']))
    page.click("button.add.right")

    # Check that the attributes are in the list
    attrs = page.query_selector_all(".axis-attribute")
    assert len(attrs) == 3

    # Select the first attribute
    page.locator(".axis-attribute-name").first.click()

    remove_button = page.locator("button.remove").first
    remove_button.click(no_wait_after=True)

    # Check that we removed one
    attrs = page.query_selector_all(".axis-attribute")
    assert len(attrs) == 2


def test_change_default_agg_func(archviewer, page: Page):
    # Preload one attribute, with aggregation
    page.goto(archviewer + "/#db=hdb_test1&cs=test.control.system.2:10000&attr=hdb_test1;5;0,#00ff00;&agg_limit=1")

    # Check that the attribute is loaded, with default aggregation function
    attrs = page.query_selector_all(".axis-attribute")
    assert len(attrs) == 1
    agg_indicator = page.locator(".aggregated").first
    expect(agg_indicator).to_be_visible()
    expect(agg_indicator).to_have_text("avg")

    # Selecting another default agg func should reload the attribute
    # with the new default agg
    page.select_option("select.agg-func", "max")
    expect(agg_indicator).to_have_text("max")


def test_open_attribute_config(archviewer, page: Page):
    # Preload one attribute
    page.goto(archviewer + "/#db=hdb_test1&cs=test.control.system.2:10000&attr=hdb_test1;5;0,#00ff00;")

    # Wait for the attribute to load
    expect(page.locator(".status-message")
           ).to_have_text(re.compile(".* Loaded data for 1 attributes .*"))

    # Click the attribute color button
    page.locator("button.color-patch").first.click()

    # Check that the attribute config panel is open
    config = page.locator("footer.config")
    expect(config).to_be_visible()
    expect(config.locator(".heading")).to_have_text("Attribute settings")
    expect(config.locator(".attribute-name")).to_have_text("test/hello/5/attr_c")


def test_auto_attribute_colors(archviewer, page):
    page.goto(archviewer)
    page.select_option("#database select", "hdb_test3")
    page.select_option("#controlsystem select", "test.control.system.5:10000")
    page.fill("#search input", "*")

    # Add 10 attributes
    page.press("#search input", "Enter")
    expect(page.locator('#attribute-search-results option')).to_have_count(12)
    (page.query_selector('#attribute-search-results option[value="0"]')
     .click(modifiers=['Shift']))
    (page.query_selector('#attribute-search-results option[value="9"]')
     .click(modifiers=['Shift']))
    page.click("button.add.right")

    attrs = page.query_selector_all(".axis-attribute")
    assert len(attrs) == 10

    # Check that we have 10 different colors
    color_buttons = page.query_selector_all("button.color-patch")
    styles = {
        cb.get_attribute("style")
        for cb in color_buttons
    }
    assert len(styles) == 10

    # Remove the first attribute
    page.locator(".axis-attribute-name").first.click()
    remove_button = page.locator("button.remove").first
    remove_button.click(no_wait_after=True)

    # Add another attribute
    page.query_selector('#attribute-search-results option[value="11"]').click()
    page.click("button.add.right")
    expect(page.locator(".axis-attribute")).to_have_count(10)

    # Check that we still have 10 different colors
    color_buttons = page.query_selector_all("button.color-patch")
    styles = {
        cb.get_attribute("style")
        for cb in color_buttons
    }
    assert len(styles) == 10


def test_named_attribute(archviewer, page):
    #
    # Test loading a URL with a named attribute
    #

    params = (
        "db=hdb_test1&cs=test.control.system.4:10000&attr=hdb_test1;"
        + "test.control.system.1:10000/test/hello/1/attr_b;0;#1f77b4;&axis=0,l,lin,,"
    )
    page.goto(f"{archviewer}/#{params}")

    # Wait for URL with attribute conf IDs to load
    page.wait_for_url(re.compile(r".*attr=hdb_test1;\d+;\d+;#"))

    # Check that the attribute is in the list
    attrs = page.query_selector_all(".axis-attribute")
    assert len(attrs) == 1

    # We now expect the URL to be rewritten, using attribute's conf ID
    new_url = page.url
    params = parse_qs(new_url[len(archviewer) + 2 :])

    # Check that URL lists correct attribute using its conf ID
    assert params.get("db") == ["hdb_test1"]
    assert params.get("cs") == ["test.control.system.4:10000"]
    assert params.get("attr") == ["hdb_test1;1;0;#1f77b4;"]
