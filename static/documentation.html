<head>
    <style>

     body {
         width: 50%;
         margin-left: auto;
         margin-right: auto;
     }

    </style>
</head>


<h1>Archviewer</h1>

<p>
    This is a web application for accessing historical data from a Tango control system, archived using the HDB++ archiving system. It currently only supports the TimescaleDB database.
</p>

<h2>Usage</h2>

<h3>Attributes</h3>

<p>
    The archiving system stores historical values for individual Tango attributes, e.g. <tt>sys/tg_test/1/ampli</tt>. In order to view data for an attribute, first you need to locate it.
</p>

<p>
    First, choose the correct <b>database</b>. You do this via the dropdown selector in the top left. This is knowledge you need to provide, but maybe all the available data is in one single database and there is no choice. Else, hopefully you can find out which database to choose. One typical case would be that all the data for a control system goes in one separate databse.
</p>

<p>
    Second, you need to choose the correct <b>control system</b>, in the dropdown below. If there is only one control system available in the chosen database, it will be pre-selected and you don't need to do anything here. Otherwise, again you need to know, or be able to figure out from the hostname which one to pick.
</p>

<p>
    Third, you need to locate the attribute or attributes you are interested in. If you know the full name, put it in the search panel right below, and press enter. If the attribuite exists in the database, it should appear in the results list beneath the search field, for selection. If you don't know exactly, or you are interested in a range of attributes with similar names, you can use asterisks (*) as wildcards. For example, <tt>sys/tg_test/1/*</tt> should list all the archived attributes of the device called <tt>sys/tg_test/1</tt>.
</p>

<p>
    Once you find some attributes, use the results list to select one or more (using shift- or ctrl-click) of them. Then press one of the "Add" buttons below to add them to one of the Y axes. Currently there are only two, "left" and "right". Using different scales can be useful for comparing values of totally different scales or units. This should immediately cause the attributes to appear in the Y axis attribute lists right below.
</p>

<p>
    Hopefully, within a short time, some colored lines are shown in the main plot window to the right. If not, either there is no data available for the attributes in the current <b>time range</b> (see below), or something went wrong when fetching data. In the latter case, some error message should be displayed at the bottom of the screen.
</p>


<h4>Attribute settings</h4>

<p>
    Each attribute has some configuration that may be interesting to change. This is accessed by clicking the colored button to the left of the attribute name, which should open a small panel at the bottom of the left hand column.
</p>

<p>
    Here it's possible to change the color of the attribute's line in the plot, as well as set which aggregation function to use (see below).
</p>

<p>
    The settings box also contains some "metadata" about the attribute such as the number of points shown, min/max value, etc.
</p>

<p>
    At he bottom there are buttons to download the currently visible attribute data in either TSV or JSON format.
</p>


<h3>Y axes</h3>

<p>
    Each attribute belongs to an Y axis, and all attributes on that axis share the same scale.
</p>

<p>
    There are a few settings on each Y axis:
</p>

<ul>
    <li>"Log" determines whether to use a logarithmic scale, or the default linear scale. Log scale is mainly useful for values that reach over a large range of values, such as pressures</li>
    <li>"Auto" determines whether the viewer should always "fit" the axis scale to show all the attributes in an optimal way, e.g. when the time window changes. This is on by default, but can be disabled if manual adjustment is preferred. The range of an Y axis can also be changed manually, by click-dragging or wheel-zooming on the axis in the plot. Doing this also turns off the "auto" setting on the axis.</li>
</ul>

<p>
    Like the attributes, each axis also has a "settings" box, that can be opened by clicking the axis name. Currently this is mainly useful for manually setting the axis limits to exact values.
</p>

<p>
    If all the attributes on an axis share the same Tango settings of "unit" or "label", this information will be displayed on the plot axis, for convenience.
</p>


<h3>Time range</h3>

<p>
    The "time range" refers to the domain of the "X" axis in the plot. It is also displayed in a more approximate way in the top right corner, where only the dates are shown.
</p>

<p>
    The time range can be changed in several ways:
</p>

<ul>
    <li>By clicking the date range display in the top right, and choosing a range of dates in the calendar dropdown, then pressing "Apply".</li>
    <li>By again clicking the date range, and then clicking one of the "preset" ranges to the right of the dropdown.</li>
</ul>

<p>
    In each case the plot should immediately start to update by fetching new data for the attributes on display.
</p>


<h4>Live mode</h4>

<p>
   The dropdown in the top right can be used to set the application in "live" mode. In this mode, the time range ends at "now", and is automatically moved forward at a fixed period, displaying the last X minutes of data. Live mode is intended for monitoring, where you might want to keep an eye on some attributes over time. However, the update times are intentionally rather slow, to prevent high load on the database. If you want more fast updating display, the archive viewer may not be the right tool.
</p>

<p>
    Panning or zooming the time axis in the plot, or using the time range dropdown to select an interval will cause live mode to be turned off.
</p>


<h3>Aggregation</h3>

<p>
    In an effort to keep performance reasonable, the viewer does not always fetch the entire dataset for the given time range. Instead, it asks the database to estimate the number of points in the current range, for each attribute, and if this number is larger than an "aggregation limit", it instead asks the database to deliver <b>aggregated</b> data for that attribute. Note that this is based on an <i>estimate</i> of the number of points, which means it's expected to be a bit off in either direction, but close enough. The idea is that this is a faster operation than to actually count them exactly.
</p>


<p>
    <i>Aggregation</i> in this case means that the time range is split into a number of evenly sized "buckets", and then all the Y values within each bucket are combined into a single, "aggregated" value. The number of buckets used will vary a bit but it should be slightly more than the horizontal width of the plot in screen pixels. The number is chosen so that each bucket corresponds to a reasonably even number of seconds.
</p>

<p>
    The point of doing this is that we can avoid having to transfer potentially hundreds of millions of data points to the browser, and can instead let the database do this processing at the "source". This leads to much better performance, especially when viewing a large time range. In the most common case, plotting more points than the number of horizontal pixels on screen is kind of useless anyway.
</p>


<h4>Aggregation functions</h4>

<p>
    The aggregation happens according to one of several different "functions". Currently available functions are:
</p>

<ul>
    <li>"Avg" which takes the average of all the values in the bucket. This is the default.</li>
    <li>"Max" which takes the highest value in each bucket</li>
    <li>"Min" which takes the smallest value in each bucket</li>
</ul>

<p>
    The "Aggregation function" dropdown in the top bar changes the default aggregation function used for all attributes. It is also possible to set the aggregation function to use per attribute, via the attribute settings panel (see above). This will always override the default setting.
</p>

<p>
    However, aggregation can sometimes give unexpected results, for example averaging tends to hide interesting features. Since the aggregation happens automatically it is important to be aware of it. Therefore, attributes that are currently aggregated have a small indicator in the attribute list on the left, showing which aggregation function is in use.
</p>

<p>
    The aggregation functions are provided by the TimescaleDB database, and the list is likely to grow.
</p>

<h4>Aggregation limit</h4>

<p>
    To influence aggregation in edge cases, it's possible to change the limit value from 20000 to any number, in the top bar. Aggregation can also be completely disabled by setting the aggregation limit to 0. However this may make the application extremely slow if there are many attribute points to be fetched, so you do this on your own risk! It's probably a good idea to check the estimated count numbers in the attribute settings panel before doing this. The application can probably handle up to some hundred thousand total points reasonably, but loading and interaction will be progressively slower beyond that.
</p>


<h3>Plot</h3>

<p>
    The main part of the page is the <b>plot</b>. Here, each attribute is represented by a colored line. There are some ways to interact with the plot:
</p>

<ul>
    <li>By simply clicking and dragging the main plot with the mouse to the left or right, the time range (see below) can be shifted.</li>
    <li>By scrolling the mouse wheel on the main plot, the time range may be zoomed in or out.</li>
    <li>Dragging or zooming on a particular Y axis allows overriding the automatic Y scaling (see "Y axes" below).</li>
    <li>Just "hovering" the mouse over the plot should show small "dots" marking the point nearest in time of each attribute. For the one point closest to the mouse pointer a small "popup" also shows the name of the attribute, the value and timestamp of the point as well as some other info.</li>
</ul>


<h3>Saving configurations</h3>

<p>
    There is currently no functionality in the application itself, to save or retrieve configurations.
</p>

<p>
    However, it is a web application, and all the important settings are stored in the URL of the page itself. This means that any changes in attributes, time range, axes etc are reflected there. The whole URL can be copied and pasted to create a "persistent" link to a particular view, saved (e.g. using browser bookmarks) or shared (for collaboration or reporting issues).
</p>


<h3>Downloading plot</h3>

<p>
    The current plot can be downloaded as a PNG image, by pressing the "PNG" image in the bottom corner. This will trigger a download, and you should find the image in your local download directory.
</p>
