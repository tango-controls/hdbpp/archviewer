#!/bin/env python

"""
An example script for how to work with the API.
Can be used to search and fetch archived data.
Supports aggregation and various output formats and conversions.

$ archfetcher.py -h for help

$ archfetcher.py --url http://localhost:8009 sys/tg_test/1/ampli --start 2025-01-26T13:57:00

"""

import csv
from datetime import datetime, timedelta, timezone
import json
import logging
from math import ceil
import time
from urllib.request import Request, urlopen
from urllib.parse import urlencode
from urllib.error import URLError, HTTPError


def list_databases(url):
    logging.debug("Listing databases, URL: %r", url)
    request = Request(
        f"{url}/api/databases",
        headers={
            'Content-Type': 'application/json',
            "Accept": "application/json",
        },
        method="GET"
    )
    try:
        with urlopen(request) as f:
            result = json.load(f)
            return result
    except HTTPError as e:
        error = e.read().decode()
        raise RuntimeError(f"Failed to list databases: {error}")


def list_controlsystems(url, db):
    logging.debug("Listing control systems, URL: %r", url)
    request = Request(
        f"{url}/api/controlsystems/?db={db}",
        headers={
            'Content-Type': 'application/json',
            "Accept": "application/json",
        },
        method="GET"
    )
    try:
        with urlopen(request) as f:
            result = json.load(f)
            return result
    except HTTPError as e:
        error = e.read().decode()
        raise RuntimeError(f"Failed to list control systems: {error}")


def search_attributes(base_url, db, cs, phrase):
    query = urlencode({"db": db, "cs": cs, "phrase": phrase})
    url = f"{base_url}/api/attribute/search?{query}"
    logging.debug("Searching for attributes, URL: %r", url)
    request = Request(
        url,
        headers={
            'Content-Type': 'application/json',
            "Accept": "application/json",
        },
        method="GET"
    )
    try:
        with urlopen(request) as f:
            return json.load(f)
    except HTTPError as e:
        error = e.read().decode()
        raise RuntimeError(f"Failed to search attributes: {error}")


def get_attribute_data(base_url: str, db: str, att_conf_id: int,
                       start_time: datetime, end_time: datetime,
                       agg_size: float = 0, agg_buckets: int = 0,
                       agg_func="avg"):
    """
    Return data for one attribute in the given time window.

    Optionally aggregate data, if you give an "agg_size" in seconds.
    It means that the time window will be split in buckets of (roughly)
    this size, and all values in each bucket will be combined
    into one value, using agg_func. By default it's "avg" but may
    also be "max" or "min".

    Alternatively you can give the number of buckets to use as agg_buckets.
    It's equivalent to agg_size = (end_time - start_time) / agg_buckets.

    Aggregation can speed things up a lot if the window is large.
    """
    query = {
        "db": db, "att_conf_id": att_conf_id,
        "start_time": start_time.isoformat(),
        "end_time": end_time.isoformat(),
    }
    if agg_buckets:
        query["agg_limit"] = 1
        query["agg_buckets"] = agg_buckets
        query["agg_func"] = agg_func
    elif agg_size > 0:
        query["agg_limit"] = 1
        query["agg_buckets"] = ceil((end_time - start_time).total_seconds() / agg_size)
        query["agg_func"] = agg_func

    else:
        query["agg_limit"] = 0

    url = f"{base_url}/api/attribute/data?{urlencode(query)}"
    logging.debug("Getting attribute data, URL: %r", url)
    request = Request(
        url,
        headers={
            'Content-Type': 'application/json',
            "Accept": "application/json",
        },
        method="GET"
    )
    with urlopen(request) as f:
        info = json.load(f)
    result_url = f"{base_url}{info['urls']['result']}"
    data_request = Request(
        result_url,
        headers={
            'Content-Type': 'application/json',
            "Accept": "text/tab-separated-values",
        },
        method="GET"
    )
    while True:
        try:
            with urlopen(data_request) as f:
                yield from f.readlines()
            return
        except URLError:
            # Timeout (I think), let's try again
            time.sleep(1)
        except HTTPError as e:
            error = e.read().decode()
            raise RuntimeError(f"Failed to get data: {error}")


def decode_data(data, as_json=False, utc=False):
    lines = (l.decode('utf-8') for l in data)
    csv.field_size_limit(1000000)  # Handle large arrays
    reader = csv.reader(lines, delimiter="\t")
    for data_time, value_r, value_w, quality, error in reader:

        if utc:
            timestamp = data_time
        else:
            timestamp = (
                datetime
                .fromisoformat(data_time)
                .replace(tzinfo=timezone.utc)
                .astimezone(tz=None)
                .isoformat()
            )
        if as_json:
            yield {
                "data_time": timestamp,
                "value_r": None if value_r == "" else json.loads(value_r),
                "value_w": None if value_w == "" else json.loads(value_w),
                "quality": int(quality) if quality else None,
                "error": error or None
            }
        else:
            yield "\t".join([timestamp, value_r or "", value_w or "", quality, error])


if __name__ == "__main__":

    from argparse import ArgumentParser
    import sys

    parser = ArgumentParser(
        description="Get attribute data from the archiving database."
    )
    parser.add_argument(
        "--url",
        default="https://archviewer.maxiv.lu.se",
        help=("Base URL for the archviewer service, e.g."
              + " 'https://archviewer.maxiv.lu.se'")
    )
    parser.add_argument(
        "--db",
        help="Name of the database to use, e.g. 'hdb_machine'"
    )
    parser.add_argument(
        "--cs",
        help="Name of the control system, e.g. 'g-v-csdb-0.maxiv.lu.se:10000'"
    )
    parser.add_argument(
        "attribute",
        help="Name of an attribute, e.g. 'sys/tg_test/1/double_scalar'."
    )
    parser.add_argument(
        "--search",
        action="store_true",
        help=("Only search for matching attributes, and list them."
              + " Here, the attribute argument may contain '*' wildcards.")
    )
    # Output
    parser.add_argument(
        "--json",
        action="store_true",
        help="Output data as ND-JSON (one object per line)"
    )
    parser.add_argument(
        "--raw",
        action="store_true",
        help="Output unprocessed TSV data. Fastest method. Always UTC timestamps."
    )
    parser.add_argument(
        "--utc",
        action="store_true",
        help="Don't convert timestamps from UTC to local. Good for performance."
    )
    # Time window
    parser.add_argument(
        "--start",
        type=datetime.fromisoformat,
        default=datetime.now() - timedelta(hours=1),
        help=("Start of the time window, e.g. '2022-01-01T00:00:00'."
              + " Uses local timezone.")
    )
    parser.add_argument(
        "--end",
        type=datetime.fromisoformat,
        default=datetime.now(),
        help=("End of the time window, e.g. '2022-01-01T00:00:00'."
              + " Uses local timezone.")
    )
    # Aggregation
    parser.add_argument(
        "--agg-size",
        default=0,
        type=int,
        help="Aggregation bucket size in seconds."
    )
    parser.add_argument(
        "--agg-buckets",
        default=0,
        type=int,
        help="Number of aggregation 'buckets'."
    )
    # Other
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Enable debug logging, for troubleshooting."
    )

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    # Find the DB
    if not args.db:
        dbs = list_databases(args.url)
        if len(dbs) == 1:
            # There's only one DB, use that
            args.db = dbs[0]["db_name"]
        else:
            for db in dbs:
                if args.json:
                    print(json.dumps(db))
                else:
                    print(db["db_name"])
            sys.exit("You must choose one of the above databases using the --db argument.")

    # Find the CS
    css = list_controlsystems(args.url, args.db)
    if not(css):
        sys.exit("Could not find any control systems; are you using the right db?")
    if args.cs and args.cs not in css:
        for cs in css:
            if args.json:
                print(json.dumps(cs))
            else:
                print(cs["cs_name"])
        sys.exit(f"Unknown control system '{args.cs}' given. Pick one of the above.")
    cs = args.cs or css[0]["cs_name"]

    # Find the attribute
    if "*" in args.attribute and not args.search:
        sys.exit("Wildcards are only allowed with the --search argument.")
    attrs = search_attributes(args.url, args.db, cs, args.attribute)
    logging.debug("Found %r attributes", len(attrs))
    if args.search:
        for attr in attrs:
            if args.json:
                print(json.dumps(attr))
            else:
                print("\t".join([attr["cs_name"], attr["name"], attr["format"], attr["type"], str(attr["att_conf_id"])]))
        exit_code = not bool(attrs)
        sys.exit(exit_code and f"Found no attributes matching '{args.attribute}'.")

    if len(attrs) == 0:
        sys.exit("Could not find an attribute of the given name. Perhaps try adding a wildcard (*)?")
    if len(attrs) > 1:
        for attr in attrs:
            print(attr["name"])
        sys.exit("Several attributes match the given pattern, please choose one.")
    att_conf_id = attrs[0]["att_conf_id"]

    t0 = time.time()

    # Download some data
    data = get_attribute_data(args.url, args.db, att_conf_id,
                              args.start, args.end,
                              agg_size=args.agg_size, agg_buckets=args.agg_buckets)

    rows = 0
    if args.raw:
        # "Raw" method, just flushing the data directly to stdout
        # This skips any parsing and conversion, a lot faster for large data
        for line in data:
            sys.stdout.buffer.write(line)
            rows += 1
        sys.stdout.flush()
    else:
        for item in decode_data(data, as_json=args.json, utc=args.utc):
            if args.json:
                print(json.dumps(item))
            else:
                print(item)
            rows += 1

    logging.debug("Got %r rows of data in %.3fs", rows, time.time() - t0)
